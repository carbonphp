<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>CarbonPHP Documentation</title>
	<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}

		html {
			font-family: "Lucida Sans Unicode", "Lucida Grande";
			font-size: 14px;
		}

		body {
			background: #fff;
			color: #666;
		}

		h1 {
			font-size: 15px;
			font-weight: bold;
			margin: 0 0 5px 0;
		}

		h2 {
			font-size: 13px;
			font-weight: bold;
			margin: 5px 0 5px 0;
		}

		ol, ul {
			margin: 10px 0 10px 0;
			list-style-position: inside;
		}

		p {
			margin: 5px 0 5px 0;
		}

		.content {
			border: 1px dotted #444;
			margin: 10px;
			padding: 10px;
			text-align: justify;
			width: 700px;
		}

		.example {
			border: 1px solid #ccc;
			background: #eee;
			margin: 10px;
			padding: 10px;
		}
	</style>
</head>

<body>
	<div class="content">
		<h1>Zip Library</h1>

		<p>The zip library allows you to create zip archives which can be downloaded or saved to a directory on the
		web host.</p>

		<p>You can load the zip library just like any other library with the <b>$this->load->library()</b> method.</p>
	</div>

	<div class="content">
		<h1>Using the Zip Library</h1>

		<p>This example shows you how to compress a file, save it to a directory on the web host, and download it.</p>

		<div class="example">
			$name = 'somedata.txt';<br />
			$data = 'This is a data string';<br />
			<br />
			$this->zip->data_date($name, $data);<br />
			<br />
			// Write the zip file to a directory...<br />
			$this->zip->archive('/path/to/a/dir/backup.zip');<br />
			<br />
			// Download the file...<br />
			$this->zip->download('backup.zip');			
		</div>
	</div>

	<div class="content">
		<h1>$this->zip->add_data()</h1>

		<p>This method allows you to add data to your zip archive.  The first parameter is the file name, and the
		second parameter is the data contained in the file as a string.</p>

		<div class="example">
			$name = 'some_file.txt';<br />
			$data = 'This is some random data to store in the file...';<br />
			<br />
			$this->zip->add_data($name, $data);
		</div>

		<p>You can also call the method multiple times to add multiple files to the archive.  You can also pass multiple
		files to the method using an array.</p>

		<div class="example">
			$data = array(<br />
			&nbsp;&nbsp;&nbsp;&nbsp;'file1.txt' => 'Some data',<br />
			&nbsp;&nbsp;&nbsp;&nbsp;'file2.txt' => 'More data'<br />
			);<br />
			<br />
			$this->zip->add_data($data);<br />
			<br />
			$this->zip->download('backup.zip');
		</div>

		<p>You can also organised your compressed data into subdirectories by including the path in the file name.</p>
	</div>

	<div class="content">
		<h1>$this->zip->add_dir()</h1>

		<p>This method allows you to add a directory to your archive.  You usually do not need to call this as you
		can place your data into directories with the <b>$this->zip->add_data()</b> method.</p>

		<div class="example">
			// Creates a directory called 'folder' in the archive...<br />
			$this->zip->add_dir('folder');
		</div>
	</div>

	<div class="content">
		<h1>$this->zip->read_file()</h1>

		<p>This method allows you to add an existing file to the archive.</p>

		<div class="example">
			$path = '/path/to/my/photo.jpg';<br />
			<br />
			$this->zip->read_file($path);<br />
			<br />
			$this->zip->download('my_photos.zip');
		</div>

		<p>If you wish the zip archive to maintain the directory structure the file is in, you can pass <b>true</b> in
		the second parameter of the method.</p>
	</div>

	<div class="content">
		<h1>$this->zip->read_dir()</h1>

		<p>This allows you to recursively add a directory and it's contents to an archive.</p>

		<div class="example">
			$path = '/path/to/directory/';<br />
			<br />
			$this->zip->read_dir($path);<br />
			<br />
			$this->zip->download('backup.zip');
		</div>
	</div>

	<div class="content">
		<h1>$this->zip->archive()</h1>

		<p>This method allows you to create the zip archive in a directory on the web host.  The directory must have
		the correct permissions (666 or 777 will be fine).</p>

		<div class="example">
			// Creates an archive called 'archive.zip'...
			$this->zip->archive('/path/to/backups/archive.zip');
		</div>
	</div>

	<div class="content">
		<h1>$this->zip->archive()</h1>

		<p>This method causes the archive to be downloads from the server.  The function must be passed the name
		you would like the zip archive to the called.  <i>Note</i>: do not display any data in the controller in which
		you call this method since it sends server headers that cause the download to occur and the file to be treated
		as binary data.</p>
	</div>
</body>

</html>
