<?php

class Carbon extends Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->scaffolding('tombell_blog_posts');
	}

	public function index()
	{
		$this->load->utility('url');
		$this->load->view('carbon_view');
	}
}

?>
