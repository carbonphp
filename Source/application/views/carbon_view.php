<html>

<head>
	<title>Welcome to CarbonPHP</title>
	<style type="text/css">
		<?php $this->load->file(APP_PATH . 'views/stylesheet.css'); ?>
	</style>
</head>

<body>

<div class="content">
	<h1>Welcome to CarbonPHP</h1>

	<p>You have successfully setup CarbonPHP ready for use.  You can begin coding your controllers
	in application/controllers and your views in the applicastion/views directory.  Once you work
	on models they can be stored in application/models.</p>

	<p>CarbonPHP is a framework developed to allow developers to create PHP web applications in less
	time than it would than if they were writing from scratch. CarbonPHP offers common libraries for
	developers to use, but also allows developers to extend and create their own application specific
	libraries.</p>

	<h1>Default Controller</h1>

	<div class="code">
		$this-&gt;load-&gt;view('carbon_view');
	</div>

	<p>The default controller contains the above code to load this view.  You can create your own
	views and controllers.</p>

	<h1>Default View</h1>

	<p>The default view is the page you're viewing now!  It contains the HTML and CSS that is viewed
	by the user.</p>

	<div class="code">
		application/views/carbon_view.php
	</div>

	<p>You are also able to pass data from the controller/models.  This allows for the creation of
	dynamic pages.  You are free to modify the CarbonPHP code aslong as the copyright notices remiain
	at the top of the CarbonPHP files.</p>

	<p>Total execution time: {elapsed_time} seconds</p>
	<p>Memory usage: {memory_usage}</p>

	<p><?=anchor('', 'Return home');?></p>
	
</div>

</body>

</html>
