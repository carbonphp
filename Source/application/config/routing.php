<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

/*------------------------------------------------------------
*  This is the default controller which will be used if
*  no controller is specified in the URI.
*------------------------------------------------------------*/
$routing['default_controller'] = 'blog';

/*------------------------------------------------------------
*  This is a secret keyword used to trigger a scaffolding
*  request.  It is here for added security.  You must
*  enable scaffolding in the controller you wish to use it
*  in
*------------------------------------------------------------*/
$routing['scaffolding_trigger'] = '';

/*------------------------------------------------------------
*  You may enter any custom routes here.  For example:
*  $routing['foo/bar/'] = 'carbon/index/';
*------------------------------------------------------------*/


?>
