<html>
<head>
	<title>Database Error</title>
	<style type="text/css">
		body {
			background: #fff;
			color: #000;
			font-family: "Lucida Sans Unicode", "Lucida Grande";
			font-size: 12px;
			margin: 30px;
		}

		#container {
			padding: 25px 12px 25px 12px;
		}
	
		h1 {
			color: #770000;
			font-size: 14px;
			font-weight: normal;
			margin-bottom: 5px;
		}
	</style>
</head>

<body>

	<div id="content">
		<h1><?php echo $heading; ?></h1>
		<p><?php echo $message; ?></p>
	</div>

</body>
</html>
