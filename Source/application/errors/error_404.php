<?php

header("HTTP/1.1 404 Not Found");

?>

<html>
<head>
	<title>404 Not Found</title>
	<style type="text/css">
		body {
			background: #fff;
			color: #000;
			font-family: "Lucida Sans Unicode", "Lucida Grande";
			font-size: 12px;
			margin: 30px;
		}

		#container {
			padding: 25px 12px 25px 12px;
		}

		h1 {
			color: #770000;
			font-size: 14px;
			font-weight: normal;
			margin-bottom: 5px;
		}
	</style>
</head>

<body>
	<div id="container">
		<h1><?=$heading;?></h1>
		<p><?=$message;?></p>
	</div>
</body>
</html>
