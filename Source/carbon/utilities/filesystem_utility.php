<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

function read_file($file_name)
{
	if (!file_exists($file_name))
	{
		return false;
	}

	if (function_exists('file_get_contents'))
	{
		return file_get_contents($file_name);
	}

	$file = @fopen($file_name, 'rb');

	if ($file == false)
	{
		return false;
	}

	flock($file, LOCK_SH);

	$data = '';

	if (filesize($file) > 0)
	{
		$data =& fread($file, filesize($file));
	}

	flock($file, LOCK_UN);
	fclose($file);

	return $data;
}

function write_file($path, $data, $mode = 'wb')
{
	$file = fopen($path, $mode);

	if ($file == false)
	{
		return false;
	}

	flock($file, LOCK_EX);
	fwrite($file, $data);
	flock($file, LOCK_UN);
	fclose($file);

	return true;
}

function delete_files($path, $del_dir = false, $level = 0)
{
	$path = preg_replace("|^(.+?)/*$|", "\\1", $path);

	$current_dir = @opendir($path);

	if ($current_dir == false)
	{
		return false;
	}

	while (($filename = @readdir($current_dir)) !== false)
	{
		if ($filename != "." && $filename != "..")
		{
			if (is_dir($path . '/' . $filename))
			{
				delete_files($path . '/' . $filename, $del_dir, $level + 1);
			}
			else
			{
				unlink($path . '/' . $filename);
			}
		}
	}

	@closedir($current_dir);

	if ($del_dir == true && $level > 0)
	{
		@rmdir($path);
	}
}

function get_filenames($source_dir, $include_path = false)
{
	$filedata = array();

	$file = @opendir($source_dir);

	if ($file == true)
	{
		while (($filename = readdir($file)) !== false)
		{
			if (@is_dir($source_dir . $filename) && substr($filename, 0, 1) != '.')
			{
				get_filenames($source_dir . $filename . '/', $include_path);
			}
			elseif (substr($filename, 0, 1) != ".")
			{
				$filedata[] = ($include_path == true) ? $source_dir . $filename : $filename;
			}
		}

		return $filedata;
	}
}

?>
