<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('site_url'))
{
	function site_url($uri = '')
	{
		$carbon =& get_instance();
		return $carbon->config->get_site_url($uri);
	}
}

if (!function_exists('base_url'))
{
	function base_url()
	{
		$carbon =& get_instance();
		return $carbon->config->slash_item('base_url');
	}
}

if (!function_exists('index_page'))
{
	function index_page()
	{
		$carbon =& get_instance();
		return $carbon->config->get_item_value('index_page');
	}
}

if (!function_exists('anchor'))
{
	function anchor($uri = '', $title = '', $attributes = '')
	{
		$title = (string) $title;

		if (!is_array($uri))
		{
			$site_url = (!preg_match('!^\w+://!i', $uri)) ? site_url($uri) : $uri;
		}
		else
		{
			$site_url = site_url($uri);
		}

		if ($title == '')
		{
			$title = $site_url;
		}

		if ($attributes == '')
		{
			$attributes = ' title="' . $title . '"';
		}
		else
		{
			$attributes = parse_attributes($attributes);
		}

		return '<a href="' . $site_url . '"' . $attributes . '>' . $title . '</a>';
	}
}

if (!function_exists('anchor_popup'))
{
	function anchor_popup($uri = '', $title = '', $attributes = false)
	{
		$title = (string) $title;
		$site_url = (!preg_match('!^\w+://!i', $uri)) ? site_url($uri) : $uri;

		if ($title == '')
		{
			$title = $site_url;
		}

		if ($attributes === false)
		{
			return "<a href='javascript:void(0);' onclick=\"window.open('" . $site_url . "', '_blank');\">" . $title . "</a>";
		}

		if (!is_array($attributes))
		{
			$attributes = array();
		}

		$default = array(
			'width' => '800',
			'height' => '600',
			'scrollbars' => 'yes',
			'status' => 'yes',
			'resizable' => 'yes',
			'screenx' => '0',
			'screeny' => '0'
		);

		foreach ($default as $key => $val)
		{
			$attr[$key] = (!isset($attributes[$key])) ? $val : $attributes[$key];
		}

		return "<a href='javascript(0);' onclick=\"window.open('" . $site_url . "', '_blank', '" . parse_attributes($attr, true) . "');\">" . $title . "</a>";
	}
}

if (!function_exists('mailto'))
{
	function mailto($email, $title = '', $attributes = '')
	{
		$title = (string) $title;

		if ($title == '')
		{
			$title = $email;
		}

		$attributes = parse_attributes($attributes);

		return '<a href="mailto:' . $email . '"' . $attributes . '>' . $title . '</a>';
	}
}

if (!function_exists('safe_mailto'))
{
	function safe_mailto($email, $title = '', $attributes = '')
	{
		$title = (string) $title;

		if ($title == '')
		{
			$title = $email;
		}

		for ($i = 0; $i < 16; $i++)
		{
			$x[] = substr('<a href="mailto:', $i, 1);
		}

		for ($i = 0; $i < strlen($email); $i++)
		{
			$x[] = '|' . ord(substr($email, $i, 1));
		}

		$x[] = '"';

		if ($attributes != '')
		{
			if (is_array($attributes))
			{
				foreach ($attributes as $key => $val)
				{
					$x[] = ' ' . $key . '="';

					for ($i = 0; $i < strlen($val); $i++)
					{
						$x[] = '|' . ord(substr($val, $i, 1));
					}

					$x[] = '"';
				}
			}
			else
			{
				for ($i = 0; $i < strlen($attributes); $i++)
				{
					$x[] = substr($attributes, $i, 1);
				}
			}
		}

		$x[] = '>';

		$temp = array();

		for ($i = 0; $i < strlen($title); $i++)
		{
			$ordinal = ord($title[$i]);

			if ($ordinal < 128)
			{
				$x[] = '|' . $ordinal;
			}
			else
			{
				if (count($temp) == 0)
				{
					$count = ($ordinal < 224) ? 2 : 3;
				}

				$temp[] = $ordinal;

				if (count($temp) == $count)
				{
					$number = ($count == 3) ? (($temp['0'] % 16) * 4096) + (($temp['1'] % 64) * 64) + ($temp['2'] % 64) : (($temp['0'] % 32) * 64) + ($temp['1'] % 64);
					$x[] = '|' . $number;
					$count = 1;
					$temp = array();
				}
			}
		}

		$x[] = '<';
		$x[] = '/';
		$x[] = 'a';
		$x[] = '>';

		$x = array_reverse($x);

		ob_start();
	?>

		<script type="text/javascript">
		//<![CDATA[
			var l = new Array();

			<?php
			$i = 0;
			foreach ($x as $val)
			{
			?>
				l[<?php echo $i++;?>] = '<?php echo $val;?>';
			<?php
			}
			?>

			for (var i = l.length - 1; i >= 0; i = i - 1)
			{
				if (l[i].substring(0, 1) == '|')
				{
					document.write("&#" + unescape(l[i].substring(1)) + ";");
				}
				else
				{
					document.write(unescape(l[i]));
				}
			}
		//]]>	
		</script>

	<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}
}

if (!function_exists('auto_link'))
{
	function auto_link($string, $type = 'both', $popup = false)
	{
		if ($type != 'email')
		{
			if (preg_match_all("#(^|\s|\()((http(s?)://)|(www\.))(\w+[^\s\)\<]+)#i", $string, $matches))
			{
				$pop = ($popup == true) ? ' target="_blank" ' : '';

				for ($i = 0; $i < sizeof($matches['0']); $i++)
				{
					$period = '';

					if (preg_match("|\.$|", $matches['6'][$i]))
					{
						$peroid = '.';
						$matches['6'][$i] = substr($matches['6'][$i], 0, -1);
					}

					$string = str_replace($matches['0'][$i],
						$matches['1'][$i] . '<a href="http' .
						$matches['4'][$i] . '://' .
						$matches['5'][$i] .
						$matches['6'][$i] . '"' . $pop . '>http' .
						$matches['4'][$i] . '://' .
						$matches['5'][$i] .
						$matches['6'][$i] . '</a>' .
						$period, $string);
				}
			}
		}

		if ($type != 'url')
		{
			if (preg_match_all("/([a-zA-Z0-9_\.\-]+)@([a-zA-Z0-9\-]+)\.([a-zA-Z0-9\-\.]*)/i", $string, $matches))
			{
				for ($i = 0; $i < sizeof($matches['0']); $i++)
				{
					$period = '.';
					$matches['3'][$i] = substr($matches['3'][$i], 0, -1);
				}

				$string = str_replace($matches['0'][$i], safe_mailto($matches['1'][$i] . '@' . $matches['2'][$i] . '.' . $matches['3'][$i]) . $period, $string);
			}
		}

		return $string;
	}
}

if (!function_exists('prep_url'))
{
	function prep_url($string = '')
	{
		if ($string == 'http://' || $string == '')
		{
			return '';
		}

		if (substr($string, 0, 7) != 'http://' && substr($string, 0, 8) != 'https://')
		{
			$string = 'http://' . $string;
		}

		return $string;
	}
}

if (!function_exists('url_title'))
{
	function url_title($string, $separator = 'dash')
	{
		if ($separator == 'dash')
		{
			$search = '_';
			$replace = '-';
		}
		else
		{
			$search = '-';
			$replace = '_';
		}

		$trans = array(
			$search => $replace,
			"\s+" => $replace,
			"[^a-z0-9]" . $replace . "]" => '',
			$replace . "+" => $replace,
			$replace . "$" => '',
			"^" . $replace => ''
		);

		$string = strip_tags(strtolower($string));

		foreach ($trans as $key => $val)
		{
			$string = preg_replace("#" . $key . "#", $val, $string);
		}

		return trim(stripslashes($string));
	}
}

if (!function_exists('redirect'))
{
	function redirect($uri = '', $method = 'location')
	{
		switch ($method)
		{
			case 'refresh':
				header('Refresh:0; url= ' . site_url($uri));
			break;

			default:
				header('Location: ' . site_url($uri));
			break;
		}

		exit();
	}
}

if (!function_exists('parse_attributes'))
{
	function parse_attributes($attributes, $javascript = false)
	{
		if (is_string($attributes))
		{
			return ($attributes != '') ? ' ' . $attributes : '';
		}

		$attr = '';

		foreach ($attributes as $key => $val)
		{
			if ($javascript == true)
			{
				$attr .= $key . '=' . $val . ',';
			}
			else
			{
				$attr .= ' ' . $key . '="' . $val . '"';
			}
		}

		if ($javascript == true && $attr != '')
		{
			$attr = substr($attr, 0, -1);
		}

		return $attr;
	}
}

?>
