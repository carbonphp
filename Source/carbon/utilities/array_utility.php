<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('element'))
{
	function element($item, $array, $default = false)
	{
		if (!isset($array[$item]) || $array[$item] == '')
		{
			return $default;
		}

		return $array[$item];
	}
}

if (!function_exists('random_element'))
{
	function random_element($array)
	{
		if (!is_array($array))
		{
			return $array;
		}

		return $array[array_rand($array)];
	}
}

?>
