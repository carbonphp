<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('form_open'))
{
	function form_open($action = '', $attributes = array(), $hidden = array())
	{
		$carbon =& get_instance();

		$action = (strpos($action, '://') === false) ? $carbon->config->get_site_url($action) : $action;

		$form = '<form action="' .  $action . '"';

		if (!isset($attributes['method']))
		{
			$form .= ' method="post"';
		}

		if (is_array($attributes) && count($attributes) > 0)
		{
			foreach ($attributes as $key => $val)
			{
				$form .= ' ' . $key . '="' . $val . '"';
			}
		}

		$form .= '>';

		if (is_array($hidden) && count($hidden) > 0)
		{
			$form .= form_hidden($hidden);
		}

		return $form;
	}
}

if (!function_exists('form_open_multipart'))
{
	function form_open_multipart($action, $attributes = array(), $hidden = array())
	{
		$attributes['enctype'] = 'multipart/form-data';
		return form_open($action, $attributes, $hidden);
	}
}

if (!function_exists('form_hidden'))
{
	function form_hidden($name, $value = '')
	{
		if (!is_array($name))
		{
			return '<input type="hidden" name="' . $name . '" value="' . form_prep($value) . '" />';
		}

		$form = '';

		foreach ($name as $name => $value)
		{
			$form .= '<input type="hidden" name="' . $name . '" value="' . form_prep($value) . '" />';
		}

		return $form;
	}
}

if (!function_exists('form_input'))
{
	function form_input($data = '', $value = '', $extra = '')
	{
		$defaults = array(
			'type' => 'text',
			'name' => ((!is_array($data)) ? $data : ''),
			'value' => $value,
			'maxlength' => '500',
			'size' => '50'
		);

		return "<input " . parse_form_attributes($data, $defaults) . $extra . " />\n";
	}
}

if (!function_exists('form_password'))
{
	function form_password($data = '', $value = '', $extra = '')
	{
		if (!is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'password';
		return form_input($data, $value, $extra);
	}
}

if (!function_exists('form_upload'))
{
	function form_upload($data = '', $value = '', $extra = '')
	{
		if (!is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'file';
		return form_input($data, $value, $extra);
	}
}

if (!function_exists('form_textarea'))
{
	function form_textarea($data = '', $value = '', $extra = '')
	{
		$defaults = array(
			'name' => ((!is_array($data)) ? $data : ''),
			'cols' => '90',
			'rows' => '12'
		);

		if (!is_array($data) || !isset($data['value']))
		{
			$val = $value;
		}
		else
		{
			$val = $data['value'];
			unset($data['value']);
		}

		return "<textarea " . parse_form_attributes($data, $defaults) . $extra . ">" . $val . "</textarea>\n";
	}
}

if (!function_exists('form_dropdown'))
{
	function form_dropdown($name = '', $options = array(), $selected = '', $extra = '')
	{
		if ($extra != '')
		{
			$extra = ' ' . $extra;
		}

		$form = '<select name="' . $name . '"' . $extra . ">\n";

		foreach ($options as $key => $val)
		{
			$key = (string) $key;
			$val = (string) $val;

			$sel = ($selected != $key) ? '' : ' selected="selected"';

			$form .= '<option value="' . $key . '"' . $sel . '>' . $val . "</option>\n";
		}

		$form .= '</select>';
		return $form;
	}
}

if (!function_exists('form_checkbox'))
{
	function form_checkbox($data = '', $value = '', $checked = true, $extra = '')
	{
		$defaults = array(
			'type' => 'checkbox',
			'name' => ((!is_array($data)) ? $data : ''),
			'value' => $value
		);

		if (is_array($data) && array_key_exists('checked', $data))
		{
			$checked = $data['checked'];

			if ($checked == false)
			{
				unset($data['checked']);
			}
			else
			{
				$data['checked'] = 'checked';
			}
		}

		if ($checked == true)
		{
			$defaults['checked'] = 'checked';
		}
		else
		{
			unset($defaults['checked']);
		}

		return "<input " . parse_form_attributes($data, $defaults) . $extra . " />\n";
	}
}

if (!function_exists('form_radio'))
{
	function form_radio($data = '', $value = '', $checked = true, $extra = '')
	{
		if (!is_array($data))
		{
			$data = array('name' => $data);
		}

		$data['type'] = 'radio';
		return form_checkbox($data, $value, $checked, $extra);
	}
}

if (!function_exists('form_submit'))
{
	function form_submit($data = '', $value = '', $extra = '')
	{
		$defaults = array(
			'type' => 'submit',
			'name' => ((!is_array($data)) ? $data : ''),
			'value' => $value
		);

		return "<input " . parse_form_attributes($data, $defaults) . $extra . " />\n";
	}
}

if (!function_exists('form_reset'))
{
	function form_reset($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'reset', 'name' => ((!is_array($data)) ? $data : ''), 'value' => $value);
		return "<input " . parse_form_attributes($data, $defaults) . $extra . " />\n";
	}
}

if (!function_exists('form_label'))
{
	function form_label($label_text = '', $id = '', $attributes = array())
	{
		$label = '<label';

		if ($id != '')
		{
			$label .= ' for="' . $id . '"';
		}

		if (is_array($attributes) && count($attributes) > 0)
		{
			foreach ($attributes as $key => $value)
			{
				$label .= ' ' . $key . '="' . $value . '"';
			}
		}

		$label .= '>' . $label_text . '</label>';
		return $label;
	}
}

if (!function_exists('form_fieldset'))
{
	function form_fieldset($legend_text = '', $attributes = array())
	{
		$fieldset = '<fieldset';

		if (is_array($attributes) && count($attributes) > 0)
		{
			foreach ($attributes as $key => $value)
			{
				$fieldset .= ' ' . $key . '="' . $value . '"';
			}
		}

		$fieldset .= ">\n";

		if ($legend_text != '')
		{
			$fieldset .= "<legend>" . $legend_text . "</legend>\n";
		}

		return $fieldset;
	}
}

if (!function_exists('form_fieldset_close'))
{
	function form_fieldset_close($extra = '')
	{
		return "</fieldset>\n" . $extra;
	}
}

if (!function_exists('form_close'))
{
	function form_close($extra = '')
	{
		return "</form>\n" . $extra;
	}
}

if (!function_exists('form_prep'))
{
	function form_prep($string = '')
	{
		if ($string === '')
		{
			return '';
		}

		$temp = '__TEMP_AMPERSANDS__';

		$string = preg_replace("/&#(\d+);/", "$temp\\1;", $string);
		$stirng = preg_replace("/&(\w+);/",  "$temp\\1;", $string);

		$string = htmlspecialchars($string);

		$string = str_replace(array("'", '"'), array("&#39;", "&quot;"), $string);

		$string = preg_replace("/$temp(\d+);/", "&#\\1;", $string);
		$string = preg_replace("/$temp(\w+);/", "&\\1;", $string);

		return $string;
	}
}

if (!function_exists('parse_form_attributes'))
{
	function parse_form_attributes($attributes, $default)
	{
		if (is_array($attributes))
		{
			foreach ($default as $key => $val)
			{
				if (isset($attributes[$key]))
				{
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}

			if (count($attributes) > 0)
			{
				$default = array_merge($default, $attributes);
			}
		}

		$attr = '';

		foreach ($default as $key => $val)
		{
			if ($key == 'value')
			{
				$val = form_prep($val);
			}

			$attr .= $key . '="' . $val . '" ';
		}

		return $attr;
	}
}

?>
