<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('generate_uuid'))
{
	function generate_uuid($system_id)
	{
		$rnd_id = crypt(uniqid(rand(), 1));
		$rnd_id = strip_tags(stripslashes($rnd_id));
		$rnd_id = str_replace(array(".", "/"), array("", ""), $rnd_id);
		$rnd_id = strrev($rnd_id);
		$rnd_id = substr($rnd_id, 0, 8);
		$rnd_id = $system_id . $rnd_id;

		return $rnd_id;
	}
}

?>
