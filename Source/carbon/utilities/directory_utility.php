<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('directory_map'))
{
	function directory_map($source_dir, $top_level_only = false)
	{
		if ($fp = @opendir($source_dir))
		{
			$filedata = array();

			while (($file = readdir($fp)) !== false)
			{
				if (@is_dir($source_dir . $file) && substr($file, 0, 1) != '.' && $top_level_only == false)
				{
					$temp_array = array();
					$temp_array = directory_map($source_dir . $file . '/');
					$filedata[$file] = $temp_array;
				}
				else if (substr($file, 0, 1) != '.')
				{
					$filedata[] = $file;
				}
			}

			return $filedata;
		}
	}
}

?>
