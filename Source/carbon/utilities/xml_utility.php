<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

if (!function_exists('xml_entities'))
{
	function xml_entities($string)
	{
		$temp = '__TEMP_AMPERSANDS__';

		$string = preg_replace("/&#(\d+);/", "$temp\\1;", $string);
		$string = preg_replace("/&(\w+);/", "$temp\\1;", $string);
		$string = str_replace(array("&", "<", ">", "\"", "'", "-"), array("&amp;", "&lt;", "&gt;", "&quot;", "&#39;", "&#45;"), $string);
		$string = preg_replace("/$temp(\d+);/", "&#\\1;", $string);
		$string = preg_replace("/$temp(\w+);/", "&\\1;", $string);

		return $string;
	}
}

?>
