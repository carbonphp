<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_active_record extends Carbon_Database_driver
{
	protected $ar_select = array();
	protected $ar_distinct = false;
	protected $ar_from = array();
	protected $ar_join = array();
	protected $ar_where = array();
	protected $ar_like = array();
	protected $ar_groupby = array();
	protected $ar_having = array();
	protected $ar_limit = false;
	protected $ar_offset = false;
	protected $ar_order = false;
	protected $ar_orderby = array();
	protected $ar_set = array();
	protected $ar_wherein = array();
	protected $ar_aliased_tables = array();
	protected $ar_store_array = array();

	protected $ar_caching = false;
	protected $ar_cache_select = array();
	protected $ar_cache_from = array();
	protected $ar_cache_join = array();
	protected $ar_cache_where = array();
	protected $ar_cache_like = array();
	protected $ar_cache_groupby = array();
	protected $ar_cache_having = array();
	protected $ar_cache_limit = false;
	protected $ar_cache_offset = false;
	protected $ar_cache_order = false;
	protected $ar_cache_orderby = array();
	protected $ar_cache_set = array();

	public function dbprefix($table = '')
	{
		if ($table == '')
		{
			$this->display_error('database_table_name_required');
		}

		return $this->dbprefix . $table;
	}

	public function select($select = '*', $protected_identifiers = true)
	{
		if (is_string($select))
		{
			$select = explode(', ', $select);
		}

		foreach ($select as $value)
		{
			$value = trim($value);

			if ($value != '*' && $protected_identifiers !== false)
			{	
				if (strpos($value, '.') !== false)
				{
					$value = $this->dbprefix . $value;
				}
				else
				{
					$value = $this->database_protect_identifiers($value);
				}
			}

			if ($value != '')
			{
				$this->ar_select[] = $value;

				if ($this->ar_caching === true)
				{
					$this->ar_cache_select[] = $value;
				}
			}
		}

		return $this;
	}

	public function select_max($select = '', $alias = '')
	{
		if (!is_string($select) || $select = '')
		{
			$this->display_error('database_invalid_query');
		}

		$alias = ($alias != '') ? $alias : $select;

		$sql = 'MAX(' . $this->database_protect_identifiers(trim($select)) . ') AS ' . $this->database_protect_identifiers(trim($alias));

		$this->ar_select[] = $sql;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_select[] = $sql;
		}

		return $this;
	}

	public function select_min($select = '', $alias = '')
	{
		if (!is_string($select) || $select = '')
		{
			$this->display_error('database_invalid_query');
		}

		$alias = ($alias != '') ? $alias : $select;

		$sql = 'MIN(' . $this->database_protect_identifiers(trim($select)) . ') AS ' . $this->database_protect_identifiers(trim($alias));

		$this->ar_select[] = $sql;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_select[] = $sql;
		}

		return $this;
	}

	public function select_avg($select = '', $alias = '')
	{
		if (!is_string($select) || $select = '')
		{
			$this->display_error('database_invalid_query');
		}

		$alias = ($alias != '') ? $alias : $select;

		$sql = 'AVG(' . $this->database_protect_identifiers(trim($select)) . ') AS ' . $this->database_protect_identifiers(trim($alias));

		$this->ar_select[] = $sql;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_select[] = $sql;
		}

		return $this;
	}

	public function select_sum($select = '', $alias = '')
	{
		if (!is_string($select) || $select = '')
		{
			$this->display_error('database_invalid_query');
		}

		$alias = ($alias != '') ? $alias : $select;

		$sql = 'SUM(' . $this->database_protect_identifiers(trim($select)) . ') AS ' . $this->database_protect_identifiers(trim($alias));

		$this->ar_select[] = $sql;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_select[] = $sql;
		}

		return $this;
	}

	public function distinct($value = true)
	{
		$this->ar_distinct = (is_bool($value)) ? $value : true;
		return $this;
	}

	public function from($from)
	{
		foreach ((array) $from as $value)
		{
			$this->ar_from[] = $this->database_protect_identifiers($this->database_track_aliases($value));

			if ($this->ar_caching === true)
			{
				$this->ar_caching_from[] = $this->database_protect_identifiers($value);
			}
		}

		return $this;
	}

	public function join($table, $condition, $type = '')
	{
		if ($type != '')
		{
			$type = strtoupper(trim($type));

			if (!in_array($type, array('LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER'), true))
			{
				$type = '';
			}
			else
			{
				$type .= ' ';
			}
		}

		if ($this->dbprefix)
		{
			$this->database_track_aliases($table);
			$condition = preg_replace('|(' . $this->dbprefix . ')([\w\.]+)([\W\s]+)|', "$2$3", $condition);
			$condition = preg_replace('|([\w\.]+)([\W\s]+)(.+)|', $this->dbprefix . "$1$2" . $this->dbprefix . "$3", $condition);
		}

		$join = $type . 'JOIN' . $this->database_protect_identifiers($this->dbprefix . $table, true) . ' ON ' . $condition;
		$this->ar_join[] = $join;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_join[] = $join;
		}

		return $this;
	}

	public function where($key, $value = null, $escape = true)
	{
		return $this->database_where($key, $value, 'AND ', $escape);
	}

	public function or_where($key, $value = null, $escape = true)
	{
		return $this->database_where($key, $value, 'OR ', $escape);
	}

	private function database_where($key, $value = null, $type = 'AND ', $escape = true)
	{
		if (!is_array($key))
		{
			$key = array($key => $value);
		}

		foreach ($key as $k => $v)
		{
			$prefix = (count($this->ar_where) == 0) ? '' : $type;

			if (!$this->database_has_operator($k) && is_null($key[$k]))
			{
				$k .= ' IS NULL';
			}

			if (!is_null($v))
			{
				if ($escape === true)
				{
					if ($this->database_has_operator($k))
					{
						$k = preg_replace("/([A-Za-z_0-9]+)/", $this->database_protect_identifiers('$1'), $k);
					}
					else
					{
						$k = $this->database_protect_identifiers($k);
					}
				}

				if (!$this->database_has_operator($k))
				{
					$k .= ' =';
				}

				$v = ' ' . $this->escape($v);
			}
			else
			{
				$k = $this->database_protect_identifiers($k, true);
			}

			$this->ar_where[] = $prefix . $k . $v;

			if ($this->ar_caching === true)
			{
				$this->ar_cache_where[] = $prefix . $k . $v;
			}
		}

		return $this;
	}

	public function where_in($key = null, $values = null)
	{
		return $this->database_where_in($key, $values);
	}

	public function where_in_or($key = null, $values = null)
	{
		return $this->database_where_in($key, $values, false, 'OR ');
	}

	public function where_not_in($key = null, $values = null)
	{
		return $this->database_where_in($key, $values, true);
	}

	public function or_where_not_in($key = null, $values = null)
	{
		return $this->database_where_in($key, $values, false, 'OR ');
	}

	public function database_where_in($key = null, $values = null, $not = false, $type = 'AND ')
	{
		if ($key === null || !is_array($values))
		{
			return;
		}

		$not = ($not) ? ' NOT ' : '';

		foreach ($values as $value)
		{
			$this->ar_wherein[] = $this->escape($value);
		}

		$prefix = (count($this->ar_where) == 0) ? '' : $type;

		$where_in = $prefix . $this->protect_identifiers($key) . $not . ' IN (' . implode(', ', $this->ar_wherein) . ') ';

		$this->ar_where[] = $where_in;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_where[] = $where_in;
		}

		$this->ar_wherein = array();
		return $this;
	}

	public function like($field, $match = '', $side = 'both')
	{
		return $this->database_like($field, $match, 'AND ', $side);
	}

	public function not_like($field, $match = '', $side = 'both')
	{
		return $this->database_like($field, $match,  'AND ', $side, ' NOT');
	}

	public function or_like($field, $match = '', $side = 'both')
	{
		return $this->database_like($field, $match, 'ON ', $side);
	}

	public function or_not_like($field, $match = '', $side = 'both')
	{
		return $this->database_like($field, $match, 'OR ', $side, 'NOT ');
	}

	private function database_like($field, $match = '', $type = 'AND ', $side = 'both', $not = '')
	{
		if (!is_array($field))
		{
			$field = array($field => $match);
		}

		foreach ($field as $k => $v)
		{
			$k = $this->database_protect_identifiers($k);

			$prefix = (count($this->ar_like) == 0) ? '' : $type;

			$v = $this->escape_string($v);

			if ($side == 'before')
			{
				$like_statement = $prefix . " $k $not LIKE '%{$v}'";
			}
			else if ($side == 'after')
			{
				$like_statement = $prefix . " $k $not LIKE '{$v}%'";
			}
			else
			{
				$like_statement = $prefix . " $k $not LIKE '%{$v}%'";
			}

			$this->ar_like[] = $like_statement;

			if ($this->ar_caching === true)
			{
				$this->ar_cache_like[] = $like_statement;
			}
		}

		return $this;
	}

	public function group_by($by)
	{
		if (is_string($by))
		{
			$by = explode(',', $by);
		}

		foreach ($by as $val)
		{
			$val = trim($val);

			if ($val != '')
			{
				$this->ar_groupby[] = $this->database_protect_identifiers($val);

				if ($this->ar_caching === true)
				{
					$this->ar_cache_groupby[] = $this->database_protect_identifiers($val);
				}
			}
		}

		return $this;
	}

	public function having($key, $value = '')
	{
		return $this->database_having($key, $value, 'AND ');
	}

	public function or_having($key, $value = '')
	{
		return $this->database_having($key, $value, 'ON ');
	}

	private function database_having($key, $value, $type = 'AND ')
	{
		if (!is_array($key))
		{
			$key = array($key => $value);
		}

		foreach ($key as $k => $v)
		{
			$prefix = (count($this->ar_having) == 0) ? '' : $type;
			$k = $this->database_protect_identifiers($k);

			if ($v != '')
			{
				$v = ' ' . $this->escape($v);
			}

			$this->ar_having[] = $prefix . $k . $v;

			if ($this->ar_caching === true)
			{
				$this->ar_cache_having[] = $prefix . $k . $v;
			}
		}

		return $this;
	}

	public function order_by($orderby, $direction = '')
	{
		if (strtolower($direction) == 'random')
		{
			$orderby = '';
			$direction = $this->random_keyword;
		}
		else if (trim($direction) != '')
		{
			$direction = (in_array(strtoupper(trim($direction)), array('ASC', 'DESC', 'RAND()'), true)) ? ' ' . $direction : ' ASC';
		}

		$orderby_statement = $this->database_protect_identifiers($orderby, true) . $direction;
		$this->ar_orderby[] = $orderby_statement;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_orderby[] = $orderby_statement;
		}

		return $this;
	}

	public function limit($value, $offset = '')
	{
		$this->ar_limit = $value;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_limit[] = $value;
		}

		if ($offset != '')
		{
			$this->ar_offset = $offset;

			if ($this->ar_caching === true)
			{
				$this->ar_cache_offset[] = $offset;
			}
		}

		return $this;
	}

	public function offset($value)
	{
		$this->ar_offset = $value;

		if ($this->ar_caching === true)
		{
			$this->ar_cache_offset[] = $value;
		}

		return $this;
	}

	public function set($key, $value = '', $escape = true)
	{
		$key = $this->database_object_to_array($key);

		if (!is_array($key))
		{
			$key = array($key => $value);
		}

		foreach ($key as $k => $v)
		{
			if ($escape === false)
			{
				$this->ar_set[$this->database_protect_identifiers($k)] = $v;

				if ($this->ar_caching === true)
				{
					$this->ar_cache_set[$this->database_protect_identifiers($k)] = $v;
				}
			}
			else
			{
				$this->ar_set[$this->database_protect_identifiers($k)] = $this->escape($v);

				if ($this->ar_caching === true)
				{
					$this->ar_cache_set[$this->database_protect_identifiers($k)] = $this->escape($v);
				}
			}
		}

		return $this;
	}

	public function get($table = '', $limit = null, $offset = null)
	{
		if ($table != '')
		{
			$this->database_track_aliases($table);
			$this->from($table);
		}

		if (!is_null($limit))
		{
			$this->limit($limit, $offset);
		}

		$sql = $this->database_compile_select();

		$result = $this->query($sql);
		$this->database_reset_select();

		return $result;
	}

	public function count_all_results($table = '')
	{
		if ($table != '')
		{
			$this->database_track_aliases($table);
			$this->from($table);
		}

		$sql = $this->database_compile_select($this->count_string . $this->database_protect_identifiers('numrows'));

		$query = $this->query($sql);
		$this->database_reset_select();

		if ($query->num_rows() == 0)
		{
			return '0';
		}

		$row = $query->row();
		return $row->numrows;
	}

	public function get_where($table = '', $where = null, $limit = null, $offset = null)
	{
		if ($table != '')
		{
			$this->database_track_aliases($table);
			$this->from($table);
		}

		if (!is_null($where))
		{
			$this->where($where);
		}

		if (!is_null($limit))
		{
			$this->limit($limit, $offset);
		}

		$sql = $this->database_compile_select();

		$result = $this->query($sql);
		$this->database_reset_select();

		return $result;
	}

	public function insert($table = '', $set = null)
	{
		if (!is_null($set))
		{
			$this->set($set);
		}

		if (count($this->ar_set) == 0)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_must_use_set');
			}

			return false;
		}

		if ($table == '')
		{
			if (!isset($this->ar_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('database_must_set_table');
				}

				return false;
			}

			$table = $this->ar_from[0];
		}

		$sql = $this->database_insert($this->database_protect_identifiers($this->dbprefix . $table), array_keys($this->ar_set), array_values($this->ar_set));

		$this->database_reset_write();
		return $this->query($sql);
	}

	public function update($table = '', $set = null, $where = null, $limit = null)
	{
		if (!is_null($set))
		{
			$this->set($set);
		}

		if (count($this->ar_set) == 0)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_must_use_set');
			}

			return false;
		}

		if ($table == '')
		{
			if (!isset($this->ar_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('database_must_set_table');
				}

				return false;
			}

			$table = $this->ar_from[0];
		}

		if ($where != null)
		{
			$this->where($where);
		}

		if ($limit != null)
		{
			$this->limit($limit);
		}


		$sql = $this->database_update($this->database_protect_identifiers($this->dbprefix . $table), $this->ar_set, $this->ar_where, $this->ar_orderby, $this->ar_limit);

		$this->database_reset_write();
		return $this->query($sql);
	}

	public function empty_table($table = '')
	{
		if ($table == '')
		{
			if (!isset($this->ar_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('database_must_set_table');
				}

				return false;
			}

			$table = $this->ar_from[0];
		}
		else
		{
			$table = $this->database_protect_identifiers($this->dbprefix . $table);
		}

		$sql = $this->database_delete($table);
		$this->database_reset_write();

		return $this->query($sql);
	}

	public function truncate($table = '')
	{
		if ($table == '')
		{
			if (!isset($this->ar_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('database_must_set_table');
				}

				return false;
			}

			$table = $this->ar_from[0];
		}
		else
		{
			$table = $this->database_protect_identifiers($this->dbprefix . $table);
		}

		$sql = $this->database_truncate($table);
		$this->database_reset_write();

		return $this->query($sql);
	}

	public function delete($table = '', $where = '', $limit = null, $reset_data = true)
	{
		if ($table == '')
		{
			if (!isset($this->ar_from[0]))
			{
				if ($this->db_debug)
				{
					return $this->display_error('database_must_set_table');
				}

				return false;
			}

			$table = $this->ar_from[0];
		}
		else if (is_array($table))
		{
			foreach ($table as $single_table)
			{
				$this->delete($single_table, $where, $limit, false);
			}

			$this->database_reset_write();
			return;
		}
		else
		{
			$table = $this->database_protect_identifiers($this->dbprefix . $table);
		}

		if ($where != '')
		{
			$this->where($where);
		}

		if ($limit != null)
		{
			$this->limit($limit);
		}

		if (count($this->ar_where) == 0 && count($this->ar_like) == 0)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_del_must_use_where');
			}

			return false;
		}

		$sql = $this->database_delete($this->dbprefix . $table, $this->ar_where, $this->ar_like, $this->ar_limit);
		$this->database_reset_write();

		if ($reset_data)
		{
			$this->database_reset_write();
		}

		return $this->query($sql);
	}

	public function database_has_operator($string)
	{
		$string = trim($string);

		if (!preg_match("/(\s|<|>|!|=|is null|is not null)/i", $string))
		{
			return false;
		}

		return true;
	}

	private function database_track_aliases($table)
	{
		if (strpos($table, ' ') !== false)
		{
			$table = preg_replace('/ AS /i', ' ', $table);
			$this->ar_aliased_tables[] = trim(strrchr($table, ' '));
		}

		return $this->dbprefix . $table;
	}

	private function database_filter_table_aliases($statements)
	{
		foreach ($statements as $k => $v)
		{
			foreach ($this->ar_aliased_tables as $table)
			{
				$statement = preg_replace('/(\w+\.\w+)/', $this->database_protect_identifiers('$0'), $v);
				$statement = str_replace(array($this->dbprefix . $table, '.'), array($table, $this->database_protect_identifiers('.')), $statement);
			}

			$statements[$k] = $statement;
		}

		return $statements;
	}

	private function database_compile_select($select_override = false)
	{
		$this->database_merge_cache();

		$sql = (!$this->ar_distinct) ? 'SELECT ' : 'SELECT DISTINCT ';

		$sql .= (count($this->ar_select) == 0) ? '*' : implode(', ', $this->ar_select);

		if ($select_override !== false)
		{
			$sql = $select_override;
		}

		if (count($this->ar_from) > 0)
		{
			$sql .= "\nFROM ";
			$sql .= $this->database_from_tables($this->ar_from);
		}

		if (count($this->ar_join) > 0)
		{
			$sql .= "\n";

			if (count($this->ar_aliased_table) > 0 && $this->dbprefix)
			{
				$sql .= implode("\n", $this->database_filter_table_aliases($this->ar_join));
			}
			else
			{
				$sql .= implode("\n", $this->ar_join);
			}
		}

		if (count($this->ar_where) > 0 || count($this->ar_like) > 0)
		{
			$sql .= "\nWHERE ";
		}

		$sql .= implode("\n", $this->ar_where);

		if (count($this->ar_like) > 0)
		{
			if (count($this->ar_where) > 0)
			{
				$sql .= " AND ";
			}

			$sql .= implode("\n", $this->ar_like);
		}

		if (count($this->ar_groupby) > 0)
		{
			$sql .= "\nGROUP BY ";

			if (count($this->ar_aliased_tables) > 0 && $this->dbprefix)
			{
				$sql .= implode(', ', $this->database_filter_table_aliases($this->ar_groupby));
			}
			else
			{
				$sql .= implode(', ', $this->ar_groupby);
			}
		}

		if (count($this->ar_having) > 0)
		{
			$sql .= "\nHAVING ";
			$sql .= implode("\n", $this->ar_having);
		}

		if (count($this->ar_orderby) > 0)
		{
			$sql .= "\nORDER BY ";
			$sql .= implode(', ', $this->ar_orderby);

			if ($this->ar_order !== false)
			{
				$sql .= ($this->ar_order == 'desc') ? ' DESC' : ' ASC';
			}		
		}

		if (is_numeric($this->ar_limit))
		{
			$sql .= "\n";
			$sql = $this->database_limit($sql, $this->ar_limit, $this->ar_offset);
		}

		return $sql;
	}

	private function database_object_to_array($object)
	{
		if (!is_object($object))
		{
			return $object;
		}

		$array = array();

		foreach (get_object_vars($object) as $key => $val)
		{
			if (!is_object($val) && !is_array($val) && $key != 'c_scaffolding' && $key != 'c_scaff_table')
			{
				$array[$key] = $val;
			}
		}

		return $array;
	}

	public function start_cache()
	{
		$this->ar_caching = true;
	}

	public function stop_cache()
	{
		$this->ar_caching = false;
	}

	public function flush_cache()
	{
		$ar_reset_items = array(
			'ar_cache_select' => array(),
			'ar_cache_from' => array(),
			'ar_cache_join' => array(),
			'ar_cache_where' => array(),
			'ar_cache_like' => array(),
			'ar_cache_groupby' => array(),
			'ar_cache_having' => array(),
			'ar_cache_orderby' => array(),
			'ar_cache_set' => array()
		);

		$this->database_reset_run($ar_reset_items);
	}

	private function database_merge_cache()
	{
		$ar_items = array('select', 'from', 'join', 'where', 'like', 'groupby', 'having', 'orderby', 'set');

		foreach ($ar_items as $ar_item)
		{
			$ar_cache_item = 'ar_cache_' . $ar_item;
			$ar_item = 'ar_' . $ar_item;
			$this->$ar_item = array_unique(array_merge($this->$ar_item, $this->$ar_cache_item));
		}
	}

	private function database_reset_run($ar_reset_items)
	{
		foreach ($ar_reset_items as $item => $default_value)
		{
			if (!in_array($item, $this->ar_store_array))
			{
				$this->$item = $default_value;
			}
		}
	}

	private function database_reset_select()
	{
		$ar_reset_items = array(
			'ar_select' => array(),
			'ar_from' => array(),
			'ar_join' => array(),
			'ar_where' => array(),
			'ar_like' => array(),
			'ar_groupby' => array(),
			'ar_having' => array(),
			'ar_orderby' => array(),
			'ar_wherein' => array(),
			'ar_aliased_tables' => array(),
			'ar_distinct' => false,
			'ar_limit' => false,
			'ar_offset' => false,
			'ar_order' => false
		);

		$this->database_reset_run($ar_reset_items);
	}

	private function database_reset_write()
	{
		$ar_reset_items = array(
			'ar_set' => array(), 
			'ar_from' => array(), 
			'ar_where' => array(), 
			'ar_like' => array(),
			'ar_orderby' => array(), 
			'ar_limit' => false, 
			'ar_order' => false
		);

		$this->database_reset_run($ar_reset_items);
	}
}

?>
