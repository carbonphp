<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_result
{
	public $conn_id = null;
	public $result_id = null;
	public $result_array = array();
	public $result_object = array();
	public $current_row = 0;
	public $num_rows = 0;
	public $row_data = null;

	public function result($type = 'object')
	{
		return ($type == 'object') ? $this->result_object() : $this->result_array();
	}

	public function result_object()
	{
		if (count($this->result_object) > 0)
		{
			return $this->result_object;
		}

		if ($this->result_id === false || $this->num_rows() == 0)
		{
			return array();
		}

		$this->database_data_seek(0);

		while($row = $this->database_fetch_object())
		{
			$this->result_object[] = $row;
		}

		return $this->result_object;
	}

	public function result_array()
	{
		if (count($this->result_array) > 0)
		{
			return $this->result_array;
		}

		if ($this->result_id === false || $this->num_rows() == 0)
		{
			return array();
		}

		$this->database_data_seek(0);

		while ($row = $this->database_fetch_assoc())
		{
			$this->result_array[] = $row;
		}

		return $this->result_array;
	}

	public function row($index = 0, $type = 'object')
	{
		if (!is_numeric($index))
		{
			if (!is_array($this->row_data))
			{
				$this->row_data = $this->row_array(0);
			}

			if (isset($this->row_data[$index]))
			{
				return $this->row_data[$index];
			}

			$index = 0;
		}

		return ($type == 'object') ? $this->row_object($index) : $this->row_array($index);
	}

	public function set_row($key, $value = null)
	{
		if (!is_array($this->row_data))
		{
			$this->row_data = $this->row_array(0);
		}

		if (is_array($key))
		{
			foreach ($key as $k => $v)
			{
				$this->row_data[$k] = $v;
			}

			return;
		}

		if ($key != '' && !is_null($value))
		{
			$this->row_data[$key] = $value;
		}
	}

	public function row_object($index = 0)
	{
		$result = $this->result_object();

		if (count($result) == 0)
		{
			return $result;
		}

		if ($index != $this->current_row && isset($result[$index]))
		{
			$this->current_row = $index;
		}

		return $result[$this->current_row];
	}

	public function row_array($index = 0)
	{
		$result = $this->result_array();

		if (count($result) == 0)
		{
			return $result;
		}

		if ($index != $this->current_row && isset($result[$index]))
		{
			$this->current_row = $index;
		}

		return $result[$this->current_row];
	}

	public function first_row($type = 'object')
	{
		$result = $this->result($type);

		if (count($result) == 0)
		{
			return $result;
		}

		return $result[0];
	}

	public function last_row($type = 'object')
	{
		$result = $this->result($type);

		if (count($result) == 0)
		{
			return $result;
		}

		return $result[count($result) - 1];
	}

	public function next_row($type = 'object')
	{
		$result = $this->result($type);

		if (count($result) == 0)
		{
			return $result;
		}

		if (isset($result[$this->current_row + 1]))
		{
			$this->current_row++;
		}

		return $result[$this->current_row];
	}

	public function previous_row($type = 'object')
	{
		$result = $this->result($type);

		if (count($result) == 0)
		{
			return $result;
		}

		if (isset($result[$this->current_row - 1]))
		{
			$this->current_row--;
		}

		return $result[$this->current_row];
	}

	public function num_rows()
	{
		return $this->num_rows;
	}

	public function num_fields()
	{
		return 0;
	}

	public function list_fields()
	{
		return array();
	}

	public function field_data()
	{
		return array();
	}

	public function free_result()
	{
		return true;
	}

	public function database_data_seek()
	{
		return true;
	}

	public function database_fetch_assoc()
	{
		return array();
	}

	public function database_fetch_object()
	{
		return array();
	}
}

?>
