<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_Cache
{
	private $carbon;

	public function __construct()
	{
		$this->carbon =& get_instance();
		$this->carbon->load->utility('filesystem');
	}

	public function check_path($path = '')
	{
		if ($path == '')
		{
			if ($this->carbon->db->cachedir == '')
			{
				return $this->carbon->db->cache_off();
			}

			$path = $this->carbon->db->cachedir;
		}

		$path = preg_replace("/(.+?)\/*$/", "\\1/", $path);

		if (!is_dir($path) || !is_writable($path))
		{
			if ($this->carbon->db->db_debug)
			{
				return $this->carbon->db->display_error('database_invalid_cache_path');
			}

			return $this->carbon->db->cache_off();
		}

		$this->cache->db->cachedir = $path;

		return true;
	}

	public function read($sql)
	{
		if (!$this->check_path())
		{
			return $this->carbon->db->cache_off();
		}

		$uri = ($this->carbon->uri->get_segment(1) == false) ? 'default.' : $this->carbon->uri->get_segment(1) . '+';
		$uri .= ($this->carbon->uri->get_segment(2) == false) ? 'index' : $this->carbon->uri->get_segment(2);

		$filepath = $uri . '/' . md5($sql);

		$cachedata = read_file($this->carbon->db->cachedir . $filepath);

		if ($cachedata === false)
		{
			return false;
		}

		return unserialize($cachedata);
	}

	public function write($sql, $object)
	{
		if (!$this->check_path())
		{
			return $this->carbon->db->cache_off();
		}

		$uri = ($this->carbon->uri->get_segment(1) == false) ? 'default.' : $this->carbon->uri->get_segment(1) . '+';
		$uri .= ($this->carbon->uri->get_segment(2) == false) ? 'index' : $this->carbon->uri->get_segment(2);

		$dir_path = $this->carbon->db->cachedir . $uri . '/';
		$filename = md5($sql);

		if (!@is_dir($dir_path))
		{
			if (!@mkdir($dir_path, 0777))
			{
				return false;
			}

			@chmod($dir_path, 0777);
		}

		if (write_file($dir_path . $filename, serialize($object)) === false)
		{
			return false;
		}

		@chmod($dir_path, $filename, 0777);

		return true;
	}

	public function delete($segment_one = '', $segment_two = '')
	{
		if ($segment_one == '')
		{
			$segment_one = ($this->carbon->uri->get_segment(1) == false) ? 'default' : $this->carbon->uri->get_segment(2);
		}

		if ($segment_two == '')
		{
			$segment_two = ($this->carbon->uri->get_segment(2) == false) ? 'index' : $this->carbon->uri->get_segment(2);
		}

		$dir_path = $this->carbon->db->cachedir . $segment_one . '+' . $segment_two . '/';

		delete_files($dir_path, true);
	}

	public function delete_all()
	{
		delete_files($this->carbon->db->cachedir, true);
	}
}

?>
