<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_driver
{
	public $username;
	public $password;
	public $hostname;
	public $database;
	public $dbdriver = 'mysql';
	public $dbprefix = '';
	public $autoinit = true;
	public $swap_pre = '';
	public $port = '';
	public $pconnect = false;
	public $conn_id = false;
	public $result_id = false;
	public $db_debug = false;
	public $benchmark = 0;
	public $query_count = 0;
	public $bind_marker = '?';
	public $save_queries = true;
	public $queries = array();
	public $query_times = array();
	public $data_cache = array();
	public $trans_enabled = true;
	public $_trans_depth = 0;
	public $_trans_status = true;
	public $cache_on = false;
	public $cachedir = '';
	public $cache_autodel = false;
	public $cache;

	public $stmt_id;
	public $curs_id;
	public $limit_used;

	public function __construct($params)
	{
		if (is_array($params))
		{
			foreach ($params as $key => $value)
			{
				$this->$key = $value;
			}
		}

		log_message('debug', 'Database_driver.php - Carbon_Database_driver class initialised');
	}

	public function initialise($create_db = false)
	{
		if (is_resource($this->conn_id))
		{
			return true;
		}

		$this->conn_id = ($this->pconnect == false) ? $this->database_connect() : $this->database_pconnect();

		if (!$this->conn_id)
		{
			log_message('error', 'Database_driver.php - Unable to connect to the database');

			if ($this->db_debug)
			{
				$this->display_error('database_unable_to_connect');
			}

			return false;
		}

		if ($this->database != '')
		{
			if (!$this->database_select())
			{
				if ($create_db == true)
				{
					$carbon =& get_instance();
					$carbon->load->dbutil();

					if (!$carbon->dbutil->create_database($this->database))
					{
						log_message('error', 'Database_driver.php - Unable to create the database: ' . $this->database);

						if ($this->db_debug)
						{
							$this->display_error('database_unable_to_create', $this->database);
						}

						return false;
					}
					else
					{
						if ($this->database_select())
						{
							if (!$this->database_set_charset($this->char_set, $this->dbcollat))
							{
								log_message('error', 'Database_driver.php - Unable to set the database connection charset: ' . $this->char_set);

								if ($this->db_debug)
								{
									$this->display_error('database_unable_to_set_charset', $this->char_set);
								}

								return false;
							}

							return true;
						}
					}
				}

				log_message('error', 'Database_driver.php - Unable to select the database: ' . $this->database);

				if ($this->db_debug)
				{
					$this->display_error('database_unable_to_select', $this->database);
				}

				return false;
			}

			if (!$this->database_set_charset($this->char_set, $this->dbcollat))
			{
				log_message('error', 'Database_driver.php - Unable to set the database connection charset: ' . $this->char_set);

				if ($this->db_debug)
				{
					$this->display_error('database_unable_to_set_charset', $this->char_set);
				}

				return false;
			}
		}

		return true;
	}

	public function platform()
	{
		return $this->dbdriver;
	}

	public function version()
	{
		$sql = $this->database_version();

		if ($sql === false)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_unsupported_function');
			}

			return false;
		}

		if ($this->dbdriver == 'oci8')
		{
			return $sql;
		}

		$query = $this->query($sql);
		return $row->row('ver');
	}

	public function query($sql, $binds = false, $return_object = true)
	{
		if ($sql == '')
		{
			log_message('error', 'Database_driver.php - Invalid query: ' . $sql);

			if ($this->db_debug)
			{
				return $this->display_error('database_invalid_query');
			}

			return false;
		}

		if (($this->dbprefix != '' && $this->swap_pre != '') && ($this->dbprefix != $this->swap_pre))
		{
			$sql = preg_replace("/(\W)" . $this->swap_pre . "(\S+?)/", "\\1" . $this->dbprefix . "\\2", $sql);
		}

		if ($this->cache_on == true && stristr($sql, 'SELECT'))
		{
			if ($this->database_cache_init())
			{
				$this->load_result_driver();
				$cache = $this->cache->read($sql);

				if ($cache !== false)
				{
					return $cache;
				}
			}
		}

		if ($binds !== false)
		{
			$sql = $this->compile_binds($sql, $binds);
		}

		if ($this->save_queries == true)
		{
			$this->queries[] = $sql;
		}

		$time_start = list($sm, $ss) = explode(' ', microtime());
		$this->result_id = $this->simple_query($sql);

		if ($this->result_id === false)
		{
			$this->_trans_status = false;

			log_message('error', 'Database_driver.php - Query error: ' . $this->database_error_message());

			if ($this->db_debug)
			{
				return $this->display_error(array('Error number: ' . $this->database_error_number(), $this->database_error_message(), $sql));
			}

			return false;
		}

		$time_end = list($em, $es) = explode(' ', microtime());
		$this->benchmark += ($em + $es) - ($sm + $ss);

		if ($this->save_queries == true)
		{
			$this->query_times[] = ($em + $es) - ($sm + $ss);
		}

		$this->query_count++;

		if ($this->is_write_type($sql) === true)
		{
			if ($this->cache_on == true && $this->cache_autodel == true && $this->database_cache_init())
			{
				$this->cache->delete();
			}

			return true;
		}

		if ($return_object !== true)
		{
			return true;
		}

		$driver = $this->load_result_driver();

		$result = new $driver();

		$result->conn_id = $this->conn_id;
		$result->result_id = $this->result_id;
		$result->num_rows = $result->num_rows();

		if ($this->dbdriver == 'oci8')
		{
			$result->stmt_id = $this->stmt_id;
			$result->curs_id = $this->curs_id;
			$result->limit_used = $this->limit_used;
		}

		if ($this->cache_on == true && $this->database_cache_init())
		{
			$cache_result = new Carbon_Database_result();

			$cache_result->num_rows = $result->num_rows();
			$cache_result->result_object = $result->result_object();
			$cache_result->result_array = $result->result_array();

			$cache_result->conn_id = null;
			$cache_result->result_id = null;

			$this->cache->write($sql, $cache_result);
		}

		return $result;
	}

	public function load_result_driver()
	{
		$driver = 'Carbon_Database_' . $this->dbdriver . '_result';

		if (!class_exists($driver))
		{
			include_once(CARBON_PATH . 'database/Database_result' . FILE_EXT);
			include_once(CARBON_PATH . 'database/drivers/' . $this->dbdriver . '/' . $this->dbdriver . '_result' . FILE_EXT);
		}

		return $driver;
	}

	public function simple_query($sql)
	{
		if (!$this->conn_id)
		{
			$this->initialise();
		}

		return $this->database_execute($sql);
	}

	public function trans_off()
	{
		$this->trans_enabled = false;
	}

	public function trans_start($test_mode = false)
	{
		if (!$this->trans_enabled)
		{
			return false;
		}

		if ($this->_trans_depth > 0)
		{
			$this->_trans_depth += 1;
			return;
		}

		$this->trans_begin($test_mode);
	}

	public function trans_complete()
	{
		if (!$this->trans_enabled)
		{
			return false;
		}

		if ($this->_trans_depth > 1)
		{
			$this->_trans_depth -= 1;
			return true;
		}

		if ($this->_trans_status === false)
		{
			$this->trans_rollback();

			if ($this->db_debug)
			{
				return $this->display_error('database_transaction_failure');
			}

			return false;
		}

		$this->trans_commit();
		return true;
	}

	public function trans_status()
	{
		return $this->_trans_status;
	}

	public function compile_binds($sql, $binds)
	{
		if (strpos($sql, $this->bind_marker) === false)
		{
			return $sql;
		}

		if (!is_array($binds))
		{
			$binds = array($binds);
		}

		$segments = explode($this->bind_marker, $sql);

		if (count($binds) >= count($segments))
		{
			$binds = array_slice($binds, 0, count($segments) - 1);
		}

		$result = $segments[0];
		$i = 0;

		foreach ($binds as $bind)
		{
			$result .= $this->escape($bind);
			$result .= $segments[++$i];
		}

		return $result;
	}

	public function is_write_type($sql)
	{
		if (!preg_match('/^\s*"?(INSERT|UPDATE|DELETE|REPLACE|CREATE|DROP|LOAD DATA|COPY|ALTER|GRANT|REVOKE|LOCK|UNLOCK)\s+/i', $sql))
		{
			return false;
		}

		return true;
	}

	public function elapsed_time($precision = 6)
	{
		return number_format($this->benchmark, $precision);
	}

	public function total_queries()
	{
		return $this->query_count;
	}

	public function last_query()
	{
		return end($this->queries);
	}

	public function protect_identifiers($item, $first_word_only = false)
	{
		return $this->database_protect_identifiers($item, $first_word_only);
	}

	public function escape($string)
	{

		if (is_numeric ($string))
		{
			return $string;
		}
		else if (is_string ($string))
		{
			return "'" . $this->escape_string ($string) . "'";
		}
		else if (is_bool ($string))
		{
			return ($string === false) ? 0 : 1;
		}	
		else
		{
			return $string;
		}
	}

	public function primary($table = '')
	{
		$fields = $this->list_fields($table);

		if (!is_array($fields))
		{
			return false;
		}

		return current($fields);
	}

	public function list_tables($constrain_by_prefix = false)
	{
		if (isset($this->data_cache['table_names']))
		{
			return $this->data_cache['table_names'];
		}

		$sql = $this->database_list_tables($constrain_by_prefix);

		if ($sql === false)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_unsupported_function');
			}

			return false;
		}

		$retval = array();
		$query = $this->query($sql);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				if (isset($row['TABLE_NAME']))
				{
					$retval[] = $row['TABLE_NAME'];
				}
				else
				{
					$retval[] = array_shift($row);
				}
			}
		}

		$this->data_cache['table_names'] = $retval;

		return $this->data_cache['table_names'];
	}

	public function table_exist($table_name)
	{
		return (!in_array($this->prep_tablename($table_name), $this->list_tables())) ? false : true;
	}

	public function list_fields($table = '')
	{
		if (isset($this->data_cache['field_names'][$table]))
		{
			return $this->data_cache['field_names'][$table];
		}

		if ($table == '')
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_field_param_missing');
			}

			return false;
		}

		$sql = $this->database_list_columns($this->dbprefix . $table);

		if ($sql === false)
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_unsupported_function');
			}

			return false;
		}

		$query = $this->query($sql);

		$retval = array();

		foreach ($query->result_array() as $row)
		{
			if (isset($row['COLUMN_NAME']))
			{
				$retval[] = $row['COLUMN_NAME'];
			}
			else
			{
				$retval[] = current($row);
			}
		}

		$this->data_cache['field_names'][$table] = $retval;

		return $this->data_cache['field_names'][$table];
	}

	public function field_exists($field_names, $table_name)
	{
		return (!in_array($field_names, $this->list_fields($table_name))) ? false : true;
	}

	public function field_names($table = '')
	{
		return $this->list_fields($table);
	}

	public function field_data($table = '')
	{
		if ($table == '')
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_field_param_missing');
			}

			return false;
		}

		$query = $this->query($this->database_field_data($this->dbprefix . $table));

		return $query->field_data();
	}

	public function insert_string($table, $data)
	{
		$fields = array();
		$values = array();

		foreach ($data as $key => $val)
		{
			$fields[] = $key;
			$values[] = $this->escape($val);
		}

		return $this->database_insert($this->dbprefix . $table, $fields, $values);
	}

	public function update_string($table, $data, $where)
	{
		if ($where == '')
		{
			return false;
		}

		$fields = array();

		foreach ($data as $key => $val)
		{
			$fields[$key] = $this->escape($val);
		}

		if (!is_array($where))
		{
			$dest = array($where);
		}
		else
		{
			$dest = array();

			foreach ($where as $key => $val)
			{
				$prefix = (count($dest) == 0) ? '' : ' AND ';

				if ($val != '')
				{
					if (!$this->database_has_operator($key))
					{
						$key .= ' =';
					}

					$val = ' ' . $this->escape($val);
				}

				$dest[] = $prefix . $key . $val;
			}
		}

		return $this->database_update($this->dbprefix . $table, $fields, $dest);
	}

	public function prep_tablename($table = '')
	{
		if ($this->dbprefix != '')
		{
			if (substr($table, 0, strlen($this->dbprefix)) != $this->dbprefix)
			{
				$table = $this->dbprefix . $table;
			}
		}

		return $table;
	}

	public function call_function($function)
	{
		$driver = ($this->dbdriver == 'postgre') ? 'pg_' : $this->dbdriver . '_';

		if (strpos($driver, $function) === false)
		{
			$function = $driver . $function;
		}

		if (!function_exists($function))
		{
			if ($this->db_debug)
			{
				return $this->display_error('database_unsupported_function');
			}

			return false;
		}
		else
		{
			$args = (func_num_args() > 1) ? array_splice(func_get_args(), 1) : null;

			return call_user_func_array($function, $args);
		}
	}

	public function cache_set_path($path = '')
	{
		$this->cachedir = $path;
	}

	public function cache_on()
	{
		$this->cache_on = true;

		return true;
	}

	public function cache_off()
	{
		$this->cache_on = false;

		return false;
	}

	public function cache_delete($segment_one = '', $segment_two = '')
	{
		if (!$this->_cache_init())
		{
			return false;
		}

		return $this->cache->delete($segment_one, $segment_two);
	}

	public function cache_delete_all()
	{
		if (!$this->_cache_init())
		{
			return false;
		}

		return $this->cache->delete_all();
	}

	public function database_cache_init()
	{
		if (is_object($this->cache) && class_exists('Carbon_Database_Cache'))
		{
			return true;
		}

		if (!@include(CARBON_PATH . 'database/Database_cache' . FILE_EXT))
		{
			return $this->cache_off();
		}

		$this->cache = new Carbon_Database_Cache;

		return true;
	}

	public function close()
	{
		if (is_resource($this->conn_id))
		{
			$this->database_close($this->conn_id);
		}

		$this->conn_id = false;
	}

	public function display_error($error = '', $swap = '', $native = false)
	{
		$lang = load_class('Language');
		$lang->load('database');

		if ($native == true)
		{
			$message = $error;
		}
		else
		{
			$message = (!is_array($error)) ? array(str_replace('%s', $swap, $lang->line($error))) : $error;
		}

		$error = load_class('Exception');
		echo $error->display_error('An error has been encountered', $message, 'error_database');
		exit;
	}
}

?>
