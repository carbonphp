<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_mssql_utility extends Carbon_Database_utility
{
	public function database_create_database($name)
	{
		return "CREATE DATABASE " . $name;
	}

	public function database_drop_database($name)
	{
		return 'DROP DATABASE ' . $name;
	}

	public function database_drop_table($table)
	{
		return 'DROP TABLE ' . $this->db->database_escape_table($table);
	}

	public function database_list_databases()
	{
		return "EXEC sp_databases";
	}

	public function database_optimise_table($table)
	{
		return false;
	}

	public function database_repair_table($table)
	{
		return false;
	}

	public function database_backup($params = array())
	{
		return $this->db->display_error('db_unsupported_feature');
	}
}

?>
