<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_mssql_driver extends Carbon_Database
{
	public function database_connect()
	{
		return @mssql_connect($this->hostname, $this->username, $this->password);
	}

	public function database_pconnect()
	{
		return @mssql_pconnect($this->hostname, $this->username, $this->password);
	}

	public function database_select()
	{
		return @mssql_select_db($this->database, $this->conn_id);
	}

	public function database_version()
	{
		return "SELECT @@VERSION AS ver";
	}

	public function database_execute($sql)
	{
		$sql = $this->database_prep_query($sql);
		return @mssql_query($sql, $this->conn_id);
	}

	public function database_prep_query($sql)
	{
		return $sql;
	}

	public function trans_begin($test_mode = false)
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->_trans_failure = ($test_mode === true) ? true : false;
		$this->simple_query('BEGIN TRAN');
		return true;
	}

	public function trans_commit()
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->simple_query('COMMIT TRAN');
		return true;
	}

	public function trans_rollback()
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->simple_query('ROLLBACK TRAN');
		return true;
	}

	public function escape_string($string)
	{
		return str_replace("'", "''", $string);
	}

	public function affected_rows()
	{
		return @mssql_row_affected($this->conn_id);
	}

	public function insert_id()
	{
		$ver = self::database_parse_major_version($this->database_version());
		$sql = ($ver >= 8 ? "SELECT SCOPE_IDENTITY() AS last_id" : "SELECT @@IDENTITY AS last_id");
		$query = $this->query($sql);
		$row = $query->row();
		return $row->last_id;
	}

	private function database_parse_major_version($version)
	{
		preg_match('/([0-9]+)\.([0-9]+)\.([0-9]+)/', $version, $ver_info);
		return $ver_info[1];
	}

	public function count_all($table = '')
	{
		if ($table == '')
		{
			return '0';
		}

		$query = $this->query('SELECT COUNT(*) AS numrows FROM ' . $this->dbprefix . $table);

		if ($query->num_rows() == 0)
		{
			return '0';
		}

		$row = $query->row();
		return $row->numrows;
	}

	public function database_list_tables()
	{
		return 'SELECT name FROM sysobjects WHERE type = \'U\' ORDER BY name';
	}

	public function database_list_columns($table = '')
	{
		return "SELECT * FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" . $this->database_escape_table($table) . "'";
	}

	public function database_field_data($table)
	{
		'SELECT TOP 1 * FROM ' . $this->database_escape_table($table);
	}

	public function database_error_message()
	{
		return '';
	}

	public function database_error_number()
	{
		return '';
	}

	public function database_escape_table($table)
	{
		return $table;
	}

	public function database_insert($table, $keys, $values)
	{
		return "INSERT INTO " . $this->database_escape_table($table) . " (" . implode(', ', $keys) . ") VALUES (" . implode(', ', $values) . ")";
	}

	public function database_update($table, $values, $where)
	{
		foreach($values as $key => $val)
		{
			$valstr[] = $key . " = " . $val;
		}

		return "UPDATE " . $this->database_escape_table($table) . " SET " . implode(', ', $valstr) . " WHERE " . implode(" ", $where);
	}

	public function database_delete($table, $where)
	{
		return "DELETE FROM " . $this->database_escape_table($table) . " WHERE " . implode(" ", $where);
	}

	public function database_limit($sql, $limit, $offset)
	{
		$i = $limit + $offset;
		return preg_replace('/(^\SELECT (DISTINCT)?)/i', '\\1 TOP ' . $i . ' ' . $sql);
	}

	public function database_close($conn_id)
	{
		@mssql_close($conn_id);
	}
}

?>
