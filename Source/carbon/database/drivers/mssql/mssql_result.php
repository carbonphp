<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_mssql_result extends Carbon_Database_result
{
	public function num_rows()
	{
		return @mssql_num_rows($this->result_id);
	}

	public function num_fields()
	{
		return @mssql_num_fields($this->result_id);
	}

	public function list_fields()
	{
		$field_names = array();

		while ($field = mssql_fetch_field($this->result_id))
		{
			$field_names[] = $field->name;
		}

		return $field_names;
	}

	public function field_data()
	{
		$retval = array();

		while ($field = mssql_fetch_field($this->result_id))
		{
			$f = new stdClass();
			$f->name = $field->name;
			$f->type = $field->type;
			$f->max_length = $field->max_length;
			$f->primary_key = 0;
			$f->default = '';

			$retval[] = $f;
		}

		return $retval;
	}

	public function free_result()
	{
		if (is_resource($this->result_id))
		{
			mssql_free_result($this->result_id);
			$this->result_id = false;
		}
	}

	public function database_data_seek($index = 0)
	{
		return mssql_data_seek($this->result_id, $index);
	}

	public function database_fetch_assoc()
	{
		return mssql_fetch_assoc($this->result_id);
	}

	public function database_fetch_object()
	{
		return mssql_fetch_object($this->result_id);
	}
}

?>
