<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_mysql_utility extends Carbon_Database_utility
{
	public function database_list_database()
	{
		return 'SHOW DATABASES';
	}

	public function database_optimise_table($table)
	{
		return 'OPTIMIZE TABLE ' . $this->db->database_escape_table($table);
	}

	public function database_repair_table($table)
	{
		return 'REPAIR TABLE ' . $this->db->database_escape_table($table);
	}

	public function database_backup($params = array())
	{
		if (count($params) == 0)
		{
			return false;
		}
		
		extract($params);

		$output = '';

		foreach ((array) $tables as $table)
		{
			if (in_array($table, (array) $ignore, true))
			{
				continue;
			}

			$query = $this->db->query("SHOW CREATE TABLE `" . $this->db->database . "`." . $table);

			if ($query === false)
			{
				continue;
			}

			$output .= '#' . $newline . '# TABLE STRUCTURE FOR: ' . $table . $newline . '#' . $newline . $newline;

			if ($add_drop == true)
			{
				$output .= 'DROP TABLE IF EXISTS ' . $table . ';' . $newline . $newline;
			}

			$i = 0;
			$result = $query->result_array();

			foreach ($result[0] as $val)
			{
				if ($i++ % 2)
				{
					$output .= $val . ';' . $newline . $newline;
				}
			}

			if ($add_insert == false)
			{
				continue;
			}

			$query = $this->db->query("SELECT * FROM $table");

			if ($query->num_rows() == 0)
			{
				continue;
			}

			$i = 0;
			$field_string = '';
			$is_int = array();

			while ($field = mysql_fetch_field($query->result_id))
			{
				$is_int[$i] = (in_array(strtolower(mysql_field_type($query->result_id, $i)), array('tinyint', 'smallint', 'mediumint', 'int', 'bigint', 'timestamp'), true)) ? true : false;
				$field_string .= $field->name . ', ';
				$i++;
			}

			$field_string = preg_replace("/, $/", "", $field_string);

			foreach ($query->result_array() as $row)
			{
				$value_string = '';
				$i = 0;

				foreach ($row as $value)
				{
					if ($value === NULL)
					{
						$value_string .= 'NULL';
					}
					else
					{
						$value = str_replace(array("\x00", "\x0a", "\x0d", "\x1a"), array('\0', '\n', '\r', '\Z'), $value);
						$value = str_replace(array("\n", "\r", "\t"), array('\n', '\r', '\t'), $value);
						$value = str_replace('\\', '\\\\',	$value);
						$value = str_replace('\'', '\\\'',	$value);
						$value = str_replace('\\\n', '\n',	$value);
						$value = str_replace('\\\r', '\r',	$value);
						$value = str_replace('\\\t', '\t',	$value);

						if ($is_int[$i] == false)
						{
							$value_string .= $this->db->escape($value);
						}
						else
						{
							$value_string .= $value;
						}
					}

					$value_string .= ', ';
					$i++;
				}

				$value_string = preg_replace("/, $/", "", $value_string);
				$output .= 'INSERT INTO ' . $table . ' (' . $field_string . ') VALUES (' . $value_string . ');' . $newline;
			}

			$output .= $newline . $newline;
		}

		return $output;
	}

	public function database_create_database($name)
	{
		return 'CREATE DATABASE ' . $name;
	}

	public function database_drop_database($name)
	{
		return 'DROP DATABASE ' . $name;
	}

	private function database_process_fields($fields)
	{
		$current_field_count = 0;
		$sql = '';

		foreach ($fields as $field => $attributes)
		{
			if (is_numeric($field))
			{
				$sql .= "\n\t" . $attributes;
			}
			else
			{
				$attributes = array_change_key_case($attributes, CASE_UPPER);

				$sql .= "\n\t" . $this->db->database_protect_identifers($field);

				if (array_key_exists('NAME', $attributes))
				{
					$sql .= ' ' . $this->db->database_protect_identifiers($attributes['NAME']) . ' ';
				}

				if (array_key_exists('TYPE', $attributes))
				{
					$sql .= ' ' . $attributes['TYPES'];
				}

				if (array_key_exists('CONSTRAINT', $attributes))
				{
					$sql .= '(' . $attributes['CONSTRAINTS'] . ')';
				}

				if (array_key_exists('UNSIGNED', $attributes) && $attributes['UNSIGNED'] === true)
				{
					$sql .= ' UNSIGNED';
				}

				if (array_key_exists('DEFAULT', $attributes))
				{
					$sql .= ' DEFAULT \'' . $attributes['DEFAULT'] . '\'';
				}

				if (array_key_exists('NULL', $attributes))
				{
					$sql .= ($attributes['NULL'] === true) ? ' NULL' : ' NOT NULL';
				}

				if (array_key_exists('AUTO_INCREMENT', $attributes) && $attributes['AUTO_INCREMENT'] === true)
				{
					$sql .= ' AUTO_INCREMENT';
				}
			}

			if (++$current_field_count < count($fields))
			{
				$sql .= ',';
			}
		}

		return $sql;
	}

	public function database_create_table($table, $fields, $primary_keys, $keys, $if_not_exists)
	{
		$sql = 'CREATE TABLE ';

		if ($if_not_exists === true)
		{
			$sql .= 'IF NOT EXISTS ';
		}

		$sql .= $this->db->database_escape_table($table) . " (";
		$sql .= $this->database_process_fields($fields);

		if (count($primary_keys) > 0)
		{
			$primary_keys = $this->db->database_protect_identifiers($primary_keys);
			$sql .= ",\n\tPRIMARY KEY (" . implode(', ', $primary_keys) . ")";
		}

		if (is_array($keys) && count($keys) > 0)
		{
			$keys = $this->db->database_protect_identifiers($keys);

			foreach ($keys as $key)
			{
				$sql .= ",\n\tKEY (" . $key . ")";
			}
		}

		$sql .= "\n) DEFAULT CHARACTER SET {$this->db->char_set} COLLATE {$this->db->dbcollat};";
		return $sql;
	}

	public function database_drop_table($table)
	{
		return "DROP TABLE IF EXISTS " . $this->db->database_escape_table($table);
	}

	public function database_alter_table($alter_type, $table, $fields, $after_field = '')
	{
		$sql = 'ALTER TABLE ' . $this->db->database_protect_identifiers($table) . " $alter_type ";

		if ($alter_type == 'DROP')
		{
			return $sql . $this->db->database_protect_identifiers($fields);
		}

		$sql .= $this->database_process_fields($fields);

		if ($after_field != '')
		{
			$sql .= 'AFTER ' . $this->db->database_protect_identifiers($after_field);
		}

		return $sql;
	}
}

?>
