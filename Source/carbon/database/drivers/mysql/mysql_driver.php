<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_mysql_driver extends Carbon_Database
{
	public $delete_hack = true;
	public $count_string = 'SELECT COUNT(*) AS ';
	public $random_keyword = ' RAND()'; 

	public function database_connect()
	{
		return @mysql_connect($this->hostname, $this->username, $this->password, true);
	}

	public function database_pconnect()
	{
		return @mysql_pconnect($this->hostname, $this->username, $this->password);
	}

	public function database_select()
	{
		return @mysql_select_db($this->database, $this->conn_id);
	}

	public function database_set_charset($charset, $collation)
	{
		return @mysql_query("SET NAMES '" . $this->escape_string($charset) . "' COLLATE '" . $this->escape_string($collation) . "'", $this->conn_id);
	}

	public function database_version()
	{
		return "SELECT version() AS ver";
	}

	public function database_execute($sql)
	{
		$sql = $this->database_prep_query($sql);
		return @mysql_query($sql, $this->conn_id);
	}

	public function database_prep_query($sql)
	{
		if ($this->delete_hack === true)
		{
			if (preg_match('/^\s*DELETE\s+FROM\s+(\S+)\s*$/i', $sql))
			{
				$sql = preg_replace("/^\s*DELETE\s+FROM\s+(\S+)\s*$/", "DELETE FROM \\1 WHERE 1=1", $sql);
			}
		}

		return $sql;
	}

	public function trans_begin($test_mode = false)
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->_trans_failure = ($test_mode === true) ? true : false;

		$this->simple_query('SET AUTOCOMMIT=0');
		$this->simple_query('START TRANSACTION');
		return true;
	}

	public function trans_commit()
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->simple_query('COMMIT');
		$this->simple_query('SET AUTOCOMMIT=1');
		return true;
	}

	public function trans_rollback()
	{
		if (!$this->trans_enabled)
		{
			return true;
		}

		if ($this->_trans_depth > 0)
		{
			return true;
		}

		$this->simple_query('ROLLBACK');
		$this->simple_query('SET AUTOCOMMIT=1');
	}

	public function escape_string($string)
	{
		if (is_array($string))
		{
			foreach ($string as $key => $value)
			{
				$string[$key] = $this->escape_string($value);
			}

			return $string;
		}

		if (function_exists('mysql_real_escape_string'))
		{
			return mysql_real_escape_string($string, $this->conn_id);
		}
		elseif (function_exists('mysql_escape_string'))
		{
			return mysql_escape_string($string);
		}
		else
		{
			return addslashes($string);
		}
	}

	public function affected_rows()
	{
		return @mysql_affected_rows($this->conn_id);
	}

	public function insert_id()
	{
		return @mysql_insert_id($this->conn_id);
	}

	public function count_all($table = '')
	{
		if ($table == '')
		{
			return '0';
		}

		$query = $this->query($this->count_string . $this->database_protect_identifiers('numrows'). " FROM " . $this->database_protect_identifiers($this->dbprefix . $table));

		if ($query->num_rows() == 0)
		{
			return '0';
		}

		$row = $query->row();
		return $row->numrows;
	}

	public function database_list_tables($prefix_limit = false)
	{
		$sql = "SHOW TABLES FROM '" . $this->database . "'";

		if ($prefix_limit !== false && $this->dbprefix != '')
		{
			$sql .= " LIKE '" . $this->dbprefix . "%'";
		}

		return $sql;
	}

	public function database_list_columns($table = '')
	{
		return "SHOW COLUMNS FROM " . $this->database_escape_table($table);
	}

	public function database_field_data($table)
	{
		return "SELECT * FROM " . $this->database_escape_table($table) . " LIMIT 1";
	}

	public function database_error_message()
	{
		return mysql_error($this->conn_id);
	}

	public function database_error_number()
	{
		return mysql_errno($this->conn_id);
	}

	public function database_escape_table($table)
	{
		if (stristr($table, '.'))
		{
			$table = '`' . str_replace('.', '`.`', $table) . '`';
		}

		return $table;
	}

	public function database_protect_identifiers($item, $first_word_only = false)
	{
		if (is_array($item))
		{
			$escaped_array = array();

			foreach ($item as $k => $v)
			{
				$escaped_array[$this->database_protect_identifiers($k)] = $this->database_protect_identifiers($v, $first_word_only);
			}

			return $escaped_array;
		}

		if (ctype_alnum($item) === false)
		{
			if (strpos($item, '.') !== false)
			{
				$aliased_tables = implode(".", $this->ar_aliased_tables) . '.';
				$table_name = substr($item, 0, strpos($item, '.') + 1);
				$item = (strpos($aliased_tables, $table_name) !== false) ? $item = $item : $this->dbprefix . $item;
			}

			$lbound = ($first_word_only === true) ? '' : '|\s|\(';
			$item = preg_replace('/(^' . $lbound . ')([\w\d\-\_]+?)(\s|\)|$)/iS', '$1`$2`$3', $item);
		}
		else
		{
			return "`{$item}`";
		}

		$exceptions = array('AS', '/', '-', '%', '+', '*');

		foreach ($exceptions as $exception)
		{
			if (stristr($item, " `{$exception}` ") !== false)
			{
				$item = preg_replace('/ `(' . preg_quote($exception) . ')` /i', '$1 ', $item);
			}
		}

		return $item;
	}

	public function database_from_tables($tables)
	{
		if (!is_array($tables))
		{
			$tables = array($tables);
		}

		return '(' . implode(', ', $tables) . ')';
	}

	public function database_insert($table, $key, $values)
	{
		return "INSERT INTO " . $this->database_escape_table($table) . " (" . implode(', ', $key) . ") VALUES (" . implode(', ', $values) . ")";
	}

	public function database_update($table, $values, $where, $orderby = array(), $limit = false)
	{
		foreach($values as $key => $val)
		{
			$valstr[] = $key . " = " . $val;
		}

		$limit = (!$limit) ? '' : ' LIMIT ' . $limit;

		$orderby = (count($orderby) >= 1) ? ' ORDER BY ' . implode(', ', $orderby) : '';

		$sql = "UPDATE " . $this->database_escape_table($table) . " SET " . implode(', ', $valstr);
		$sql .= ($where != '' && count($where) >= 1) ? " WHERE " . implode(' ', $where) : '';
		$sql .= $orderby . $limit;

		return $sql;
	}

	public function database_truncate($table)
	{
		return "TRUNCATE " . $this->database_escape_table($table);
	}

	public function database_delete($table, $where, $like = array(), $limit = false)
	{
		$conditions = '';

		if (count($where) > 0 || count($like) > 0)
		{
			$conditions = "\nWHERE ";
			$conditions .= implode("\n", $this->ar_where);

			if (count($where) > 0 && count($like) > 0)
			{
				$conditions .= " AND ";
			}

			$conditions .= implode("\n", $like);
		}

		$limit = (!$limit) ? '' : " LIMIT " . $limit;

		return "DELETE FROM " . $table . $conditions . $limit;
	}

	public function database_limit($sql, $limit, $offset)
	{
		if ($offset == 0)
		{
			$offset = '';
		}
		else
		{
			$offset .= ", ";
		}

		return $sql . "LIMIT " . $offset . $limit;
	}

	public function database_close($conn_id)
	{
		@mysql_close($conn_id);
	}
}

?>
