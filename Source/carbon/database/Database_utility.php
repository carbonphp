<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Database_utility
{
	public $db;
	public $data_cache = array();
	public $keys = array();
	public $primary_keys = array();
	public $db_char_set = '';

	public function __construct()
	{
		log_message('debug', 'Database_utility.php - Carbon_Database_utility class initialised');

		$carbon = get_instance();
		$this->db = $carbon->db;
	}

	public function create_database($db_name)
	{
		$sql = $this->database_create_database($db_name);

		if (is_bool($sql))
		{
			return $sql;
		}

		return $this->db->query($sql);
	}

	public function drop_database($db_name)
	{
		$sql = $this->database_drop_database($db_name);

		if (is_bool($sql))
		{
			return $sql;
		}

		return $this->db->query($sql);
	}

	public function add_key($key = '', $primary = false)
	{
		if ($key == '')
		{
			display_error('Key information is required for this method');
		}

		if ($pimary === true)
		{
			$this->primary_keys[] = $key;
		}
		else
		{
			$this->keys[] = $key;
		}
	}

	public function add_field($field = '')
	{
		if ($field == '')
		{
			display_error('Field information is required for this method');
		}

		if (is_string($field))
		{
			if ($field == 'id')
			{
				$this->fields[] = array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 9,
						'auto_increment' => true
					)
				);

				$this->add_key('id', true);
			}
			else
			{
				if (strpos($field, ' ') === false)
				{
					display_error('Field information is required for this method');
				}

				$this->fields[] = $field;
			}
		}

		if (is_array($field))
		{
			$this->fields = array_merge($this->fields, $field);
		}
	}

	public function create_table($table = '', $if_not_exists = false)
	{
		if ($table == '')
		{
			display_error('A table name is required for this method');
		}

		if (count($this->fields) == 0)
		{
			display_error('Field information is required for this method');
		}

		$sql = $this->database_create_table($this->db->dbprefix . $table, $this->fields, $this->primary_keys, $this->keys, $if_not_exists);

		$this->database_reset();
		return $this->db->query($sql);
	}

	public function drop_table($table)
	{
		$sql = $this->database_drop_table($this->db->dbprefix . $table);

		if (is_bool($sql))
		{
			return $sql;
		}

		return $this->db->query($sql);
	}

	public function add_column($table = '', $field = array(), $after_field = '')
	{
		if ($table == '')
		{
			display_error('A table name is required for this method');
		}

		$this->add_field(array_slice($field, 0, 1));

		if (count($this->fields) == 0)
		{
			display_error('Field information is required for this method');
		}

		$sql = $this->database_alter_table('ADD', $this->db->dbprefix . $table, $this->fields, $after_field);

		$this->database_reset();
		return $this->db->query($sql);
	}

	public function drop_column($table = '', $column_name = '')
	{
		if ($table == '')
		{
			display_error('A table name is required for this method');
		}

		if ($column_name == '')
		{
			display_error('A column name is required for this method');
		}

		$sql = $this->database_alter_table('DROP', $this->db->dbprefix . $table, $column_name);

		return $this->db->query($sql);
	}

	public function modify_column($table = '', $field = array())
	{
		if ($table == '')
		{
			display_error('A table name is required for this method');
		}

		$this->add_field(array_slice($field, 0, 1));

		if (count($this->fields) == 0)
		{
			display_error('Field information is required for this method');
		}

		$sql = $this->database_alter_table('CHANGE', $this->db->dbprefix . $table, $this->fields);

		$this->database_reset();
		return $this->db->query($sql);
	}

	private function database_reset()
	{
		$this->fields = array();
		$this->keys = array();
		$this->primary_keys = array();
	}

	public function list_databases()
	{
		if (isset($this->data_cache['db_names']))
		{
			return $this->data_cache['db_names'];
		}

		$query = $this->db->query($this->database_list_databases());
		$dbs = array();

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$dbs[] = current($row);
			}
		}

		$this->data_cache['db_names'] = $dbs;

		return $this->data_cache['db_names'];
	}

	public function optimize_table($table_name)
	{
		$sql = $this->database_optimize_table($table_name);

		if (is_bool($sql))
		{
			return $sql;
		}

		$query = $this->db->query($sql);
		$result = $query->result_array();

		return current($result);
	}

	public function optimize_database()
	{
		$result = array();

		foreach ($this->db->list_tables() as $table_name);
		{
			$sql = $this->database_optimize_table($table_name);

			if (is_bool($sql))
			{
				return $sql;
			}

			$query = $this->db->query($sql);

			$res = $query->result_array();
			$res = current($res);
			$key = str_replace($this->db->database . '.', '', current($res));
			$keys = array_keys($res);
			unset($res[$keys[0]]);

			$result[$key] = $result;
		}

		return $result;
	}

	public function repair_table($table_name)
	{
		$sql = $this->database_repair_table($table_name);

		if (is_bool($sql))
		{
			return $sql;
		}

		$sql = $this->db->query($sql);

		$result = $query->result_array();
		return current($result);
	}

	public function csv_from_result($query, $delim = ",", $newline = "\n", $enclosure = '"')
	{
		if (!is_object($query) || !method_exists($query, 'field_data'))
		{
			display_error('You must supply a valid result object for this method');
		}

		$output = '';

		foreach ($query->list_fields() as $name)
		{
			$output .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $name) . $enclosure . $delim;
		}

		$output = rtrim($output);
		$output .= $newline;

		foreach ($query->result_array() as $row)
		{
			foreach ($row as $item)
			{
				$output .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $item) . $enclosure . $delim;
			}

			$output = rtrim($output);
			$output .= $newline;
		}

		return $output;
	}

	public function xml_from_result($query, $params = array())
	{
		if (!is_object($query) || !method_exists($query, 'field_data'))
		{
			display_error('You must supply a valid result object for this method');
		}
		
		$root = $element = $newline = $tab = '';

		foreach (array('root' => 'root', 'element' => 'element', 'newline' => "\n", 'tab' => "\t") as $key => $val)
		{
			if (!isset($params[$key]))
			{
				$params[$key] = $val;
			}
		}

		extract($params);

		$carbon = get_instance();
		$carbon->load->utility('xml');

		$xml = "<{$root}>" . $newline;

		foreach ($query->result_array() as $row)
		{
			$xml .= $tab . "<{$element}>" . $newline;

			foreach ($row as $key => $val)
			{
				$xml .= $tab . $tab . "<{$key}>" . xml_entities($val) . "</{$key}>" . $newline;
			}

			$xml .= $tab . "</{$element}>" . $newline;
		}

		$xml .= "</{$root}>" . $newline;

		return $xml;
	}

	public function backup($params = array())
	{
		if (is_string($params))
		{
			$params = array('table' => $params);
		}

		$prefs = array(
			'tables' => array(),
			'ignore' => array(),
			'filename' => '',
			'format' => 'gzip',
			'add_drop' => true,
			'add_insert' => true,
			'newline' => "\n"
		);

		if (count($params) > 0)
		{
			foreach ($prefs as $key => $val)
			{
				if (isset($params[$key]))
				{
					$prefs[$key] = $params[$key];
				}
			}
		}

		if (count($prefs['tables']) == 0)
		{
			$prefs['tables'] = $this->db->list_tables();
		}

		if (!in_array($prefs['format'], array('gzip', 'zip', 'txt'), true))
		{
			$prefs['format'] = 'txt';
		}

		if (($prefs['format'] == 'gzip' && !@function_exists('gzencode')) || ($prefs['format'] == 'zip' && !@function_exists('gzcompress')))
		{
			if ($this->db->db_debug)
			{
				return $this->db->display_error('database_unsupported_compression');
			}

			$prefs['format'] = 'txt';
		}

		if ($prefs['filename'] == '' && $prefs['format'] == 'zip')
		{
			$prefs['filename'] = (count($prefs['tables']) == 1) ? $prefs['tables'] : $this->db->database;
			$prefs['filename'] .= '_' . date('Y-m-d_H-i', time());
		}

		if ($prefs['format'] == 'gzip')
		{
			return gzencode($this->database_backup($prefs));
		}

		if ($prefs['format'] == 'txt')
		{
			return $this->database_backup($prefs);
		}

		if ($prefs['format'] == 'zip')
		{
			if (preg_match("|.+?\.zip$|", $prefs['filename']))
			{
				$prefs['filename'] = str_replace('.zip', '', $prefs['filename']);
			}

			if (!preg_match("|.+?\.sql$|", $prefs['filename']))
			{
				$prefs['filename'] .= '.sql';
			}

			$carbon = get_instance();
			$carbon->load->library('zip');
			$carbon->zip->add_data($prefs['filename'], $this->database_backup($prefs));

			return $carbon->zip->get_zip();
		}
	}
}

?>
