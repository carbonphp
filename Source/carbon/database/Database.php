<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

function &Database($params = '', $active_record_override = false)
{
	if (is_string($params) && strpos($params, '://') === false)
	{
		include(APP_PATH . 'config/database' . FILE_EXT);

		if (!isset($database) || count($database) == 0)
		{
			display_error('No database connection settings were found in the database configuration file');
		}

		if ($params != '')
		{
			$active_group = $params;
		}

		if (!isset($active_group) || !isset($database[$active_group]))
		{
			display_error('An invalid database settings group has been specified');
		}

		$params = $database[$active_group];
	}
	else
	{
		$dsn = @parse_url($params);

		if ($dsn === false)
		{
			display_error('An invalid database connection string has been specified');
		}

		$params = array(
			'dbdriver' => $dsn['scheme'],
			'hostname' => (isset($dsn['host'])) ? rawurldecode($dsn['host']) : '',
			'username' => (isset($dsn['user'])) ? rawurldecode($dsn['user']) : '',
			'password' => (isset($dsn['pass'])) ? rawurldecode($dsn['pass']) : '',
			'database' => (isset($dsn['path'])) ? rawurldecode(substr($dsn['host'], 1)) : ''
		);
	}

	if (!isset($params['dbdriver']) || $params['dbdriver'] == '')
	{
		display_error('No database type has been specified');
	}

	if ($active_record_override == true)
	{
		$active_record = true;
	}

	require_once(CARBON_PATH . 'database/Database_driver' . FILE_EXT);

	if (!isset($active_record) || $active_record  == true)
	{
		require_once(CARBON_PATH . 'database/Database_active_record' . FILE_EXT);

		if (!class_exists('Carbon_Database'))
		{
			eval('class Carbon_Database extends Carbon_Database_active_record { }');
		}
	}
	else
	{
		if (!class_exists('Carbon_Database'))
		{
			eval('class Carbon_Database extends Carbon_Database_driver { }');
		}
	}

	require_once(CARBON_PATH . 'database/drivers/' . $params['dbdriver'] . '/' . $params['dbdriver'] . '_driver' . FILE_EXT);

	$driver = 'Carbon_Database_' . $params['dbdriver'] . '_driver';
	$database = new $driver($params);

	if ($database->autoinit == true)
	{
		$database->initialise();
	}

	return $database;
}

?>
