<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

/*------------------------------------------------------------
*  This is an array of libraries you would like to be loaded
*  automatically.
*------------------------------------------------------------*/
$autoload['libraries'] = array();

/*------------------------------------------------------------
*  This is an array of utilities you would like to be loaded
*  automatically.
*------------------------------------------------------------*/
$autoload['utility'] = array();

/*------------------------------------------------------------
*  This is an array of configuration files you would like to
*  be loaded automatically.
*------------------------------------------------------------*/
$autoload['config'] = array();

/*------------------------------------------------------------
*  This is an array of language files you would like to be
*  loaded automatically.
*------------------------------------------------------------*/
$autoload['language'] = array();

?>
