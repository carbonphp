<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

/*------------------------------------------------------------
*  The active configuration group to use.
*------------------------------------------------------------*/
$active_group = 'default';

/*------------------------------------------------------------
*  These are the details used to connect to, and select the
*  database.
*------------------------------------------------------------*/
$database['default']['hostname'] = '';
$database['default']['username'] = '';
$database['default']['password'] = '';
$database['default']['database'] = '';

/*------------------------------------------------------------
*  This is the database driver you wish to use when
*  connecting to the database.
*------------------------------------------------------------*/
$database['default']['dbdriver'] = 'mysql';

/*------------------------------------------------------------
*  This is the prefix for any tables if they are prefixed.
*------------------------------------------------------------*/
$database['default']['dbprefix'] = '';

/*------------------------------------------------------------
*  Whether you wish to use the active record class or not.
*------------------------------------------------------------*/
$database['default']['active_r'] = true;

/*------------------------------------------------------------
*  Whether you wish to persistantly conenct to the database.
*------------------------------------------------------------*/
$database['default']['pconnect'] = true;

/*------------------------------------------------------------
*  Whether you wish to show database error messages.
*------------------------------------------------------------*/
$database['default']['db_debug'] = true;

/*------------------------------------------------------------
*  Whether you wish to cache query data, and the path to
*  where the cached data should be stored.
*------------------------------------------------------------*/
$database['default']['cache_on'] = false;
$database['default']['cachedir'] = CARBON_PATH . 'cache/';

?>
