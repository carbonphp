<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

/*------------------------------------------------------------
*  This extension point is called during the early
*  execution of the system.  The only system classes loaded
*  are the benchmark, and extensions classes.
*------------------------------------------------------------*/
$extension['pre_system'] = array();

/*------------------------------------------------------------
*  This extension point is called before any controllers
*  are called.  All of the system classes have been loaded
*  as well as routing, and security checks.
*------------------------------------------------------------*/
$extension['pre_controller'] = array();

/*------------------------------------------------------------
*  This extension point is called immediately after the
*  controller constructor has been called, but before any
*  method is called.
*------------------------------------------------------------*/
$extension['post_controller_constructor'] = array();

/*------------------------------------------------------------
*  This extension point is called immediately after the
*  controller has been fully executed.
*------------------------------------------------------------*/
$extension['post_controller'] = array();

/*------------------------------------------------------------
*  This extension point override the c_display() function.
*  Which is used to send the final output to the browser.
*  This means you can use your own methodology.  You can
*  get the final output by calling $this->output->get_output().
*------------------------------------------------------------*/
$extension['display_override'] = array();

/*------------------------------------------------------------
*  This extension point overrides the c_display_cache()
*  function.  This allows you to use your own cache display
*  methodology.
*------------------------------------------------------------*/
$extension['cache_override'] = array();

/*------------------------------------------------------------
*  This extension point overrides the c_scaffolding()
*  functions.  It allows you to call your own script when
*  scaffolding is requested.
*------------------------------------------------------------*/
$extension['scaffolding_override'] = array();

/*------------------------------------------------------------
*  This extension point is called after the final page
*  rendered has been sent to the browser, right as the end
*  of the system execution after the data is sent.
*------------------------------------------------------------*/
$extension['post_system'] = array();

?>
