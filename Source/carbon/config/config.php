<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

$config['carbon_url'] = 'http://localhost/';

$config['index_page'] = 'index.php';

$config['uri_protocol'] = 'AUTO';

$config['url_postfix'] = '';

$config['language'] = 'english';

$config['charset'] = 'UTF-8';

$config['enable_extensions'] = false;

$config['subclass_prefix'] = 'Sub_';

$config['allowed_uri_chars'] = 'a-z 0-9~%.:_-';

$config['use_query_strings'] = false;
$config['controller_trigger'] = 'c';
$config['method_trigger'] = 'm';

$config['logging_threshold'] = 0;

$config['logging_path'] = '';

$config['logging_date_format'] = 'Y-m-d H:i:s';

$config['cache_path'] = '';

$config['encryption_key'] = '';

$config['session_cookie_name'] = 'carbon_session';
$config['session_expiration'] = 7200;
$config['session_encrypt_cookie'] = false;
$config['session_use_database'] = false;
$config['session_table_name'] = 'carbon_sessions';
$config['session_match_ip'] = false;
$config['session_match_useragent'] = true;

$config['cookie_prefix'] = '';
$config['cookie_domain'] = '';
$config['cookie_path'] = '/';

$config['use_xss_filtering'] = false;

$config['compress_output'] = false;

$config['master_time'] = 'local';

$config['short_tag_rewrite'] = false;

?>
