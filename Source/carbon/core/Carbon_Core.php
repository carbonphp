<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

$php_version = floor(phpversion());

if ($php_version < 5)
{
	die('PHP version 5 or higher is required to run CarbonPHP');
}

require(CARBON_PATH . 'core/Common' . FILE_EXT);

set_error_handler('exception_handler');

$bench =& load_class('Benchmark');
$bench->mark('total_execution_time_start');
$bench->mark('total_base_class_loading_time_start');

$ext =& load_class('Extensions');
$ext->_call_extension('pre_system');

$config =& load_class('Config');
$uri =& load_class('Uri');
$router =& load_class('Router');
$output =& load_class('Output');

if ($ext->_call_extension('cache_override') === false)
{
	if ($output->_display_cache($config, $router) == true)
	{
		exit();
	}
}

$input =& load_class('Input');
$lang =& load_class('Language');

require(CARBON_PATH . 'core/Base' . FILE_EXT);

load_class('Controller', false);

if (!@include(APP_PATH . 'controllers/' . $router->get_directory_name() . $router->get_class_name() . FILE_EXT))
{
	display_error('Unable to load the default controller. Make sure that the default controller in the routing.php file is correct');
}

$bench->mark('total_base_class_loading_time_end');

$class_name = $router->get_class_name();
$method_name = $router->get_method_name();

if (!class_exists($class_name) || $method_name == 'controller' || substr($method_name, 0, 1) == '_' || in_array($method_name, get_class_methods('Controller'), true))
{
	display_not_found();
}

$ext->_call_extension('pre_controller');

$bench->mark('controller_execution_time_(' . $class_name . ' / ' . $method_name . ')_start');

$carbon = new $class_name();

if ($router->is_scaffolding_request() === true)
{
	if ($ext->_call_extension('scaffolding_override') === false)
	{
		$carbon->_scaffolding();
	}
}
else
{
	$ext->_call_extension('post_controller_constructor');

	if (method_exists($carbon, '_remap'))
	{
		$carbon->_remap($method_name);
	}
	else
	{
		if (!method_exists($carbon, $method_name))
		{
			display_not_found();
		}

		call_user_func_array(array(&$carbon, $method_name), array_slice($uri->get_rsegment_array(), 2));
	}
}

$bench->mark('controller_execution_time_(' . $class_name . ' / ' . $method_name . ')_end');

$ext->_call_extension('post_controller');

if ($ext->_call_extension('display_override') === false)
{
	$output->display_output();
}

$ext->_call_extension('post_system');

if (class_exists('Carbon_Database') && isset($carbon->db))
{
	$carbon->db->close();
}

?>
