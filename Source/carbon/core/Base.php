<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Base
{
	protected static $instance;

	public function __construct()
	{
		self::$instance = $this;
	}

	public static function & get_instance()
	{
		return self::$instance;
	}
}

function & get_instance()
{
	return Carbon_Base::get_instance();
}

?>
