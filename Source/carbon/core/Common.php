<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

function & load_class($class_name, $instantiate = true)
{
	static $class_objects = array();

	if (isset($class_objects[$class_name]))
	{
		return $class_objects[$class_name];
	}

	if (file_exists(APP_PATH . 'libraries/' . get_config_item('subclass_prefix') . $class_name . FILE_EXT))
	{
		require(CARBON_PATH . 'libraries/' . $class_name . FILE_EXT);
		require(APP_PATH . 'libraries/' . get_config_item('subclass_prexfix') . $class_name . FILE_EXT);
		$is_subclass = true;
	}
	else
	{
		if (file_exists(APP_PATH . 'libraries/' . $class_name . FILE_EXT))
		{
			require(APP_PATH . 'libraries/' . $class_name . FILE_EXT);
			$is_subclass = false;
		}
		else
		{
			require(CARBON_PATH . 'libraries/' . $class_name . FILE_EXT);
			$is_subclass = false;
		}
	}

	if ($instantiate == false)
	{
		$class_objects[$class_name] = true;
		return $class_objects[$class_name];
	}

	if ($is_subclass == true)
	{
		$full_class_name = get_config_item('subclass_prefix') . $class_name;
		$class_objects[$class_name] = new $full_class_name();
		return $class_objects[$class_name];
	}

	$full_class_name = ($class_name != 'Controller') ? 'Carbon_' . $class_name : $class_name;
	$class_objects[$class_name] = new $full_class_name();
	return $class_objects[$class_name];
}

function & load_config()
{
	static $carbon_config;

	if (isset($carbon_config))
	{
		return $carbon_config[0];
	}

	if (!file_exists(APP_PATH . 'config/config' . FILE_EXT))
	{
		exit('The CarbonPHP configuration file does not exist: config' . FILE_EXT);
	}

	require(APP_PATH . 'config/config' . FILE_EXT);

	if (!isset($config) || !is_array($config))
	{
		exit('The CarbonPHP configuration file is incorrectly formatted: config' . FILE_EXT);
	}

	$carbon_config[0] =  $config;
	return $carbon_config[0];
}

function get_config_item($config_item)
{
	static $config_items = array();

	if (isset($config_items[$config_item]))
	{
		return $config_items[$config_item];
	}

	$config =& load_config();

	if (!isset($config[$config_item]))
	{
		return false;
	}

	$config_items[$config_item] = $config[$config_item];
	return $config_items[$config_item];
}

function display_error($error_message)
{
	$exception =& load_class('Exception');
	echo $exception->display_error('An error has been encountered', $error_message);
	exit();
}

function display_not_found($page = '')
{
	$exception =& load_class('Exception');
	$exception->display_not_found($page);
	exit();
}

function log_message($level = 'error', $message, $php_error = false)
{
	static $logging;
	$config =& load_config();

	if (get_config_item('logging_threshold') == 0)
	{
		return false;
	}

	$logging =& load_class('Logging');
	$logging->write_log_message($level, $message, $php_error);
}

function exception_handler($severity, $message, $file_path, $line_number)
{
	if ($severity == E_STRICT)
	{
		return true;
	}

	$exception =& load_class('Exception');

	if (($severity & error_reporting()) == $severity)
	{
		$exception->display_php_error($severity, $message, $file_path, $line_number);
	}

	$config =& load_config();

	if (get_config_item('logging_threshold') == 0)
	{
		return true;
	}

	$exception->log_exception($severity, $message, $file_path, $line_number);
}

?>
