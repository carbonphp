<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Scaffolding
{
	private $carbon;
	private $current_table;
	private $base_url = '';
	private $lang = array();

	public function __construct($db_table)
	{
		log_message('debug', 'Scaffolding.php - Scaffolding class initialised');

		$this->carbon = get_instance();

		$this->carbon->load->database('', false, true);
		$this->carbon->load->library('pagination');

		$this->carbon->db->cache_off();

		$this->current_table = $db_table;

		$this->carbon->load->set_view_path(CARBON_PATH . 'scaffolding/views/');

		$this->base_url = $this->carbon->config->get_site_url() . '/' . $this->carbon->uri->segment(1) . $this->carbon->uri->slash_segment(2, 'both');
		$this->base_uri = $this->carbon->uri->segment(1) . $this->carbon->uri->slash_segment(2, 'leading');

		$data = array(
			'image_url' => $this->carbon->config->get_system_url() . 'scaffolding/images/',
			'base_uri' => $this->base_uri,
			'base_url' => $this->base_url,
			'title' => $this->current_table
		);

		$this->carbon->load->vars($data);

		$this->lang = $this->carbon->load->scaffold_language('scaffolding', '', true);
		$this->carbon->load->vars($this->lang);

		$this->carbon->load->utility(array('url', 'form'));
	}

	public function add()
	{
		$data = array(
			'title' => (!isset($this->lang['scaff_add'])) ? 'Add Data' : $this->lang['scaff_add'],
			'fields' => $this->carbon->db->field_data($this->current_table),
			'action' => $this->base_uri . '/insert'
		);

		$this->carbon->load->view('add', $data);
	}

	public function insert()
	{
		if ($this->carbon->db->insert($this->current_table, $_POST) === false)
		{
			$this->add();
		}
		else
		{
			redirect($this->base_uri . '/view/');
		}
	}

	public function view()
	{
		$total_rows = $this->carbon->db->count_all($this->current_table);

		if ($total_rows < 1)
		{
			return $this->carbon->load->view('no_data');
		}

		$per_page = 20;
		$offset = $this->carbon->uri->segment(4, 0);

		$query = $this->carbon->db->get($this->current_table, $per_page, $offset);
		$fields = $this->carbon->db->list_fields($this->current_table);
		$primary = current($fields);

		$pagination = array(
			'base_url' => $this->base_url . '/view',
			'total_rows' => $total_rows,
			'per_page' => $per_page,
			'uri_segment' => 4,
			'full_tag_open' => '<p>',
			'full_tag_close' => '</p>'
		);

		$this->carbon->pagination->initialise($pagination);

		$data = array(
			'title' => (!isset($this->lang['scaff_view'])) ? 'View Data' : $this->lang['scaff_view'],
			'query' => $query,
			'fields' => $fields,
			'primary' => $primary,
			'paginate' => $this->carbon->pagination->create_links(),
		);

		$this->carbon->output->enable_profiler(true);
		$this->carbon->load->view('view', $data);
	}

	public function edit()
	{
		$id = $this->carbon->uri->segment(4);

		if ($id === false)
		{
			return $this->view();
		}

		$primary = $this->carbon->db->primary($this->current_table);
		$query = $this->carbon->db->get_where($this->current_table, array($primary => $id));

		$data = array(
			'title' => (!isset($this->lang['scaff_edit'])) ? 'Edit Data' : $this->lang['scaff_edit'],
			'fields' => $query->field_data(),
			'query' => $query->row(),
			'action' => $this->base_uri . '/update/' . $this->carbon->uri->segment(4)
		);

		$this->carbon->load->view('edit', $data);
	}

	public function update()
	{
		$id = $this->carbon->uri->segment(4);

		$primary = $this->carbon->db->primary($this->current_table);

		$this->carbon->db->update($this->current_table, $_POST, array($primary => $id));

		redirect($this->base_uri . '/view/');
	}

	public function delete()
	{
		if (!isset($this->lang['scaff_del_confirm']))
		{
			$message = 'Are you sure you want to delete the following row: ' . $this->carbon->uri->segment(4);
		}
		else
		{
			$message = $this->lang['scaff_del_confirm'] . ' ' . $this->carbon->uri->segment(4);
		}

		$data = array(
			'title' => (!isset($this->lang['scaff_delete'])) ? 'Delete Data' : $this->lang['scaff_delete'],
			'message' => $message,
			'no' => anchor(array($this->base_uri, 'view'), (!isset($this->lang['scaff_no'])) ? 'No' : $this->lang['scaff_no']),
			'yes' => anchor(array($this->base_uri, 'do_delete', $this->carbon->uri->segment(4)), (!isset($this->lang['scaff_yes'])) ? 'Yes' : $this->lang['scaff_yes'])
		);

		$this->carbon->load->view('delete', $data);
	}

	public function do_delete()
	{
		$id = $this->carbon->uri->segment(4);
		$primary = $this->carbon->db->primary($this->current_table);

		$this->carbon->db->where($primary, $id);
		$this->carbon->db->delete($this->current_table);

		header('Refresh:0; url=' . site_url(array($this->base_uri, 'view')));
		exit();
	}
}

?>
