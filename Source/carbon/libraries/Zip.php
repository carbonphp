<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Zip
{
	protected $zipfile = '';
	protected $zipdata = array();
	protected $directory = array();
	protected $offset = 0;

	public function __construct()
	{
		log_message('debug', 'Zip.php - Carbon_Zip class initialised');
	}

	public function add_dir($directory)
	{
		foreach ((array) $directory as $dir)
		{
			if (preg_match("|.+/&|", $dir) == false)
			{
				$dir .= '/';
			}

			$this->_add_dir($dir);
		}
	}

	protected function _add_dir($directory)
	{
		$directory = str_replace("\\", "/", $directory);

		$this->zipdata[] = "\x50\x4b\x03\x04\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00"
			. pack('V', 0)
			. pack('V', 0)
			. pack('V', 0)
			. pack('v', strlen($directory))
			. pack('v', 0)
			. $directory
			. pack('V', 0)
			. pack('V', 0)
			. pack('V', 0);

		$newoffset = strlen(implode('', $this->zipdata));

		$record = "\x50\x4b\x01\x02\x00\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00"
			. pack('V', 0)
			. pack('V', 0)
			. pack('V', 0)
			. pack('v', strlen($directory))
			. pack('v', 0)
			. pack('v', 0)
			. pack('v', 0)
			. pack('v', 0)
			. pack('V', 16)
			. pack('V', $this->offset)
			. $directory;

		$this->offset = $newoffset;
		$this->directory[] = $record;
	}

	public function add_data($filepath, $data = null)
	{
		if (is_array($filepath))
		{
			foreach ($filepath as $path => $data)
			{
				$this->_add_data($path, $data);
			}
		}
		else
		{
			$this->_add_data($filepath, $data);
		}
	}

	protected function _add_data($filepath, $data)
	{
		$filepath = str_replace("\\", "/", $filepath);

		$oldlen = strlen($data);
		$crc32 = crc32($data);

		$gzdata = gzcompress($data);
		$gzdata = substr($gzdata, 2, -4);
		$newlen = strlen($gzdata);

		$this->zipdata[] = "\x50\x4b\x03\x04\x14\x00\x00\x00\x08\x00\x00\x00\x00\x00"
			. pack('V', $crc32)
			. pack('V', $newlen)
			. pack('V', $oldlen)
			. pack('v', strlen($filepath))
			. pack('v', 0)
			. $filepath
			. $gzdata;

		$newoffset = strlen(implode('', $this->zipdata));

		$record = "\x50\x4b\x01\x02\x00\x00\x14\x00\x00\x00\x08\x00\x00\x00\x00\x00"
			. pack('V', $crc32)
			. pack('V', $newlen)
			. pack('V', $oldlen)
			. pack('v', strlen($filepath))
			. pack('v', 0)
			. pack('v', 0)
			. pack('v', 0)
			. pack('v', 0)
			. pack('V', 32)
			. pack('V', $this->offset);

		$this->offset = $newoffset;
		$this->directory[] = $record . $filepath;
	}

	public function read_file($path, $preserver_filepath = false)
	{
		if (!file_exists($path))
		{
			return false;
		}

		$data = file_get_contents($path);

		if ($data !== false)
		{
			$name = str_replace("\\", "/", $path);

			if ($preserver_filepath === false)
			{
				$name = preg_replace("|.*/(.+)|", "\\1", $name);
			}

			$this->add_data($name, $data);

			return true;
		}

		return false;
	}

	public function read_dir($path)
	{
		$file = opendir($path);

		if ($file != false)
		{
			while (($file = readdir($file)) !== false)
			{
				if (@is_dir($path . $file) && substr($file, 0, 1) != '.')
				{
					$this->read_dir($path . $file . '/');
				}
				elseif (substr($file, 0, 1) != '.')
				{
					$data = file_get_contents($path . $file);

					if ($data !== false)
					{
						$this->add_data(str_replace("\\", "/", $path) . $file, $data);
					}
				}
			}

			return true;
		}
	}

	public function get_zip()
	{
		if ($this->zipfile != '')
		{
			return $this->zipfile;
		}

		if (count($this->zipdata) == 0)
		{
			return false;
		}

		$data = implode('', $this->zipdata);
		$dir = implode('', $this->directory);

		$this->zipfile = $data . $dir . "\x50\x4b\x05\x06\x00\x00\x00\x00"
			. pack('v', sizeof($this->directory))
			. pack('v', sizeof($this->directory))
			. pack('V', strlen($dir))
			. pack('V', strlen($data))
			. "\x00\x00";

		return $this->zipfile;
	}

	public function archive($filepath)
	{
		$file = @fopen($filepath, 'wb');

		if ($file == false)
		{
			return false;
		}

		flock($file, LOCK_EX);
		fwrite($file, $this->get_zip());
		flock($file, LOCK_UN);
		fclose($file);

		return true;
	}

	public function download($filename = 'backup.zip')
	{
		if (!preg_match("|.+?\.zip$|", $filename))
		{
			$filename .= '.zip';
		}

		if (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
		{
			header('Content-Type: application/x-zip');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Transfer-Encoding: binary');
			header('Pragma: public');
			header('Content-Length: ' . strlen($this->get_zip()));
		}
		else
		{
			header('Content-Type: application/x-zip');
			header('Content-Disposition: attachment; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Pragma: no-cache');
			header('Content-Length: ' . strlen($this->get_zip()));
		}

		echo $this->get_zip();
	}

	public function clear_data()
	{
		$this->zipfile = '';
		$this->zipdata = array();
		$this->directory = array();
		$this->offset = array();
	}
}

?>
