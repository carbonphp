<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Language
{
	protected $language = array();
	protected $is_loaded = array();

	public function __construct()
	{
		log_message('debug', 'Language.php - Carbon_Language class initialised');
	}

	public function load($langfile = '', $idiom = '', $return = false)
	{
		$langfile = str_replace(FILE_EXT, '', str_replace('_lang.', '', $langfile)) . '_lang' . FILE_EXT;

		if (in_array($langfile, $this->is_loaded, true))
		{
			return true;
		}

		if ($idiom == '')
		{
			$carbon =& get_instance();
			$def_lang = $carbon->config->get_config_item('language');
			$idiom = ($def_lang == '') ? 'engliah' : $def_lang;
		}

		if (file_exists(APP_PATH . 'languages/' . $idiom . '/' . $langfile))
		{
			include(APP_PATH . 'languages/' . $idiom . '/' . $langfile);
		}
		else
		{
			if (file_exists(CARBON_PATH . 'languages/' . $idiom . '/' . $langfile))
			{
				include(CARBON_PATH . 'languages/' . $idiom . '/' . $langfile);
			}
			else
			{
				display_error('Unable to load the language file: languages/' . $langfile);	
			}
		}

		if (!isset($lang))
		{
			log_message('error', 'Language.php - Langauge file contains no data: languages/' . $idiom . '/' . $langfile);
			return false;
		}

		if ($return == true)
		{
			return $lang;
		}

		$this->is_loaded[] = $langfile;
		$this->language = array_merge($this->language, $lang);
		unset($lang);

		log_message('debug', 'Language.php - Language file loaded: languages/' . $idiom . '/' . $langfile);
		return true;
	}

	public function line($line = '')
	{
		return ($line == '' || !isset($this->language[$line])) ? false : $this->language[$line];
	}
}

?>
