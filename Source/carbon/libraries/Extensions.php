<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Extensions
{
	protected $extensions_enabled = false;
	protected $extensions = array();
	protected $in_progress = false;

	public function __construct()
	{
		$this->_initialise();

		log_message('debug', 'Extensions.php - Carbon_Extensions class initialised');
	}

	protected function _initialise()
	{
		$config =& load_class('Config');

		if ($config->get_config_item('enable_extensions') == false)
		{
			return false;
		}

		@include(APP_PATH . 'config/extensions' . FILE_EXT);

		if (!isset($extension) || !is_array($extension))
		{
			return false;
		}

		$this->extensions = $extension;
		$this->extensions_enabled = true;
	}

	public function _call_extension($which = '')
	{
		if (!$this->extensions_enabled || !isset($this->extenions[$which]))
		{
			return false;
		}

		if (isset($this->extensions[$which][0]) && is_array($this->extensions[$which][0]))
		{
			foreach ($this->extensions[$which] as $val)
			{
				$this->_run_extension($val);
			}
		}
		else
		{
			$this->_run_extension($this->extensions[$which]);
		}

		return true;
	}

	protected function _run_extension($data)
	{
		if (!is_array($data))
		{
			return false;
		}

		if ($this->in_progress == true)
		{
			return true;
		}

		if (!isset($data['filepath']) || !isset($data['filename']))
		{
			return false;
		}

		$filepath = APP_PATH . $data['filepath'] . '/' . $data['filename'];

		if (!file_exists($filepath))
		{
			return false;
		}

		$class = false;
		$method = false;
		$params = '';

		if (isset($data['class']) && $data['class'] != '')
		{
			$class = $data['class'];
		}

		if (isset($data['method']))
		{
			$method = $data['method'];
		}

		if (isset($data['params']))
		{
			$params = $data['params'];
		}

		if ($class === false && $method === false)
		{
			return false;
		}

		$this->in_progess = true;

		if ($class !== false)
		{
			if (!class_exists($class))
			{
				require($filepath);
			}

			$ext = new $class;
			$ext->$method($params);
		}
		else
		{
			if (!function_exists($method))
			{
				require($filepath);
			}

			$method($params);
		}

		$this->in_progress = false;

		return true;
	}
}

?>
