<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Flickr
{
	protected $config = array(
		'api_key' => '',
		'api_secret' => '',
		'endpoint' => 'http://www.flickr.com/services/rest/',
		'auth_endpoint' => 'http://www.flickr.com/services/auth/?',
		'upload_endpoint' => 'http://www.flickr.com/services/upload/',
		'conn_timeout' => 20,
		'io_timeout' => 60
	);

	public function __construct($params = array())
	{
		if (isset($params['token']))
		{
			$this->token = $params['token'];
		}

		foreach ($params as $key => $value)
		{
			$this->config[$key] = $value;
		}

		log_message('debug', 'Flickr.php - Carbon_Flickr class initialised');
	}

	public function set_config_values($params = array())
	{
		foreach ($params as $key => $value)
		{
			$this->config[$key] = $value;
		}
	}

	public function call_method($method, $params = array())
	{
		$this->err_code = 0;
		$this->err_message = '';

		$http_request = curl_init();

		if ($method === 'upload')
		{
			$photo = $params['photo'];
			unset($params['photo']);
		}
		else
		{
			$params['method'] = $method;
		}

		$params['api_key'] = $this->config['api_key'];
		$params['api_sig'] = $this->sign_parameters($params);

		if ($method === 'upload')
		{
			$params['photo'] = '@' . $photo;
			curl_setopt($http_request, CURLOPT_URL, $this->config['upload_endpoint']);
			curl_setopt($http_request, CURLOPT_TIMEOUT, 0);
		}
		else
		{
			curl_setopt($http_request, CURLOPT_URL, $this->config['endpoint']);
			curl_setopt($http_request, CURLOPT_TIMEOUT, $this->config['io_timeout']);
			curl_setopt($http_request, CURLOPT_POST, 1);
		}

		curl_setopt($http_request, CURLOPT_POSTFIELDS, $params);
		curl_setopt($http_request, CURLOPT_CONNECTTIMEOUT, $this->config['conn_timeout']);
		curl_setopt($http_request, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($http_request, CURLOPT_HEADER, 0);
		curl_setopt($http_request, CURLOPT_RETURNTRANSFER, 1);

		$this->http_body = curl_exec($http_request);

		curl_close($http_request);

		$xml = simplexml_load_string($this->http_body);
		$this->xml = $xml;

		if ((string) $xml['stat'] == 'fail')
		{
			$this->err_code = (int) $xml->err['code'];
			$this->err_message = (string) $xml->err['msg'];
			return -1;
		}
		else if ((string) $xml['stat'] != 'ok')
		{
			$this->err_code = 0;
			$this->err_message = 'Unrecognised REST response status';
			return -1;
		}

		return $xml;
	}

	public function get_error_code()
	{
		return $this->err_code;
	}

	public function get_error_message()
	{
		return $this->err_message;
	}

	public function get_auth_url($perms, $frob = '')
	{
		$params = array('api_key' => $this->config['api_key'], 'perms' => $perms);

		if (strlen($frob))
		{
			$params['frob'] = $frob;
		}

		$params['api_sig'] = $this->sign_parameters($params);

		$fields = '';

		foreach ($params as $key => $value)
		{
			if ($fields)
			{
				$fields .= '&';
			}

			$fields .= urlencode($key) . '=' . urlencode($value);
		}

		return $this->config['auth_endpoint'] . $fields;
	}

	public function sign_parameters($params)
	{
		ksort($params);
		$args = '';

		foreach ($params as $key => $value)
		{
			$args .= $key . $value;
		}

		return md5($this->config['api_secret'] . $args);
	}
}

?>
