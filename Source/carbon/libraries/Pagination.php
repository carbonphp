<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Pagination
{
	protected $base_url = '';
	protected $total_rows = '';
	protected $per_page = 10;
	protected $num_links = 2;
	protected $cur_page = 0;
	protected $first_link = '&lsaquo; First';
	protected $next_link = '&gt;';
	protected $prev_link = '&lt;';
	protected $last_link = 'Last &rsaquo;';
	protected $uri_segment = 3;
	protected $full_tag_open = '';
	protected $full_tag_close = '';
	protected $first_tag_open = '';
	protected $first_tag_close = '&nbsp;';
	protected $last_tag_open = '&nbsp;';
	protected $last_tag_close = '';
	protected $cur_tag_open = '&nbsp;<b>';
	protected $cur_tag_close = '</b>';
	protected $next_tag_open = '&nbsp;';
	protected $next_tag_close = '&nbsp;';
	protected $prev_tag_open = '&nbsp;';
	protected $prev_tag_close = '';
	protected $num_tag_open = '&nbsp;';
	protected $num_tag_close = '';

	public function __construct($params = array())
	{
		if (count($params) > 0)
		{
			$this->initialise($params);
		}

		log_message('debug', 'Pagination.php - Carbon_Pagination class initialised');
	}

	public function initialise($params = array())
	{
		if (count($params) > 0)
		{
			foreach ($params as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}
		}
	}

	public function create_links()
	{
		if ($this->total_rows == 0 || $this->per_page == 0)
		{
			return '';
		}
	
		$num_pages = ceil($this->total_rows / $this->per_page);

		if ($num_pages == 1)
		{
			return '';
		}

		$carbon =& get_instance();

		if ($carbon->uri->segment($this->uri_segment) != 0)
		{
			$this->cur_page = $carbon->uri->segment($this->uri_segment);
			$this->cur_page = (int) $this->cur_page;
		}

		$this->num_links = (int) $this->num_links;

		if ($this->num_links < 1)
		{
			display_error('The number of links must supplied as a positive number');
		}

		if (!is_numeric($this->cur_page))
		{
			$this->cur_page = 0;
		}

		if ($this->cur_page > $this->total_rows)
		{
			$this->cur_pge = ($num_pages - 1) * $this->per_page;
		}

		$uri_page_number = $this->cur_page;
		$this->cur_page = floor(($this->cur_page / $this->per_page) + 1);

		$start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
		$end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

		$this->base_url = rtrim($this->base_url, '/') . '/';

		$output = '';

		if ($this->cur_page > $this->num_links)
		{
			$output .= $this->first_tag_open . '<a href="' . $this->base_url . '">' . $this->first_link . '</a>' . $this->first_tag_close;
		}

		if (($this->cur_page != 1))
		{
			$i = $uri_page_number - $this->per_page;

			if ($i == 0)
			{
				$i = '';
			}

			$output .= $this->prev_tag_open . '<a href="' . $this->base_url . $i . '">' . $this->prev_link . '</a>' . $this->prev_tag_close;
		}

		for ($loop = $start - 1; $loop <= $end; $loop++)
		{
			$i = ($loop * $this->per_page) - $this->per_page;

			if ($i >= 0)
			{
				if ($this->cur_page == $loop)
				{
					$output .= $this->cur_tag_open . $loop . $this->cur_tag_close;
				}
				else
				{
					$n = ($i == 0) ? '' : $i;
					$output .= $this->num_tag_open . '<a href="' . $this->base_url . $n . '">' . $loop . '</a>' . $this->num_tag_close;
				}
			}
		}

		if ($this->cur_page < $num_pages)
		{
			$output .= $this->next_tag_open . '<a href="' . $this->base_url . ($this->cur_page * $this->per_page) . '">' . $this->next_link . '</a>' . $this->next_tag_close;
		}

		if (($this->cur_page + $this->num_links) < $num_pages)
		{
			$i = (($num_pages * $this->per_page) - $this->per_page);
			$output .= $this->last_tag_open . '<a href="' . $this->base_url . $i . '">' . $this->last_link . '</a>' . $this->last_tag_close;
		}

		$output = preg_replace("#([^:])//+#", "\\1/", $output);
		$output = $this->full_tag_open . $output . $this->full_tag_close;

		return $output;
	}
}

?>
