<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Router
{
	protected $config;
	protected $routes = array();
	protected $class_name = '';
	protected $method_name = 'index';
	protected $directory_name = '';
	protected $uri_protocol = 'auto';
	protected $default_controller;
	protected $scaffolding_request = false;

	public function __construct()
	{
		$this->config =& load_class('Config');
		$this->uri =& load_class('Uri');
		$this->_set_route_mapping();

		log_message('debug', 'Router.php - Carbon_Router class initialised');
	}

	protected function _set_route_mapping()
	{
		if ($this->config->get_config_item('use_query_strings') === true && isset($_GET[$this->config->get_config_item('controller_trigger')]))
		{
			$this->set_class_name(trim($this->uri->_filter_uri_chars($_GET[$this->config->get_config_item('controller_trigger')])));

			if (isset($_GET[$this->config->get_config_item('method_trigger')]))
			{
				$this->set_method_name(trim($this->_filter_uri_chars($_GET[$this->config->get_config_item('method_trigger')])));
			}

			return true;
		}

		@include(APP_PATH . 'config/routing' . FILE_EXT);

		$this->routes = (!isset($routing) || !is_array($routing)) ? array() : $routing;
		unset($routing);

		$this->default_controller = (!isset($this->routes['default_controller']) || $this->routes['default_controller'] == '') ? false : strtolower($this->routes['default_controller']);

		$this->uri->_get_uri_string();

		if ($this->uri->get_uri_string() == '')
		{
			if ($this->default_controller === false)
			{
				display_error('Could not determine what should be displayed, no default routing has been specified in the routing configuration file');
			}

			$this->set_class_name($this->default_controller);
			$this->set_method_name('index');
			$this->_compile_uri_segments(array($this->default_controller, 'index'));

			$this->uri->_reindex_segments();

			log_message('debug', 'Router.php - No URI is present, setting the controller to default');
			return true;
		}

		unset($this->routes['default_controller']);

		$this->uri->_remove_url_suffix();
		$this->uri->_explode_segments();
		$this->_parse_routes();
		$this->uri->_reindex_segments();
	}

	protected function _compile_uri_segments($segments = array())
	{
		$segments = $this->_validate_uri_segments($segments);

		if (count($segments) == 0)
		{
			return;
		}

		$this->set_class_name($segments[0]);

		if (isset($segments[1]))
		{
			if ($this->routes['scaffolding_trigger'] == $segments[1] && $segments[1] != '_scaffolding')
			{
				$this->scaffolding_request = true;
				unset($this->routes['scaffolding_trigger']);
			}
			else
			{
				$this->set_method_name($segments[1]);
			}
		}
		else
		{
			$segments[1] = 'index';
		}

		$this->uri->uri_rsegments = $segments;
	}

	protected function _validate_uri_segments($segments)
	{
		if (file_exists(APP_PATH . 'controllers/' . $segments[0] . FILE_EXT))
		{
			return $segments;
		}

		if (is_dir(APP_PATH . 'controllers/' . $segments[0]))
		{
			$this->set_directory_name($segments[0]);
			$segments = array_slice($segments, 1);

			if (count($segments) > 0)
			{
				if (!file_exists(APP_PATH . 'controllers/' . $this->get_directory_name() . $segments[0] . FILE_EXT))
				{
					display_not_found();
				}
			}
			else
			{
				$this->set_class_name($this->default_controller);
				$this->set_method_name('index');

				if (!file_exists(APP_PATH . 'controllers/' . $this->get_directory_name() . $this->default_controller . FILE_EXT))
				{
					$this->directory_name = '';
					return array();
				}
			}

			return $segments;
		}

		display_not_found();
	}

	protected function _parse_routes()
	{
		if (count($this->routes) == 1)
		{
			$this->_compile_uri_segments($this->uri->get_segment_array());
			return;
		}

		$uri = implode('/', $this->uri->get_segment_array());
		$num = count($this->uri->get_segment_array());

		if (isset($this->routes[$uri]))
		{
			$this->_compile_uri_segments(explode('/', $this->routes[$uri]));
			return;
		}

		foreach ($this->routes as $key => $val)
		{
			$key = str_replace(':any', '.+', str_replace(':num', '[0-9]+', $key));

			if (preg_match('#^' . $key . '$#', $uri))
			{
				if (strpos($val, '$') !== false && strpos($key, '(') !== false)
				{
					$val = preg_replace('#^' . $key . '$#', $val, $uri);
				}

				$this->_compile_uri_segments(explode('/', $val));
				return;
			}
		}

		$this->_compile_uri_segments($this->uri->get_segment_array());
	}

	public function set_class_name($class_name)
	{
		$this->class_name = $class_name;
	}

	public function get_class_name()
	{
		return $this->class_name;
	}

	public function set_method_name($method_name)
	{
		$this->method_name = $method_name;
	}

	public function get_method_name()
	{
		if ($this->method_name == $this->get_class_name())
		{
			return 'index';
		}

		return $this->method_name;
	}

	public function set_directory_name($directory_name)
	{
		$this->directory_name = $directory_name . '/';
	}

	public function get_directory_name()
	{
		return $this->directory_name;
	}

	public function is_scaffolding_request()
	{
		return $this->scaffolding_request;
	}
}

?>
