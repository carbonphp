<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Exception
{
	protected $exc_levels = array(
		E_ERROR => 'Error',
		E_WARNING => 'Warning',
		E_PARSE => 'Parsing error',
		E_NOTICE => 'Notice',
		E_CORE_ERROR => 'Core error',
		E_CORE_WARNING => 'Core warning',
		E_COMPILE_ERROR => 'Compile error',
		E_COMPILE_WARNING => 'Compile warning',
		E_USER_ERROR => 'User error',
		E_USER_WARNING => 'User warning',
		E_USER_NOTICE => 'User notice',
		E_STRICT => 'Runtime notice'
	);

	protected $exc_action;
	protected $exc_severity;
	protected $exc_message;
	protected $exc_ob_level;

	public function __construct()
	{
		$this->exc_ob_level = ob_get_level();
	}

	public function log_exception($severity, $message, $file_path, $line_number)
	{
		$severity = (!isset($this->exc_levels[$severity])) ? $severity : $this->exc_levels[$severity];
		log_message('error', 'Exception.php - Severity: ' . $severity. ' -> ' . $message . ' ' . $file_path . ' ' . $line_number, true);
	}

	public function display_not_found($page = '')
	{
		$page_heading = '404: Page Not Found';
		$page_message = 'The page you have requested could not be found.';
		log_message('error', 'Exception.php - 404 page not found: ' . $page);
		echo $this->display_error($page_heading, $page_message, 'error_404');
		exit();
	}

	public function display_error($heading, $message, $template = 'error')
	{
		$message = '<p>' . implode('</p><p>' , (!is_array($message)) ? array($message) : $message) . '</p>';

		if (ob_get_level() > $this->exc_ob_level + 1)
		{
			ob_end_flush();
		}

		ob_start();
		include(APP_PATH . 'errors/' . $template . FILE_EXT);
		$buffer = ob_get_contents();
		ob_end_clean();

		return $buffer;
	}

	public function display_php_error($severity, $message, $file_path, $line_number)
	{
		$severity = (!isset($this->exc_levels[$severity])) ? $severity : $this->exc_levels[$severity];
		$file_path = str_replace('\\', '/', $file_path);

		if (!strpos($file_path, '/'))
		{
			$x = explode('/', $file_path);
			$file_path = $x[count($x) - 2] . '/' . end($x);
		}
	
		if (ob_get_level() > $this->exc_ob_level + 1)
		{
			ob_end_flush();
		}

		ob_start();

		include(APP_PATH . 'errors/error_php' . FILE_EXT);

		$buffer = ob_get_contents();
		ob_end_clean();
		echo $buffer;	
	}
}

?>
