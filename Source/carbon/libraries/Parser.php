<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Parser
{
	protected $l_delim = '{';
	protected $r_delim = '}';
	protected $object;

	public function __construct()
	{
		log_message('debug', 'Parser.php - Carbon_Parser class initialised');
	}

	public function parse($template, $data, $return = false)
	{
		$carbon =& get_instance();
		$template = $carbon->load->view($template, $data, true);

		if ($template == '')
		{
			return false;
		}

		foreach ($data as $key => $value)
		{
			if (is_array($value))
			{
				$template = $this->_parse_pair($key, $value, $template);
			}
			else
			{
				$template = $this->_parse_single($key, (string) $value, $template);
			}
		}

		if ($return == false)
		{
			$carbon->output->set_final_output($template);
		}

		return $template;
	}

	public function set_delimiters($l = '{', $r = '}')
	{
		$this->l_delim = $l;
		$this->r_delim = $r;
	}

	protected function _parse_single($key, $value, $string)
	{
		return str_replace($this->l_delim . $key . $this->r_delim, $value, $string);
	}

	protected function _parse_pair($variable, $data, $string)
	{
		$match = $this->_match_pair($string, $variable);

		if ($match === false)
		{
			return $string;
		}

		$str = '';

		foreach ($data as $row)
		{
			$temp = $match['1'];

			foreach ($row as $key => $value)
			{
				if (is_array($value))
				{
					$temp = $this->_parse_pair($key, $value, $temp);
				}
				else
				{
					$temp = $this->_parse_single($key, $value, $temp);
				}
			}

			$str .= $temp;
		}

		return str_replace($match['0'], $str, $string);
	}

	protected function _match_pair($string, $variable)
	{
		if (!preg_match("|".$this->l_delim . $variable . $this->r_delim."(.+)".$this->l_delim . '/' . $variable . $this->r_delim."|s", $string, $match))
		{
			return false;
		}

		return $match;
	}
}

?>
