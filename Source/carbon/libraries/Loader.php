<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Loader
{
	protected $loader_ob_level;
	protected $loader_cached_vars = array();
	protected $loader_classes = array();
	protected $loader_models = array();
	protected $loader_utilities = array();
	protected $loader_varmap = array();
	protected $loader_view_path = '';

	public function __construct()
	{
		$this->loader_view_path = APP_PATH . 'views/';
		$this->loader_ob_level = ob_get_level();

		log_message('debug', 'Loader.php - Carbon_Loader class initialised');
	}

	public function library($library = '', $params = null)
	{
		if ($library == '')
		{
			return false;
		}

		if (is_array($library))
		{
			foreach ($library as $class_name)
			{
				$this->_load_class($class_name, $params);
			}
		}
		else
		{
			$this->_load_class($library, $params);
		}

		$this->_assign_to_models();
	}

	public function model($model, $name = '', $db_conn = false)
	{
		if (is_array($model))
		{
			foreach ($model as $model_name)
			{
				$this->model($model_name);
			}

			return true;
		}

		if ($model == '')
		{
			return false;
		}

		if (strpos($model, '/') === false)
		{
			$path = '';
		}
		else
		{
			$x = explode('/', $model);
			$model = end($x);
			unset($x[count($x) - 1]);
			$path = implode('/', $x) . '/';
		}

		if ($name == '')
		{
			$name = $model;
		}

		if (in_array($name, $this->loader_models, true))
		{
			return true;
		}

		$carbon =& get_instance();

		if (isset($carbon->$name))
		{
			display_error('The model name you are trying to load is sharing a name with a resource already in use: ' . $name);
		}

		$model = strtolower($model);

		if (!file_exists(APP_PATH . 'models/' . $path . $model . FILE_EXT))
		{
			display_error('Unable to load the model file you have specified: ' . $model);
		}

		if ($db_conn !== false && !class_exists('Carbon_Database'))
		{
			if ($db_conn == true)
			{
				$db_conn = '';
			}

			$carbon->load->database($db_conn, false, true);
		}

		if (!class_exists('Model'))
		{
			load_class('Model', false);
		}

		require_once(APP_PATH . 'models/' . $path . $model . FILE_EXT);

		$model = ucfirst($model);
		$carbon->$name = new $model();
		$carbon->$name->assign_libraries();
		$this->loader_models[] = $name;
	}

	public function database($params = '', $return = false, $active_record = false)
	{
		$carbon =& get_instance();

		if (class_exists('Carbon_Database') && $return == false && $active_record == false)
		{
			return false;
		}

		require_once(CARBON_PATH . 'database/Database' . FILE_EXT);

		if ($return === true)
		{
			return Database($params, $active_record);
		}

		
		$carbon->db = '';
		$carbon->db = Database($params, $active_record);
		$this->_assign_to_models();
	}

	public function dbutils()
	{
		if (!class_exists('Carbon_Database'))
		{
			$this->database();
		}

		$carbon =& get_instance();

		require_once(CARBON_PATH . 'database/Database_utility' . FILE_EXT);
		require_once(CARBON_PATH . 'database/drivers/' . $carbon->db->dbdriver . '/' . $carbon->db->dbdriver . '_utility' . FILE_EXT);

		$class = 'Carbon_Database_' . $carbon->db->dbdriver . '_utility';

		$carbon->dbutils = new $class();

		$carbon->load->_assign_to_models();
	}

	public function view($view, $vars = array(), $return = false)
	{
		return $this->_load(array('view' => $view, 'vars' => $this->_object_to_array($vars), 'return' => $return));
	}

	public function file($path, $return = false)
	{
		return $this->_load(array('path' => $path, 'return' => $return));
	}

	public function vars($vars = array())
	{
		$vars = $this->_object_to_array($vars);

		if (is_array($vars) && count($vars) > 0)
		{
			foreach ($vars as $key => $val)
			{
				$this->loader_cached_vars[$key] = $val;
			}
		}
	}

	public function utility($utilities = array())
	{
		if (!is_array($utilities))
		{
			$utilities = array($utilities);
		}

		foreach ($utilities as $utility)
		{
			$utility = strtolower(str_replace(FILE_EXT, '', str_replace('_utility', '', $utility)) . '_utility');

			if (isset($this->loader_utilities[$utility]))
			{
				continue;
			}

			if (file_exists(APP_PATH . 'utilities/' . $utility . FILE_EXT))
			{
				include_once(APP_PATH . 'utilities/' . $utility . FILE_EXT);
			}
			else
			{
				if (file_exists(CARBON_PATH . 'utilities/' . $utility . FILE_EXT))
				{
					include(CARBON_PATH . 'utilities/' . $utility . FILE_EXT);
				}
				else
				{
					display_error('Unable to load the utility file: utilities/' . $utility . FILE_EXT);
				}
			}

			$this->loader_utilities[$utility] = true;
		}

		log_message('debug', 'Loader.php - Utilities loaded successfully: ' . implode(', ', $utilities));
	}

	public function language($file = array(), $lang = '')
	{
		$carbon =& get_instance();

		if (!is_array($file))
		{
			$file = array($file);
		}

		foreach ($file as $langfile)
		{
			$carbon->lang->load($langfile, $lang);
		}
	}

	public function scaffold_language($file = '', $lang = '', $return = false)
	{
		$carbon =& get_instance();

		return $carbon->lang->load($file, $lang, $return);
	}

	public function config($file = '', $use_sections = false, $fail_gracefully = false)
	{
		$carbon =& get_instance();
		$carbon->config->load($file, $use_sections, $fail_gracefully);
	}

	public function scaffolding($table = '')
	{
		if ($table === false)
		{
			display_error('You must include the name of the table you would like to access when you initialise scaffolding');
		}

		$carbon =& get_instance();
		$carbon->set_scaffolding(true);
		$carbon->set_scaff_table($table);
	}

	protected function _load($data)
	{
		
		$view = $vars = $path = $return = '';
		
		foreach (array('view', 'vars', 'path', 'return') as $val)
		{
			$$val = (!isset($data[$val])) ? false : $data[$val];
		}

		if ($path == '')
		{
			$ext = pathinfo($view, PATHINFO_EXTENSION);
			$file = ($ext =='') ? $view . FILE_EXT : $view;
			$path = $this->loader_view_path . $file;
		}
		else
		{
			$x = explode('/', $path);
			$file = end($x);
		}

		if (!file_exists($path))
		{
			display_error('Unable to load the specified file: ' . $file);
		}

		$carbon =& get_instance();

		foreach (get_object_vars($carbon) as $key => $var)
		{
			$this->$key = $carbon->$key;
		}

		if (is_array($vars))
		{
			$this->loader_cached_vars = array_merge($this->loader_cached_vars, $vars);
		}

		extract($this->loader_cached_vars);

		ob_start();

		if ((bool) @ini_get('short_open_tag') === false && get_config_item('short_tag_rewrite') == true)
		{
			echo eval('?>' . preg_replace("/;*\s*\?>/", "; ?>", str_replace('<?=', '<?php echo ', file_get_contents($path))) . '<?php ');
		}
		else
		{
			include($path);
		}

		log_message('debug', 'Loader.php - Loaded file: ' . $path);

		if ($return === true)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}

		if (ob_get_level() > $this->loader_ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			global $output;
			$output->append_output(ob_get_contents());
			@ob_end_clean();
		}
	}

	protected function _load_class($class_name, $params = null)
	{
		$class_name = str_replace(FILE_EXT, '', $class_name);

		foreach (array(ucfirst($class_name), strtolower($class_name)) as $class)
		{
			$subclass = APP_PATH . 'libraries/' . get_config_item('subclass_prefix') . $class . FILE_EXT;

			if (file_exists($subclass))
			{
				$base_class = CARBON_PATH . 'libraries/' . ucfirst($class) . FILE_EXT;

				if (!file_exists($base_class))
				{
					log_message('error', 'Loader.php - Unable to load the requested class file: ' . $class);
					display_error('Unable to load the requested class file: ' . $class);
				}

				if (in_array($subclass, $this->loader_classes))
				{
					$is_duplicated = true;
					log_message('debug', 'Loader.php - Class already loaded, ignoring second load attempt: ' . $class);
					return;
				}

				include($base_class);
				include($subclass);
				$this->loader_classes[] = $subclass;

				return $this->_init_class($class, get_config_item('subclass_prefix'), $params);
			}

			$is_duplicated = false;

			for ($i = 1; $i < 3; $i++)
			{
				$path = ($i % 2) ? APP_PATH : CARBON_PATH;
				$file_path = $path . 'libraries/' . $class . FILE_EXT;

				if (!file_exists($file_path))
				{
					continue;
				}

				if (in_array($file_path, $this->loader_classes))
				{
					$is_duplicated = true;
					log_message('debug', 'Loader.php - Class already loaded, ignoring second load attempt: ' . $class);
					return;
				}

				include($file_path);

				$this->loader_classes[] = $file_path;
				return $this->_init_class($class, '', $params);
			}
		}

		if ($is_duplicated == false)
		{
			log_message('error', 'Loader.php - Unable to load the requested class file: ' . $class);
			display_error('Unable to load the requested class file: ' . $class);
		}
	}

	protected function _init_class($class, $prefix = '', $config = false)
	{
		if ($config === null)
		{
			$config = null;

			if (file_exists(APP_PATH . 'config/' . $class . FILE_EXT))
			{
				include(APP_PATH . 'config/' . $class . FILE_EXT);
			}
		}

		if ($prefix == '')
		{
			$name = (class_exists('Carbon_' . $class)) ? 'Carbon_' . $class : $class;
		}
		else
		{
			$name = $prefix . $class;
		}

		$class = strtolower($class);
		$classvar = (!isset($this->loader_varmap[$class])) ? $class : $this->loader_varmap[$class];

		$carbon =& get_instance();

		if ($config !== null)
		{
			$carbon->$classvar = new $name($config);
		}
		else
		{
			$carbon->$classvar = new $name;
		}
	}

	public function autoloader()
	{
		include(APP_PATH . 'config/autoload' . FILE_EXT);

		if (!isset($autoload))
		{
			return false;
		}

		if (count($autoload['config']) > 0)
		{
			$carbon =& get_instance();

			foreach ($autoload['config'] as $key => $value)
			{
				$carbon->config->load($value);
			}
		}

		foreach (array('utility', 'language') as $type)
		{
			if (isset($autoload[$type]) && count($autoload[$type]) > 0)
			{
				$this->$type($autoload[$type]);
			}
		}

		if (isset($autoload['libraries']) && count($autoload['libraries']) > 0)
		{
			if (in_array('database', $autoload['libraries']))
			{
				$this->database();
				$autoload['libraries'] = array_diff($autoload['libraries'], array('database'));
			}

			if (in_array('scaffolding', $autoload['libraries']))
			{
				$this->scaffolding();
				$autoload['libraries'] = array_diff($autoload['libraries'], array('scaffolding'));
			}

			foreach ($autoload['libraries'] as $library)
			{
				$this->library($library);
			}
		}

		if (isset($autoload['model']))
		{
			$this->model($autoload['model']);
		}
	}

	public function set_view_path($view_path)
	{
		$this->loader_view_path = $view_path;
	}

	protected function _assign_to_models()
	{
		if (count($this->loader_models) == 0)
		{
			return false;
		}

		$carbon =& get_instance();

		foreach ($this->loader_models as $model)
		{
			$carbon->$model->assign_libraries();
		}
	}

	protected function _object_to_array($object)
	{
		return (is_object($object)) ? get_object_vars($object) : $object;
	}
}

?>
