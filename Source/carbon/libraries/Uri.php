<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Uri
{
	protected $keyval = array();
	protected $uri_string;
	public $uri_segments = array();
	public $uri_rsegments = array();

	public function __construct()
	{
		$this->config =& load_class('Config');
		log_message('debug', 'Uri.php - Carbon_Uri class initialised');
	}

	public function _get_uri_string()
	{
		if (strtoupper($this->config->get_config_item('uri_protocol')) == 'AUTO')
		{
			if (is_array($_GET) && count($_GET) == 1)
			{
				$this->uri_string = key($_GET);
				return;
			}

			$path = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : @getenv('PATH_INFO');

			if ($path != '' && $path != '/' . INDEX_FILE)
			{
				$this->uri_string = $path;
				return;
			}

			$path = (isset($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : @getenv('QUERY_STRING');

			if ($path != '' && $path != '/' . INDEX_FILE)
			{
				$this->uri_string = $path;
				return;
			}

			$path = (isset($_SERVER['ORIG_PATH_INFO'])) ? $_SERVER['ORIG_PATH_INFO'] : @getenv('ORIG_PATH_INFO');

			if ($path != '' && $path != '/' . INDEX_FILE)
			{
				$this->uri_string = $path;
				return;
			}

			$this->uri_string = '';
		}
		else
		{
			$uri = strtoupper($this->config->get_config_item('uri_protocol'));

			if ($uri == 'REQUEST_URI')
			{
				$this->uri_string = $this->_parse_request_uri();
				return;
			}

			$this->uri_string = (isset($_SERVER[$uri])) ? $_SERVER[$uri] : @getenv($uri);
		}

		if ($this->uri_string == '/')
		{
			$this->uri_string = '';
		}
	}

	protected function _parse_request_uri()
	{
		if (!isset($_SERVER['REQUEST_URI']) || $_SERVER['REQUEST_URI'] == '')
		{
			return '';
		}

		$request_uri = preg_replace("|/(.*)|", "\\1", str_replace("\\", "/", $_SERVER['REQUEST_URI']));

		if ($request_uri == '' || $request_uri == INDEX_FILE)
		{
			return '';
		}

		$index_path = INDEX_PATH;

		if (strpos($request_uri, '?') !== false)
		{
			$index_path .= '?';
		}

		$parsed_uri = explode('/', $request_uri);
		$i = 0;

		foreach (explode('/', $index_path) as $segment)
		{
			if (isset($parsed_uri[$i]) && $segment == $parsed_uri[$i])
			{
				$i++;
			}
		}

		$parsed_uri = implode('/', array_slice($parsed_uri, $i));

		if ($parsed_uri != '')
		{
			$parsed_uri = '/' . $parsed_uri;
		}

		$parsed_uri;
	}

	public function _filter_uri_chars($uri_string)
	{
		if ($this->config->get_config_item('allowed_uri_chars') != '' && $uri_string != '')
		{
			if (!preg_match("|^[" . preg_quote($this->config->get_config_item('allowed_uri_chars')) . "]+$|i", $uri_string))
			{
				exit('The submitted URI contains characters that are disallowed.');
			}
		}

		return $uri_string;
	}

	public function _remove_url_suffix()
	{
		if ($this->config->get_config_item('url_postfix') != '')
		{
			$this->uri_string = preg_replace('|' . preg_quote($this->config->get_config_item('url_postfix')) . '$|', '', $this->uri_string);
		}
	}

	public function _explode_segments()
	{
		foreach (explode('/', preg_replace("|/*(.+?)/*$|", "\\1", $this->uri_string)) as $uri_segment)
		{
			$uri_segment = trim($this->_filter_uri_chars($uri_segment));

			if ($uri_segment != '')
			{
				$this->uri_segments[] = $uri_segment;
			}
		}
	}

	public function _reindex_segments()
	{
		$i = 1;

		foreach ($this->uri_segments as $value)
		{
			$this->uri_segments[$i++] = $value;
		}

		unset($this->uri_segments[0]);

		$i = 1;

		foreach ($this->uri_rsegments as $value)
		{
			$this->uri_rsegments[$i++] = $value;
		}

		unset($this->uri_rsegments[0]);
	}

	public function segment($index, $no_result = false)
	{
		return (!isset($this->uri_segments[$index]) ? $no_result : $this->uri_segments[$index]);
	}

	public function rsegment($index, $no_result = false)
	{
		return (!isset($this->uri_rsegments[$index]) ? $no_result : $this->uri_rsegments[$index]);
	}

	public function get_uri_to_assoc($index = 3, $default = array())
	{
		return $this->_uri_to_assoc($index, $default, 'segment');
	}

	public function get_ruri_to_assoc($index = 3, $default = array())
	{
		return $this->_uri_to_assoc($index, $default, 'rsegment');
	}

	protected function _uri_to_assoc($index = 3, $default, $which = 'segment')
	{
		if ($which == 'segment')
		{
			$total_segments = 'total_segments';
			$segment_array = 'segment_array';
		}
		else
		{
			$total_segments = 'total_rsegments';
			$segment_array = 'rsegment_array';
		}

		if (!is_numeric($index))
		{
			return $default;
		}

		if (isset($this->keyval[$index]))
		{
			return $this->keyval[$index];
		}

		if ($this->$total_segments() < $index)
		{
			if (count($default) == 0)
			{
				return array();
			}

			$retval = array();

			foreach ($default as $value)
			{
				$retval[$value] = false;
			}

			return $retval;
		}

		$segments = array_slice($this->segment_array(), ($index - 1));
		$i = 0;
		$lastval = '';
		$retval = array();

		foreach ($segments as $segment)
		{
			if ($i % 2)
			{
				$retval[$lastval] = $segment;
			}
			else
			{
				$retval[$$segment] = false;
				$lastval = $segment;
			}

			$i++;
		}

		if (count($default) > 0)
		{
			foreach ($default as $value)
			{
				if (!array_key_exists($value, $retval))
				{
					$retval[$value] = false;
				}
			}
		}

		$this->keyval[$index] = $retval;
		return $retval;
	}

	public function assoc_to_uri($array)
	{
		$temp = array();

		foreach ((array) $array as $key => $value)
		{
			$temp[] = $key;
			$temp[] = $value;
		}

		return implode('/', $temp);
	}

	public function slash_segment($index, $where = 'trailing')
	{
		return $this->_slash_segment($index, $where, 'segment');
	}

	public function slash_rsegment($index, $where = 'trailing')
	{
		return $this->_slash_segment($index, $where, 'rsegment');
	}

	protected function _slash_segment($index, $where = 'trailing', $which = 'segment')
	{
		if ($where == 'trailing')
		{
			$trailing = '/';
			$leading = '';
		}
		else if ($where == 'leading')
		{
			$trailing = '';
			$leading = '/';
		}
		else
		{
			$trailing = '/';
			$leading = '/';
		}

		return $leading . $this->$which($index) . $trailing;
	}

	public function get_segment_array()
	{
		return $this->uri_segments;
	}

	public function get_rsegment_array()
	{
		return $this->uri_rsegments;
	}

	public function total_segments()
	{
		return count($this->uri_segments);
	}

	public function total_rsegments()
	{
		return count($this->uri_rsegments);
	}

	public function get_uri_string()
	{
		return $this->uri_string;
	}

	public function get_ruri_string()
	{
		return '/' . implode('/', $this->uri_rsegments) . '/';
	}
}

?>
