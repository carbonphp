<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Benchmark
{
	public $marker = array();

	public function __construct()
	{
		log_message('debug', 'Benchmark.php - Carbon_Benchmark class initialised');
	}

	public function mark($mark)
	{
		$this->marker[$mark] = microtime();
	}

	public function elapsed_time($point1 = '', $point2 = '', $precision = 4)
	{
		if ($point1 == '')
		{
			return '{elapsed_time}';
		}

		if (!isset($this->marker[$point1]))
		{
			return '';
		}

		if (!isset($this->marker[$point2]))
		{
			$this->marker[$point2] = microtime();
		}

		list($sm, $ss) = explode(' ', $this->marker[$point1]);
		list($em, $es) = explode(' ', $this->marker[$point2]);

		return number_format(($em + $es) - ($sm + $ss), $precision);
	}

	public function memory_usage()
	{
		return '{memory_usage}';
	}
}

?>
