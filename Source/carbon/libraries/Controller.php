<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Controller extends Carbon_Base
{
	protected $scaffolding = false;
	protected $scaff_table = false;

	public function __construct()
	{
		parent::__construct();
		$this->_initialise();

		log_message('debug', 'Controller.php - Controller class initialised');
	}

	protected function _initialise()
	{
		$classes = array(
			'config' => 'Config',
			'input' => 'Input',
			'benchmark' => 'Benchmark',	
			'uri' => 'Uri',
			'lang' => 'Language',
			'output' => 'Output'
		);

		foreach ($classes as $class => $class_name)
		{
			$this->$class =& load_class($class_name);
		}

		$this->load =& load_class('Loader');
		$this->load->autoloader();
	}

	public function _scaffolding()
	{
		if ($this->scaffolding === false || $this->scaff_table === false)
		{
			display_not_found('Scaffolding is not available');
		}

		$method = (!in_array($this->uri->segment(3), array('add', 'insert', 'edit', 'update', 'view', 'delete', 'do_delete'), true)) ? 'view' : $this->uri->segment(3);

		require_once(CARBON_PATH . 'scaffolding/Scaffolding' . FILE_EXT);

		$scaffold = new Scaffolding($this->scaff_table);
		$scaffold->$method();
	}

	public function set_scaffolding($scaffolding)
	{
		$this->scaffolding = $scaffolding;
	}

	public function set_scaff_table($scaff_table)
	{
		$this->scaff_table = $scaff_table;
	}
}

?>
