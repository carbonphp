<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Config
{
	protected $config_files = array();
	protected $is_loaded = array();

	public function __construct()
	{
		$this->config_files =& load_config();
		log_message('debug', 'Config.php - Carbon_Config class initialised');
	}

	public function load($file_name = '', $use_sections = false, $fail_gracefully = false)
	{
		$file_name = ($file_name == '') ? 'config' : str_replace(FILE_EXT, '', $file_name);

		if (in_array($file_name, $this->is_loaded, true))
		{
			return true;
		}

		if (!file_exists(APP_PATH . 'config/' . $file_name . FILE_EXT))
		{
			if ($fail_gracefully === true)
			{
				return false;
			}

			display_error('The configuration file does not exist: ' . $file_name . FILE_EXT);
		}

		include(APP_PATH . 'config/' . $file_name . FILE_EXT);

		if (!isset($config) || !is_array($config))
		{
			if ($fail_gracefully === true)
			{
				return false;
			}

			display_error('The configuration file is incorrectly formatted: ' . $file_name . FILE_EXT);
		}

		if ($use_sections === true)
		{
			if (isset($this->config_files[$file_name]))
			{
				$this->config_files[$file_name] = array_merge($this->config_files[$file_name], $config);
			}
			else
			{
				$this->config_files[$file_name] = $config;
			}
		}
		else
		{
			$this->config_files = array_merge($this->config_files, $config);
		}

		$this->is_loaded[] = $file_name;
		unset($config);

		log_message('debug', 'Config.php - Configuration file loaded: config/' . $file_name . FILE_EXT);

		return true;
	}

	public function get_config_item($config_item, $index = '')
	{
		if ($index == '')
		{
			if (!isset($this->config_files[$config_item]))
			{
				return false;
			}

			$pref = $this->config_files[$config_item];
		}
		else
		{
			if (!isset($this->config_files[$index]))
			{
				return false;
			}

			if (!isset($this->config_files[$index][$config_item]))
			{
				return false;
			}

			$pref = $this->config_files[$index][$config_item];
		}

		return $pref;
	}

	public function slash_config_item($config_item)
	{
		if (!isset($this->config_files[$config_item]))
		{
			return false;
		}

		$pref = $this->config_files[$config_item];

		if ($pref != '')
		{
			if (ereg("/$", $pref) === false)
			{
				$pref .= '/';
			}
		}

		return $pref;
	}

	public function set_config_item($config_item, $item_value)
	{
		$this->config_files[$config_item] = $item_value;
	}

	public function get_site_url($uri = '')
	{
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}

		if ($uri == '')
		{
			return $this->slash_config_item('carbon_url') . $this->get_config_item('url_postfix');
		}
		else
		{
			$postfix = ($this->get_config_item('url_postfix') == false) ? '' : $this->get_config_item('url_postfix');

			return $this->slash_config_item('carbon_url') . $this->slash_config_item('index_page') . preg_replace("|^/*(.+?)/*$|", "\\1", $uri) . $postfix;
		}
	}

	public function get_system_url()
	{
		$x = explode('/', preg_replace("|^/*(.+?)/*$|", "\\1", CARBON_PATH));

		return $this->slash_config_item('carbon_url') . end($x) . '/';
	}

	public function get_application_url()
	{
		$x = explode('/', preg_replace("|^/*(.+?)/*$|", "\\1", APP_PATH));

		return $this->slash_config_item('carbon_url') . end($x) . '/';
	}
}

?>
