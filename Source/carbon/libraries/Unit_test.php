<?php

class Carbon_Unit_test
{
	protected $active = true;
	protected $result = array();
	protected $strict = false;
	protected $template = null;
	protected $template_rows = null;

	public function __construct()
	{
		log_message('debug', 'Unit_test.php - Carbon_Unit_test class initialised');
	}

	public function run($test, $expected = true, $test_name = 'undefined')
	{
		if ($this->active == false)
		{
			return false;
		}

		if (in_array($expected, array('is_string', 'is_bool', 'is_true', 'is_false', 'is_int', 'is_numeric', 'is_float', 'is_double', 'is_array',  'is_null'), true))
		{
			$expected = str_replace('is_float', 'is_double', $expected);
			$result = ($expected($test)) ? true : false;
			$extype = str_replace(array('true', 'false'), 'bool', str_replace('is_', '', $expected));
		}
		else
		{
			if ($this->strict == true)
			{
				$result = ($test === $expected) ? true : false;
			}
			else
			{
				$result = ($test == $expected) ? true : false;
			}

			$extype = gettype($expected);
		}

		$back = $this->_backtrack();

		$report[] = array(
			'test_name' => $test_name,
			'test_datatype' => gettype($test),
			'res_datatype' => $extype,
			'result' => ($result === true) ? 'passed' : 'failed',
			'file' => $back['file'],
			'line' => $back['line']
		);

		$this->results[] = $report;

		return ($this->report($this->result($report)));
	}

	public function report($result = array())
	{
		if (count($result) == 0)
		{
			$result = $this->result();
		}

		$this->_parse_template();

		$ret = '';

		foreach ($result as $res)
		{
			$table = '';

			foreach ($res as $key => $value)
			{
				$temp = $this->template_rows;			
				$temp = str_replace('{item}', $key, $temp);
				$temp = str_replace('{result}', $value, $temp);
				$table .= $temp;
			}

			$ret .= str_replace('{rows}', $table, $this->template);
		}

		return $ret;
	}

	public function use_strict($state = true)
	{
		$this->strict = ($state == true) ? true : false;
	}

	public function active($state = true)
	{
		$this->active = ($state == true) ? true : false;
	}

	public function result($results = array())
	{
		$carbon =& get_instance();
		$carbon->load->language('unit_test');

		if (count($results) == 0)
		{
			$results = $this->results;
		}

		$retval = array();

		foreach ($results as $result)
		{
			$temp = array();

			foreach ($result as $key => $value)
			{
				if (is_array($value))
				{
					foreach ($value as $k => $v)
					{
						$line = $carbon->lang->line(strtolower('unittest_' . $v));

						if ($line !== false)
						{
							$v = $line;
						}

						$temp[$carbon->lang->line('unittest_' . $k)] = $v;
					}
				}
				else
				{
					$line = $carbon->lang->line(strtolower('unittest_' . $value));

					if ($line !== false)
					{
						$value = $line;
					}

					$temp[$carbon->lang->line('unittest_' . $key)] = $value;
				}
			}

			$retval[] = $temp;
		}

		return $retval;
	}

	public function set_template($template)
	{
		$this->template = $template;
	}

	protected function _backtrack()
	{
		if (function_exists('debug_backtrace'))
		{
			$back = debug_backtrace();

			$file = (!isset($back['1']['file'])) ? '' : $back['1']['file'];
			$line = (!isset($back['1']['line'])) ? '' : $back['1']['line'];

			return array('file' => $file, 'line' => $line);
		}

		return array('file' => 'Unknown', 'line' => 'Unknown');
	}

	protected function _default_template()
	{
		$this->template = '';
		$this->template .= '<div style="margin:15px;background-color:#ccc;">';
		$this->template .= '<table border="0" cellpadding="4" cellspacing="1" style="width:100%;">';
		$this->template .= '{rows}';
		$this->template .= '</table></div>';

		$this->template_rows = '';
		$this->template_rows .= '<tr>';
		$this->template_rows .= '<td style="background-color:#fff;width:140px;font-size:12px;font-weight:bold;">{item}</td>';
		$this->template_rows .= '<td style="background-color:#fff;font-size:12px;">{result}</td>';
		$this->template_rows .= '</tr>';
	}

	protected function _parse_template()
	{
		if (!is_null($this->template_rows))
		{
			return;
		}

		if (is_null($this->template))
		{
			$this->_default_template();
			return;
		}

		if (!preg_match("/\{rows\}(.*?)\{\/rows\}/si", $this->template, $match))
		{
			$this->_default_template();
			return;
		}

		$this->template_rows = $match['1'];
		$this->template = str_replace($match['0'], '{rows}', $this->template);
	}
}

function is_true($test)
{
	return (is_bool($test) && $test === true) ? true : false;
}

function is_false($test)
{
	return (is_bool($test) && $test === false) ? true : false;
}

?>
