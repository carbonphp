<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Model
{
	protected $model_parent_class_name = '';

	public function __construct()
	{
		$this->assign_libraries((method_exists($this, '__set') || method_exists($this, '__get')) ? false : true);
		$this->model_parent_class_name = ucfirst(get_class($this));

		log_message('debug', 'Model.php - Model class initialised');
	}

	public function assign_libraries($use_references = true)
	{
		$carbon =& get_instance();

		foreach (array_keys(get_object_vars($carbon)) as $key)
		{
			if (!isset($this->$key) && $key != $this->model_parent_class_name)
			{
				if ($use_references)
				{
					$this->$key = '';
					$this->$key =& $carbon->$key;
				}
				else
				{
					$this->$key = $carbon->$key;
				}
			}
		}
	}
}

?>
