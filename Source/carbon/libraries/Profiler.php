<?php

class Carbon_Profiler
{
	protected $carbon;

	public function __construct()
	{
		$this->carbon =& get_instance();
		$this->carbon->load->language('profiler');
	}

	protected function _compile_benchmarks()
	{
		$profile = array();

		foreach ($this->carbon->benchmark->marker as $key => $value)
		{
			if (preg_match('/(.+?)_end/i', $key, $match))
			{
				if (isset($this->carbon->benchmark->marker[$match[1] . '_end']) && isset($this->carbon->benchmark->marker[$match[1] . '_start']))
				{
					$profile[$match[1]] = $this->carbon->benchmark->elapsed_time($match[1] . '_start', $key);
				}
			}
		}

		$output = "\n\n";
		$output .= '<fieldset style="border:1px solid #990000;padding:6px 10px 10px 10px;margin:0 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#990000;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_benchmarks') . '&nbsp;&nbsp;</legend>';
		$output .= "\n";			
		$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";

		foreach ($profile as $key => $value)
		{
			$key = ucwords(str_replace(array('_', '-'), ' ', $key));
			$output .= "<tr><td width='50%' style='color:#000;font-weight:bold;background-color:#ddd;'>" . $key . "&nbsp;&nbsp;</td><td width='50%' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $value . "</td></tr>\n";
		}

		$output .= "</table>\n";
		$output .= "</fieldset>";

		return $output;
	}

	protected function _compile_queries()
	{
		$output  = "\n\n";
		$output .= '<fieldset style="border:1px solid #0000FF;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";

		if ( ! class_exists('Carbon_Database_driver'))
		{
			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_queries') . '&nbsp;&nbsp;</legend>';
			$output .= "\n";		
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
			$output .="<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->carbon->lang->line('profiler_no_db') . "</td></tr>\n";
		}
		else
		{
			$output .= '<legend style="color:#0000FF;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_queries') . ' (' . count($this->carbon->db->queries) . ')&nbsp;&nbsp;</legend>';
			$output .= "\n";		
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
			
			if (count($this->carbon->db->queries) == 0)
			{
				$output .= "<tr><td width='100%' style='color:#0000FF;font-weight:normal;background-color:#eee;'>" . $this->carbon->lang->line('profiler_no_queries') . "</td></tr>\n";
			}
			else
			{
				foreach ($this->carbon->db->queries as $key => $value)
				{
					$value = htmlspecialchars($value, ENT_QUOTES);
					$time = number_format($this->carbon->db->query_times[$key], 4);
					
					$highlight = array('SELECT', 'FROM', 'WHERE', 'AND', 'LEFT JOIN', 'ORDER BY', 'LIMIT', 'INSERT', 'INTO', 'VALUES', 'UPDATE', 'OR');

					foreach ($highlight as $bold)
					{
						$value = str_replace($bold, '<strong>' . $bold . '</strong>', $value);	
					}
					
					$output .= "<tr><td width='1%' valign='top' style='color:#990000;font-weight:normal;background-color:#ddd;'>" . $time . "&nbsp;&nbsp;</td><td style='color:#000;font-weight:normal;background-color:#ddd;'>" . $value . "</td></tr>\n";
				}
			}
		}

		$output .= "</table>\n";
		$output .= "</fieldset>";
		
		return $output;
	}

	protected function _compile_get()
	{
		$output  = "\n\n";
		$output .= '<fieldset style="border:1px solid #cd6e00;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#cd6e00;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_get_data') . '&nbsp;&nbsp;</legend>';
		$output .= "\n";
				
		if (count($_GET) == 0)
		{
			$output .= "<div style='color:#cd6e00;font-weight:normal;padding:4px 0 4px 0'>" . $this->carbon->lang->line('profiler_no_get') . "</div>";
		}
		else
		{
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
		
			foreach ($_GET as $key => $value)
			{
				if (!is_numeric($key))
				{
					$key = "'" . $key . "'";
				}
			
				$output .= "<tr><td width='50%' style='color:#000;background-color:#ddd;'>&#36;_GET[" . $key . "]&nbsp;&nbsp; </td><td width='50%' style='color:#cd6e00;font-weight:normal;background-color:#ddd;'>";

				if (is_array($value))
				{
					$output .= "<pre>" . htmlspecialchars(stripslashes(print_r($value, true))) . "</pre>";
				}
				else
				{
					$output .= htmlspecialchars(stripslashes($value));
				}

				$output .= "</td></tr>\n";
			}
			
			$output .= "</table>\n";
		}

		$output .= "</fieldset>";

		return $output;	
	}

	protected function _compile_post()
	{
		$output  = "\n\n";
		$output .= '<fieldset style="border:1px solid #009900;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#009900;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_post_data') . '&nbsp;&nbsp;</legend>';
		$output .= "\n";
				
		if (count($_POST) == 0)
		{
			$output .= "<div style='color:#009900;font-weight:normal;padding:4px 0 4px 0'>" . $this->carbon->lang->line('profiler_no_post') . "</div>";
		}
		else
		{
			$output .= "\n\n<table cellpadding='4' cellspacing='1' border='0' width='100%'>\n";
		
			foreach ($_POST as $key => $value)
			{
				if (!is_numeric($key))
				{
					$key = "'".$key."'";
				}

				$output .= "<tr><td width='50%' style='color:#000;background-color:#ddd;'>&#36;_POST[" . $key."]&nbsp;&nbsp; </td><td width='50%' style='color:#009900;font-weight:normal;background-color:#ddd;'>";

				if (is_array($value))
				{
					$output .= "<pre>" . htmlspecialchars(stripslashes(print_r($value, true))) . "</pre>";
				}
				else
				{
					$output .= htmlspecialchars(stripslashes($value));
				}

				$output .= "</td></tr>\n";
			}
			
			$output .= "</table>\n";
		}

		$output .= "</fieldset>";

		return $output;	
	}

	protected function _compile_uri_string()
	{
		$output  = "\n\n";
		$output .= '<fieldset style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#000;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_uri_string') . '&nbsp;&nbsp;</legend>';
		$output .= "\n";
		
		if ($this->carbon->uri->get_uri_string() == '')
		{
			$output .= "<div style='color:#000;font-weight:normal;padding:4px 0 4px 0'>" . $this->carbon->lang->line('profiler_no_uri') . "</div>";
		}
		else
		{
			$output .= "<div style='color:#000;font-weight:normal;padding:4px 0 4px 0'>".$this->carbon->uri->get_uri_string() . "</div>";
		}
		
		$output .= "</fieldset>";

		return $output;	
	}

	protected function _compile_memory_usage()
	{
		$output  = "\n\n";
		$output .= '<fieldset style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= "\n";
		$output .= '<legend style="color:#000;">&nbsp;&nbsp;' . $this->carbon->lang->line('profiler_memory_usage') . '&nbsp;&nbsp;</legend>';
		$output .= "\n";
		
		if (function_exists('memory_get_usage') && ($usage = memory_get_usage()) != '')
		{
			$output .= "<div style='color:#000;font-weight:normal;padding:4px 0 4px 0'>" . number_format($usage) . ' bytes</div>';
		}
		else
		{
			$output .= "<div style='color:#000;font-weight:normal;padding:4px 0 4px 0'>" . $this->carbon->lang->line('profiler_no_memory_usage') . "</div>";				
		}
		
		$output .= "</fieldset>";

		return $output;
	}

	public function run()
	{
		$output = '<br clear="all" />';
		$output .= "<div style='background-color:#fff;padding:10px;'>";
		
		$output .= $this->_compile_memory_usage();
		$output .= $this->_compile_benchmarks();	
		$output .= $this->_compile_uri_string();
		$output .= $this->_compile_get();
		$output .= $this->_compile_post();
		$output .= $this->_compile_queries();
		
		$output .= '</div>';
		
		return $output;
	}
}

?>
