<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Logging
{
	protected $logging_levels = array('ERROR' => '1', 'DEBUG' => '2', 'INFO'  => '3', 'ALL'   => '4');
	protected $logging_path = '';
	protected $logging_threshold = 1;
	protected $logging_date_format = 'Y-m-d H:i:s';
	protected $logging_enabled = true;

	public function __construct()
	{
		$this->logging_path = (get_config_item('logging_path') != '') ? get_config_item('logging_path') : APP_PATH . 'logs/';

		if (!is_dir($this->logging_path) || !is_writeable($this->logging_path))
		{
			$this->logging_enabled = false;
		}

		if (is_numeric(get_config_item('logging_threshold')))
		{
			$this->logging_threshold = get_config_item('logging_threshold');
		}

		if (get_config_item('logging_date_format') != '')
		{
			$this->logging_date_format = get_config_item('logging_date_format');
		}
	}

	public function write_log_message($level = 'error', $message, $php_eror = false)
	{
		if ($this->logging_enabled === false)
		{
			return false;
		}

		$level = strtoupper($level);

		if (!isset($this->logging_levels[$level]) || ($this->logging_levels[$level] > $this->logging_threshold))
		{
			return false;
		}

		$file_path = $this->logging_path . 'log-' . date('Y-m-d') . FILE_EXT;
		$log_message = '';

		if (!file_exists($file_path))
		{
			$log_message .= "<" . "?php\n";
			$log_message .= "if (!defined('CARBON_PATH'))\n";
			$log_message .= "{\n";
			$log_message .= "\texit('Direct script access is not allowed.');\n";
			$log_message .= "}\n";
			$log_message .= "?" . ">\n\n";
		}

		$file = @fopen($file_path, "a");

		if (!$file)
		{
			echo 'Could not open the log file.';
			return false;
		}

		$log_message .= $level . ' ' . (($level == 'INFO') ? ' -' : '-') . ' ' . date($this->logging_date_format) . ' -> ' . $message . "\n";

		flock($file, LOCK_EX);
		fwrite($file, $log_message);
		flock($file, LOCK_UN);
		fclose($file);

		@chmod($file_path, 0666);

		return true;
	}
}

?>
