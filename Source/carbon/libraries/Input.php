<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Input
{
	protected $use_xss_clean = false;
	protected $ip_address = false;
	protected $user_agent = false;
	protected $allow_get_array = false;

	public function __construct()
	{
		$config =& load_class('Config');
		$this->use_xss_clean = ($config->get_config_item('use_xss_filtering') === true) ? true : false;
		$this->allow_get_array = ($config->get_config_item('use_query_strings') === true) ? true : false;
		$this->_sanitise_globals();

		log_message('debug', 'Input.php - Carbon_Input class initialised');
	}

	protected function _sanitise_globals()
	{
		$protected = array(
			'_SERVER',
			'_GET',
			'_POST',
			'_FILES',
			'_REQUEST',
			'_SESSION',
			'_ENV',
			'GLOBALS',
			'HTTP_RAW_POST_DATA',
			'carbon_folder',
			'application_folder',
			'benchmark',
			'ext',
			'config',
			'uri',
			'router',
			'output',
			'input');

		foreach (array($_GET, $_POST, $_COOKIE, $_SERVER, $_FILES, $_ENV, (isset($_SESSION) && is_array($_SESSION)) ? $_SESSION : array()) as $global)
		{
			if (!is_array($global))
			{
				if (!in_array($global, $protected))
				{
					unset($GLOBALS[$global]);
				}	
			}
			else
			{
				foreach ($global as $key => $val)
				{
					if (!in_array($key, $protected))
					{
						unset($GLOBALS[$key]);
					}

					if (is_array($val))
					{
						foreach ($val as $k => $v)
						{
							if (!in_array($k, $protected))
							{
								unset($GLOBALS[$k]);
							}
						}
					}
				}
			}
		}

		if ($this->allow_get_array == false)
		{
			$_GET = array();
		}
		else
		{
			if (is_array($_GET) && count($_GET) > 0)
			{
				foreach ($_GET as $key => $val)
				{
					$_GET[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
				}
			}
		}

		if (is_array($_POST) && count($_POST) > 0)
		{
			foreach ($_POST as $key => $val)
			{
				$_POST[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
			}
		}

		if (is_array($_COOKIE) && count($_COOKIE) > 0)
		{
			foreach ($_COOKIE as $key => $val)
			{
				$_COOKIE[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
			}
		}

		log_message('debug', 'Input.php - Global GET, POST, and COOKIE arrays have been sanitised');
	}

	protected function _clean_input_data($string)
	{
		if (is_array($string))
		{
			$new_array = array();

			foreach ($string as $key => $val)
			{
				$new_array[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
			}

			return $new_array;
		}

		if (get_magic_quotes_gpc())
		{
			$string = stripslashes($string);
		}

		if ($this->use_xss_clean === true)
		{
			$string = $this->xss_clean($string);
		}

		return preg_replace("/\015\012|\015|\012/", "\n", $string);
	}

	protected function _clean_input_keys($string)
	{
		if (!preg_match("/^[a-z0-9:_\/-]+$/i", $string))
		{
			exit('Disallowed characters in the array key.');
		}

		return $string;
	}

	public function get($index = '', $xss_clean = false)
	{
		if (!isset($_GET[$index]))
		{
			return false;
		}

		if ($xss_clean === true)
		{
			if (is_array($_GET[$index]))
			{
				foreach ($_GET[$index] as $key => $val)
				{
					$_GET[$index][$key] = $this->xss_clean($val);
				}
			}
			else
			{
				return $this->xss_clean($_GET[$index]);
			}
		}

		return $_GET[$index];
	}

	public function post($index = '', $xss_clean = false)
	{
		if (!isset($_POST[$index]))
		{
			return false;
		}
	
		if ($xss_clean === true)
		{
			if (is_array($_POST[$index]))
			{
				foreach ($_POST[$index] as $key => $val)
				{
					$_POST[$index][$key] = $this->xss_clean($val);
				}
			}
			else
			{
				return $this->xss_clean($_POST[$index]);
			}
		}
	
		return $_POST[$index];
	}

	public function cookie($index = '', $xss_clean = false)
	{
		if (!isset($_COOKIE[$index]))
		{
			return false;
		}

		if ($xss_clean === true)
		{
			if (is_array($_COOKIE[$index]))
			{
				$cookie = array();

				foreach ($_COOKIE[$index] as $key => $val)
				{
					$cookie[$key] = $this->xss_clean($val);
				}

				return $cookie;
			}
			else
			{
				return $this->xss_clean($_COOKIE[$index]);
			}
		}
		else
		{
			return $_COOKIE[$index];
		}
	}

	public function server($index = '', $xss_clean = false)
	{
		if (!isset($_SERVER[$index]))
		{
			return false;
		}

		if ($xss_clean === true)
		{
			return $this->xss_clean($_SERVER[$index]);
		}

		return $_SERVER[$index];
	}

	public function ip_address()
	{
		if ($this->ip_address !== false)
		{
			return $this->ip_address;
		}

		if ($this->server('REMOTE_ADDR') && $this->server('HTTP_CLIENT_IP'))
		{
			$this->ip_address = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if ($this->server('REMOTE_ADDR'))
		{
			$this->ip_address = $_SERVER['REMOTE_ADDR'];
		}
		else if ($this->server('HTTP_CLIENT_IP'))
		{
			$this->ip_address = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if ($this->server('HTTP_X_FORWARDED_FOR'))
		{
			$this->ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}

		if ($this->ip_address === false)
		{
			$this->ip_address = '0.0.0.0';

			return $this->ip_address;
		}

		if (strstr($this->ip_address, ','))
		{
			$x = explode(',', $this->ip_address);
			$this->ip_address = end($x);
		}

		if (!$this->valid_ip($this->ip_address))
		{
			$this->ip_address = '0.0.0.0';
		}

		return $this->ip_address;
	}

	public function valid_ip($ip)
	{
		$ip_segments = explode('.', $ip);

		if (count($ip_segments) != 4)
		{
			return false;
		}

		if (substr($ip_segments[0], 0, 1) == '0')
		{
			return false;
		}

		foreach ($ip_segments as $segment)
		{
			if (preg_match("/[^0-9]/", $segment) || $segment > 255 || strlen($segment) > 3)
			{
				return false;
			}
		}

		return true;
	}

	public function user_agent()
	{
		if ($this->user_agent !== false)
		{
			return $this->user_agent;
		}

		$this->user_agent = (!isset($_SERVER['HTTP_USER_AGENT'])) ? false : $_SERVER['HTTP_USER_AGENT'];

		return $this->user_agent;
	}

	public function filename_security($string)
	{
		$bad = array(
			"../",
			"./",
			"<!--",
			"-->",
			"<",
			">",
			"'",
			'"',
			'&',
			'$',
			'#',
			'{',
			'}',
			'[',
			']',
			'=',
			';',
			'?',
			"%20",
			"%22",
			"%3c",
			"%253c",
			"%3e",
			"%0e",
			"%28",
			"%29",
			"%2528",
			"%26",
			"%24",
			"%3f",
			"%3b",
			"%3d"
		);

		return stripslashes(str_replace($bad, '', $string));
	}

	public function xss_clean($string)
	{
		$string = preg_replace('/\0+/', '', $string);
		$string = preg_replace('/(\\\\0)+/', '', $string);

		$string = preg_replace('#(&\#?[0-9a-z]+)[\x00-\x20]*;?#i', "\\1;", $string);

		$string = preg_replace('#(&\#x?)([0-9A-F]+);?#i', "\\1\\2;", $string);

		$string = preg_replace("/(%20)+/", '9u3iovBnRThju941s89rKozm', $string);
		$string = preg_replace("/%u0([a-z0-9]{3})/i", "&#x\\1;", $string);
		$string = preg_replace("/%([a-z0-9]{2})/i", "&#x\\1;", $string);
		$string = str_replace('9u3iovBnRThju941s89rKozm', "%20", $string);

		$string = preg_replace_callback("/[a-z]+=([\'\"]).*?\\1/si", array($this, '_attribute_conversion'), $string);
		$string = preg_replace_callback("/<([\w]+)[^>]*>/si", array($this, '_html_entity_decode_callback'), $string);

		$string = str_replace("\t", " ", $string);

		$bad = array(
			'document.cookie' => '[removed]',
			'document.write' => '[removed]',
			'.parentNode' => '[removed]',
			'.innerHTML' => '[removed]',
			'window.location' => '[removed]',
			'-moz-binding' => '[removed]',
			'<!--' => '&lt;!--',
			'-->' => '--&gt;',
			'<!CDATA[' => '&lt;![CDATA['
		);

		foreach ($bad as $key => $val)
		{
			$string = str_replace($key, $val, $string);
		}

		$bad = array(
			"javascript\s*:" => '[removed]',
			"expression\s*\(" => '[removed]',
			"Redirect\s+302" => '[removed]'
		);

		foreach ($bad as $key => $val)
		{
			$string = preg_replace("#" . $key . "#i", $val, $string);
		}

		$string = str_replace(array('<?php', '<?PHP', '<?', '?' . '>'), array('&lt;?php', '&lt;?PHP', '&lt;?', '?&gt;'), $string);

		$words = array('javascript', 'expression', 'vbscript', 'script', 'applet', 'alert', 'document', 'write', 'cookie', 'window');

		foreach ($words as $word)
		{
			$temp = '';

			for ($i = 0; $i < strlen($word); $i++)
			{
				$temp .= substr($word, $i, 1) . "\s*";
			}

			$string = preg_replace('#(' . substr($temp, 0, -3) . ')(\W)#ise', "preg_replace('/\s+/s', '', '\\1') . '\\2'", $string);
		}

		do
		{
			$original = $string;

			if (stripos($string, '</a>') !== false || preg_match("/<\/a>/i", $string))
			{
				$string = preg_replace_callback("#<a.*?</a>#si", array($this, '_js_link_removal'), $string);
			}

			if (stripos($string, '<img') !== false || preg_match("/img/i", $string))
			{
				$string = preg_replace_callback("#<img.*?" . ">#si", array($this, '_js_img_removal'), $string);
			}

			if (stripos($string, 'script') !== false || stripos($string, 'xss') !== false || preg_match("/(script|xss)/i", $string))
			{
				$string = preg_replace("#</*(script|xss).*?\>#si", "", $string);
			}
		}
		while ($original != $string);

		unset($original);

		$event_handlers = array('onblur', 'onchange', 'onclick', 'onfocus', 'onload', 'onmouseover', 'onmouseup', 'onmousedown', 'onselect', 'onsubmit', 'onupload', 'onkeypress', 'onkeydown', 'onkeyup', 'onresize', 'xmlns');
		$string = preg_replace("#<([^>]+)(" . implode('|', $event_handlers) . ")([^>]*)>#iU", "&lt;\\1\\2\\3&gt;", $string);

		$string = preg_replace('#<(/*\s*)(alert|applet|basefont|base|behavior|bgsound|blink|body|embed|expression|form|frameset|frame|head|html|ilayer|iframe|input|layer|link|meta|object|plaintext|style|script|textarea|title|xml|xss)([^>]*)>#is', "&lt;\\1\\2\\3&gt;", $string);

		$string = preg_replace('#(alert|cmd|passthru|eval|exec|expression|system|fopen|fsockopen|file|file_get_contents|readfile|unlink)(\s*)\((.*?)\)#si', "\\1\\2&#40;\\3&#41;", $string);

		$bad = array(
			'document.cookie' => '[removed]',
			'document.write' => '[removed]',
			'.parentNode' => '[removed]',
			'.innerHTML' => '[removed]',
			'window.location' => '[removed]',
			'-moz-binding' => '[removed]',
			'<!--' => '&lt;!--',
			'-->' => '--&gt;',
			'<!CDATA[' => '&lt;![CDATA['
		);

		foreach ($bad as $key => $val)
		{
			$string = str_replace($key, $val, $string);
		}

		$bad = array(
			"javascript\s*:" => '[removed]',
			"expression\s*\(" => '[removed]',
			"Redirect\s+302" => '[removed]'
		);

		foreach ($bad as $key => $val)
		{
			$string = preg_replace("#" . $key . "#i", $val, $string);
		}

		log_message('debug', 'XSS filtering completed');

		return $string;
	}

	protected function _js_link_removal($match)
	{
		return preg_replace("#<a.+?href=.*?(alert\(|alert&\#40;|javascript\:|window\.|document\.|\.cookie|<script|<xss).*?\>.*?</a>#si", "", $match[0]);
	}

	protected function _js_img_removal($match)
	{
		return preg_replace("#<img.+?src=.*?(alert\(|alert&\#40;|javascript\:|window\.|document\.|\.cookie|<script|<xss).*?\>#si", "", $match[0]);
	}

	protected function _attribute_conversion($match)
	{
		return str_replace('>', '&lt;', $match[0]);
	}

	protected function _html_entity_decode_callback($match)
	{
		$config =& load_class('Config');

		$charset = $config->get_config_item('charset');

		return $this->_html_entity_decode($match[0], strtoupper($charset));
	}

	protected function _html_entity_decode($string, $charset = 'UTF-8')
	{
		if (stristr($string, '&') === false)
		{
			return $string;
		}

		if (function_exists('html_entitiy_decode') && strtolower($charset) != 'utf-8')
		{
			$string = html_entity_decode($string, ENT_COMPAT, $charset);
			$string = preg_match('~&#x([0-9a-f]{2,5})~ei', 'chr(hexdec("\\1"))', $string);

			return preg_replace('~&#([0-9]{2,4})~e', 'chr(\\1)', $string);
		}

		$string = preg_replace('~&#x([0-9a-f]{2,5});{0,1}~ei', 'chr(hexdec("\\1"))', $string);
		$string = preg_replace('~&#([0-9]{2,4})~e', 'chr(\\1)', $string);

		if (stristr($string, '&') === false)
		{
			$string = strtr($string, array(get_html_translation_table(HTML_ENTITIES)));
		}

		return $string;
	}
}

?>
