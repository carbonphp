<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Output
{
	protected $final_output;
	protected $cache_expiration = 0;
	protected $http_headers = array();
	protected $enable_profiler = false;

	public function __construct()
	{
		log_message('debug', 'Output.php - Carbon_Output class initialised');
	}

	public function set_final_output($final_output)
	{
		$this->final_output = $final_output;
	}

	public function get_final_output()
	{
		return $this->final_output;
	}

	public function append_output($output)
	{
		if ($this->final_output == '')
		{
			$this->final_output = $output;
		}
		else
		{
			$this->final_output .= $output;
		}
	}

	public function set_header_data($http_header)
	{
		$this->http_headers[] = $http_header;
	}

	public function enable_profiler($value = true)
	{
		$this->enable_profiler = (is_bool($value)) ? $value : true;
	}

	public function set_cache_expiration($expiration)
	{
		$this->cache_expiration = (!is_numeric($expiration) ? 0 : $expiration);
	}

	public function display_output($output = '')
	{
		global $config;
		global $bench;

		if ($output == '')
		{
			$output =& $this->final_output;
		}

		if ($this->cache_expiration > 0)
		{
			$this->_write_cache($output);
		}

		$elapsed = $bench->elapsed_time('total_execution_time_start', 'total_execution_time_end');
		$output = str_replace('{elapsed_time}', $elapsed, $output);

		$memory = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage() / 1024 / 1024, 2) . 'MB';
		$output = str_replace('{memory_usage}', $memory, $output);

		if ($config->get_config_item('compress_output') === true)
		{
			if (extension_loaded('zlib'))
			{
				if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)
				{
					ob_start('ob_gzhandler');
				}
			}
		}

		if (count($this->http_headers) > 0)
		{
			foreach ($this->http_headers as $http_header)
			{
				@header($http_header);
			}
		}

		if (!function_exists('get_instance'))
		{
			echo $output;
			log_message('debug', 'Output.php - Output sent to browser');
			log_message('debug', 'Output.php - Total execution time: ' . $elapsed);

			return true;
		}

		$carbon =& get_instance();

		if ($this->enable_profiler == true)
		{
			$carbon->load->library('profiler');

			if (preg_match("|</body>.*?</html>|is", $output))
			{
				$output  = preg_replace("|</body>.*?</html>|is", '', $output);
				$output .= $carbon->profiler->run();
				$output .= '</body></html>';
			}
			else
			{
				$output .= $carbon->profiler->run();
			}
		}

		if (method_exists($carbon, '_output'))
		{
			$carbon->_output($output);
		}
		else
		{
			echo $output;
		}

		log_message('debug', 'Output.php - Output sent to browser');
		log_message('debug', 'Output.php - Total execution time: ' . $elapsed);

		return true;
	}

	protected function _write_cache($output)
	{
		$carbon =& get_instance();
		$cache_path = $carbon->config->get_config_item('cache_path');
		$cache_path = ($cache_path == '') ? APP_PATH . 'cache/' : $cache_path;

		if (!is_dir($cache_path) || !is_writable($cache_path))
		{
			return false;
		}

		$uri = $carbon->config->get_config_item('carbon_url') . $carbon->config->get_config_item('index_page') . $carbon->uri->get_uri_string();

		$cache_path .= md5($uri);
		$file = @fopen($cache_path, 'wb');

		if (!$file)
		{
			echo 'Debug';
			log_message('error', 'Output.php - Unable to write to the cache file: ' . $cache_path);

			return false;
		}

		$expire = time() + ($this->cache_expiration * 60);

		flock($file, LOCK_EX);
		fwrite($file, $expire . 'time--->' . $output);
		flock($file, LOCK_UN);
		fclose($file);

		@chmod($cache_path, 0777);
		log_message('debug', 'Output.php - Successfully written to the cache file: ' . $cache_path);
	}

	public function _display_cache(&$config, &$router)
	{
		$uri =& load_class('Uri');

		$cache_path = ($config->get_config_item('cache_path') == '') ? APP_PATH . 'cache/' : $config->get_config_item('cache_path');

		if (!is_dir($cache_path) || !is_writable($cache_path))
		{
			return false;
		}

		$url = $config->get_config_item('carbon_url') . $config->get_config_item('index_page') . $uri->get_uri_string();

		$file_path = $cache_path . md5($url);

		if (!@file_exists($file_path))
		{
			return false;
		}

		$file = @fopen($file_path, 'rb');

		if (!$file)
		{
			return false;
		}

		flock($file, LOCK_EX);

		$cache = '';

		if (filesize($file_path) > 0)
		{
			$cache = fread($file, filesize($file_path));
		}

		flock($file, LOCK_UN);
		fclose($file);

		if (!preg_match("/(\d+time--->)/", $cache, $match))
		{
			return false;
		}

		if (time() >= trim(str_replace('time--->', '', $match['1'])))
		{
			@unlink($file_path);
			log_message('debug', 'Output.php - Cache file has expired, removing cache file');

			return false;
		}

		$this->display_output(str_replace($match['0'], '', $cache));
		log_message('debug', 'Output.php - Cache file is up to date, sending cache file to browser');

		return true;
	}
}

?>
