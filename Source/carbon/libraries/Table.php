<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

class Carbon_Table
{
	protected $rows = array();
	protected $heading = array();
	protected $auto_heading = true;
	protected $caption = null;
	protected $template = null;
	protected $newline = "\n";
	protected $empty_cells = '';

	public function __construct()
	{
		log_message('debug', 'Table.php - Carbon_Table class initialiased');
	}

	public function set_template($template)
	{
		if (!is_array($template))
		{
			return false;
		}

		$this->template = $template;

		return true;
	}

	public function set_heading()
	{
		$args = func_get_args();
		$this->heading = (is_array($args[0])) ? $args[0] : $args;
	}

	public function make_columns($array = array(), $col_limit = 0)
	{
		if (!is_array($array) || count($array) == 0)
		{
			return false;
		}

		$this->auto_heading = false;

		if ($col_limit == 0)
		{
			return $array;
		}

		$new = array();

		while (count($array) > 0)
		{
			$temp = array_slice($array, 0, $col_limit);
			$array = array_diff($array, $temp);

			if (count($temp) < $col_limit)
			{
				for ($i = count($temp); $i < $col_limit; $i++)
				{
					$temp[] = '&nbsp';
				}
			}

			$new[] = $temp;
		}

		return $new;
	}

	public function set_empty($value)
	{
		$this->empty_cells = $value;
	}

	public function add_row()
	{
		$args = func_get_args();
		$this->rows[] = (is_array($args[0])) ? $args[0] : $args;
	}

	public function set_caption($caption)
	{
		$this->caption = $caption;
	}

	public function generate($table_data = null)
	{
		if (!is_null($table_data))
		{
			if (is_object($table_data))
			{
				$this->_set_from_object($table_data);
			}
			elseif (is_array($table_data))
			{
				$set_heading = (count($this->heading) == 0 && $this->auto_heading == false) ? false : true;
				$this->_set_from_array($table_data, $set_heading);
			}
		}

		if (count($this->heading) == 0 && count($this->rows) == 0)
		{
			return 'Undefined table data';
		}

		$this->_compile_template();

		$out = $this->template['table_open'];
		$out .= $this->newline;

		if ($this->caption)
		{
			$out .= $this->newline;
			$out .= '<caption>' . $this->caption . '</caption>';
			$out .= $this->newline;
		}

		if (count($this->heading) > 0)
		{
			$out .= $this->template['heading_row_start'];
			$out .= $this->newline;

			foreach ($this->heading as $heading)
			{
				$out .= $this->template['heading_cell_start'];
				$out .= $heading;
				$out .= $this->template['heading_cell_end'];
			}

			$out .= $this->template['heading_row_end'];
			$out .= $this->newline;
		}

		if (count($this->rows) > 0)
		{
			$i = 1;

			foreach ($this->rows as $row)
			{
				if (!is_array($row))
				{
					break;
				}

				$name = (fmod($i++, 2)) ? '' : 'alt_';

				$out .= $this->template['row_' . $name . 'start'];
				$out .= $this->newline;

				foreach ($row as $cell)
				{
					$out .= $this->template['cell_' . $name . 'start'];

					if ($cell == '')
					{
						$out .= $this->empty_cells;
					}
					else
					{
						$out .= $cell;
					}

					$out .= $this->template['cell_' . $name . 'end'];
				}

				$out .= $this->template['row_' . $name . 'end'];
				$out .= $this->newline;
			}
		}

		$out .= $this->template['table_close'];

		return $out;
	}

	public function clear()
	{
		$this->rows = array();
		$this->heading = array();
		$this->auto_heading = true;
	}

	protected function _set_from_object($query)
	{
		if (!is_object($query))
		{
			return false;
		}

		if (count($this->heading) == 0)
		{
			if (!method_exists($query, 'list_fields'))
			{
				return false;
			}

			$this->heading = $query->list_fields();
		}

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$this->rows[] = $row;
			}
		}
	}

	protected function _set_from_array($data, $set_heading = true)
	{
		if (!is_array($data) || count($data) == 0)
		{
			return false;
		}

		$i = 0;

		foreach ($data as $row)
		{
			if (!is_array($row))
			{
				$this->rows[] = $data;
				break;
			}

			if ($i == 0 && count($data) > 1 && count($this->heading) == 0 && $set_heading == true)
			{
				$this->heading = $row;
			}
			else
			{
				$this->rows[] = $row;
			}

			$i++;
		}
	}

	protected function _compile_template()
	{
		if ($this->template == null)
		{
			$this->template = $this->_default_template();
			return true;
		}

		$this->temp = $this->_default_template();
		$template = array(
			'table_open',
			'heading_row_start',
			'heading_row_end',
			'heading_cell_start',
			'heading_cell_end',
			'row_start',
			'row_end',
			'cell_start',
			'cell_end',
			'row_alt_start',
			'row_alt_end',
			'cell_alt_start',
			'call_alt_end',
			'table_close'
		);
	
		foreach ($template as $value)
		{
			if (!isset($this->template[$value]))
			{
				$this->template[$value] = $this->temp[$value];
			}
		}
	}

	protected function _default_template()
	{
		$template = array(
			'table_open' => '<table border="0" cellingpadding="4" cellspacing="0">',
			'heading_row_start' => '<tr>',
			'heading_row_end' => '</tr>',
			'heading_cell_start' => '<th>',
			'heading_cell_end' => '</th>',
			'row_start' => '<tr>',
			'row_end' => '</tr>',
			'cell_start' => '<td>',
			'cell_end' => '</td>',
			'row_alt_start' => '<tr>',
			'row_alt_end' => '</tr>',
			'cell_alt_start' => '<td>',
			'cell_alt_end' => '</td>',
			'table_close' => '</table>'
		);

		return $template;
	}
}

?>
