<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

$lang['unittest_test_name'] = 'Test Name';
$lang['unittest_test_datatype'] = 'Test Datatype';
$lang['unittest_res_datatype'] = 'Expected Datatype';
$lang['unittest_result'] = 'Result';
$lang['unittest_undefined'] = 'Undefined Test Name';
$lang['unittest_file'] = 'File Name';
$lang['unittest_line'] = 'Line Number';
$lang['unittest_passed'] = 'Passed';
$lang['unittest_failed'] = 'Failed';
$lang['unittest_boolean'] = 'Boolean';
$lang['unittest_integer'] = 'Integer';
$lang['unittest_float'] = 'Float';
$lang['unittest_double'] = 'Float';
$lang['unittest_string'] = 'String';
$lang['unittest_array'] = 'Array';
$lang['unittest_object'] = 'Object';
$lang['unittest_resource'] = 'Resource';
$lang['unittest_null'] = 'Null'

?>