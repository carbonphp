<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

$lang['database_invalid_connection_str'] = 'Unable to determine the database settings based on the connection string supplied';
$lang['database_unable_to_connect'] = 'Unable to connect to the database server using the settings supplied';
$lang['database_unable_to_select'] = 'Unable to select the specified database: %s';
$lang['database_unable_to_create'] = 'Unable to create the specified database: %s';
$lang['database_invalid_query'] = 'The query submitted is not valid.';
$lang['database_must_set_table'] = 'You must set the database table to be used with your query';
$lang['database_must_use_set'] = 'You must use the "set" method to update an entry.';
$lang['database_must_use_where'] = 'Updates are not allowed unless they contain a "where" clause';
$lang['database_del_must_use_where'] = 'Deletes are not allowed unless they contain a "where" clause';
$lang['database_field_param_missing'] = 'To fetch, fields requires the name of the table as a parameter';
$lang['database_unsupported_function'] = 'This feature is not available for the database you are using';
$lang['database_transaction_failure'] = 'Transaction failure: Rollback performed';
$lang['database_unable_to_drop'] = 'Unable to drop the specified database';
$lang['database_unsupported_feature'] = 'Unsupported feature of the database platform you are using';
$lang['database_unsupported_compression'] = 'The file compression format you chose is not supported by your server';
$lang['database_filepath_error'] = 'Unable to write data to the file path you have submitted';
$lang['database_invalid_cache_path'] = 'The cache path you submitted is not valid or writable';
$lang['database_table_name_required'] = 'A table name is required for that operation';
$lang['database_column_name_required'] = 'A column name is required for that operation';
$lang['database_column_definition_required'] = 'A column definition is required for that operation';
$lang['database_unable_to_set_charset'] = 'Unable to set client connection character set: %s';

?>
