<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

if (!defined('CARBON_PATH'))
{
	exit('Direct script access is not allowed.');
}

$lang['scaff_view_records'] = 'View Records';
$lang['scaff_create_record'] = 'Create New Record';
$lang['scaff_add'] = 'Add Data';
$lang['scaff_view'] = 'View Record';
$lang['scaff_edit'] = 'Edit';
$lang['scaff_delete'] = 'Delete';
$lang['scaff_view_all'] = 'View All';
$lang['scaff_yes'] = 'Yes';
$lang['scaff_no'] = 'No';
$lang['scaff_no_data'] = 'No data exists for this table yet.';
$lang['scaff_del_confirm'] = 'Are you sure you wish to delete the following row:';

?>
