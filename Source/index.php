<?php
/*------------------------------------------------------------
*  CarbonPHP framework (C) Tom Bell
*  http://tombell.org.uk
*------------------------------------------------------------*/

error_reporting(E_ALL);

$carbon_application = 'application';
$carbon_system = 'carbon';

define('FILE_EXT', '.php');

define('INDEX_PATH', __FILE__);
define('INDEX_FILE', pathinfo(__FILE__, PATHINFO_BASENAME));

define('APP_PATH', str_replace('\\', '/', realpath($carbon_application)) . '/');
define('CARBON_PATH', str_replace('\\', '/', realpath($carbon_system)) . '/');

define('CARBON_VERSION', '2.1.2');

if (!is_dir(APP_PATH) && !is_dir(APP_PATH . '/config'))
{
	die('The application folder and configuration folder do not exist');
}

if (!is_dir(CARBON_PATH) && !file_exists(CARBON_PATH . 'core/Carbon_Core' . FILE_EXT))
{
	die('The carbon folder and Carbon_Core.php file do not exist');
}

require_once(CARBON_PATH . 'core/Carbon_Core' . FILE_EXT);

?>
